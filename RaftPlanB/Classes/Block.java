package Classes;

import java.util.List;
import java.util.Map;
import Interfaces.*;
/**
 * Structure de données “Block”
 */
public class Block implements IBlock {
    private String previousHash;
    private  String blockHash;
    private String root;

    private int nounce;
    private long timestamp;
    private int depth;
    private List<Integer> transactions;

    /**
     * Constructeur
     * @param previousHash
     * @param transactions
     * @param depth
     * @param timestamp
     */
    public Block(String previousHash, List<Integer> transactions, int depth, long timestamp){
        this.previousHash = previousHash;
        this.transactions = transactions;
        this.depth = depth;
        this.timestamp = timestamp;
    }

    /**
     * Cette méthode permet d’ajouter le nonce à l’en-tête du bloc après l’avoir calculé.
     * @param nounce
     */
    public void setNounce(int nounce){
        this.nounce = nounce;
    }

    /**
     * Cette méthode calcule le hash des transactions incluses dans le bloc. Pour simplifier le calcul,
     * on ne va pas appliquer la méthode de Merkle pour le calcul du hash.
     * On utilisera la méthode “sha256Hash” de la classe  HashfromString.
     */
    /** TransactionA/TransactionB/.. */
    public void calculateRoot(){
        String str = "";
        for(Integer tx : transactions){
            str = String.valueOf(tx) + "/";
        }
        str = str.substring(0, str.length() - 1);
        String str_hash = HashfromString.sha256Hash(str);
        this.root = str_hash;
    }

    /**
     * Cette méthode permet de calculer le hash du bloc pour enchaîner les blocs.
     * @return
     */
    public String calculateBlockHash(){ return "";}





}
