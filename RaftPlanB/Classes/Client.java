package Classes;

import Interfaces.IClient;

import java.io.IOException;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Un client est responsable de la génération des transactions. Il est connecté à un miner auquel il envoie ses transactions.
 */
public class Client implements IClient {

    //enum Cmp_type  {LASTDEPTH, TXDEPTH, NEWTX;}
    private long clientID;
    public final static int CONFIRMATION_BLOCK_NUMBER = 6;
    private DatagramSocket socket;
    private int minerPort,localPort;
    private Connection minerCon;
    private int lastdepth;
    private Semaphore trans_lock;
    private ArrayList<Transaction> transactions;
    public Thread thread_listennerIncome;

    public Client (long clientID){
        this.clientID = clientID;
        this.localPort = calculClientPort(clientID);
        trans_lock = new Semaphore(1);
        transactions = new ArrayList<>();
    }
    public int calculClientPort(long id){
        int tmp_id = (int) id;
        return 25000 + tmp_id * 100;
    }

    /**
     * Cette méthode est responsable de la création des transactions.
     * */
    private Transaction createTransaction(){
        Transaction tx = null;
        /** (Pour simplifier, une transaction est représentée par un identifiant unique)*/
        try {
            trans_lock.acquire();
            long idTemp = Utils.calcul_txID_unique(clientID,transactions.size());
            tx = new Transaction(this.clientID,idTemp);
            transactions.add(tx);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
            return tx;
    }
    private int miner_send_receive(String message) {
        String data_send = "Client "+clientID+"@@" + message;
        minerCon.write(data_send);
        String answ = minerCon.read();
        if(answ == null){
            //TODO HANDLE READ ERROR
        }
        return process(answ);
    }
    public void miner_send(String message){
        String data_send = "Client "+clientID+"@@" + message;
        minerCon.write(data_send);
    }


    /**
     * Cette méthode demande au miner la profondeur du dernier bloc dans sa blockchain.
     * @return
     * @throws IOException
     */
    private int getLastBlockDepth() {
        return miner_send_receive(Utils.Client_to_miner_command_type.LASTDEPTH.name());
    }
    /**
     * Cette méthode demande au miner la profondeur du bloc qui contient une certaine transaction représentée par son ID.
     * @return
     * @throws IOException
     */
    private int getTxBlockDepth(long id){
        int index = Utils.txID_to_index(clientID,id);
        Transaction trs = null;
        try{
            trs = transactions.get(index);
        } catch (IndexOutOfBoundsException e){
            return -1;
        }
        return miner_send_receive(Utils.Client_to_miner_command_type.TXDEPTH.name() +" "+ String.valueOf(id));
    }
    /**
     * Cette méthode permet au client d’attendre jusqu’à ce que une certaine transaction soit confirmée.
     * @param txID
     */
    private void waitForConfirmation(long txID) {
        int index = Utils.txID_to_index(clientID,txID);
        Transaction trs = null;
        try{
            trs = transactions.get(index);
        } catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }

        if(trs != null){
            while(true){
                try {
                    Boolean isAcquired = trs.lock.tryAcquire(1, TimeUnit.MINUTES);
                    if(isAcquired){
                        trs.lock.release();
                        System.out.format("Client id %d received his first confirmation for transaction id %d\n",clientID,txID);
                        break;
                    }
                    System.out.format("Client id %d is still waiting for the first confirmation for transaction id %d\n",clientID,txID);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            int txDepth = getTxBlockDepth(txID);
            if(txDepth == -1){
                //ERROR
                return;
            }
            while(true){
                int lastDepth = getLastBlockDepth();
                if(lastDepth == -1){
                    //ERROR
                    break;
                }
                if(lastDepth - txDepth >= CONFIRMATION_BLOCK_NUMBER){
                    System.out.format("Client id %d received all confirmation for transaction id %d\n",clientID,txID);
                    break;
                }
                System.out.format("Client id %d is still waiting for all confirmation for transaction id %d\n",clientID,txID);
                try {
                    Thread.currentThread().sleep(TimeUnit.MINUTES.toMillis(1));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }


    }

    public void setMinerPort(int minerPort) {
        this.minerPort = minerPort;
    }
    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }
    /**
     * Cette méthode initialise la connexion socket entre le client et un miner.
     * @return
     * @throws IOException
     * @throws Exception
     */
    public int init()throws IOException {
        minerCon = Utils.startConnectionToPeers(minerPort);
        thread_listennerIncome = new Thread(new Runnable() {
            @Override
            public void run() {
                stay_connected();
            }
        });
        thread_listennerIncome.start();

        return 0;
    }
    public int process(String message){
            String messagetmp = message;
            String message_received = null;
            //String from = null;
            String fromtype = null;
            long fromID = -1;
            String[] result = messagetmp.split("@@");
            int nb_message = result.length;
            if(nb_message ==  1){
                message_received = result[0];
            }else{
                String from = result[0];
                String[] result_1 = from.split(" ");
                fromtype = result_1[0];
                fromID = Long.parseLong(result_1[1]);
                message_received = result[1];
            }
            //TODO PARSE message_received

        return 0;
    }
    public void stay_connected(){
        while(true){
            String answer = minerCon.read();
            Thread thread = new Thread(){
                public void run(){
                    int errCode = 0;
                    if(answer != null){
                        errCode = process(answer);
                        if(errCode == -1){
                            //TODO HANDLE ERROR
                        }
                    } else{
                        //TODO HANDLE ERROR
                    }

                }
            };
            thread.start();

        }
    }

    /**
     * Cette méthode génère une transaction, l’envoie au miner et attend la confirmation.
     * @param tx
     * @throws IOException
     */
    public void sendTransaction(Transaction tx)throws IOException {

    }
    public void sendTransaction()throws IOException {
        Transaction tx = createTransaction();
        long tmp_id = tx.getTxID();
        String message = Utils.Client_to_miner_command_type.NEWTX + " " + String.valueOf(tmp_id);
        miner_send(message);
        waitForConfirmation(tmp_id);
    }
}
