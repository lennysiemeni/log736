package Classes;

import Interfaces.ITransaction;

import java.util.concurrent.Semaphore;

/**
 * Une transaction permet de transférer des Bitcoins entre deux comptes.
 * Pour simplifier l’implémentation, nous allons éliminer l’envoi de Bitcoin entre les clients.
 */
public class Transaction implements ITransaction {
        private long txID, clientID;
        private boolean confirmationState;
        public Semaphore lock;

        public Transaction(long clientID){
                this.clientID = clientID;
                this.confirmationState = false; //By default
                lock = new Semaphore(1);
                try {
                        lock.acquire();
                } catch (InterruptedException e) {
                        e.printStackTrace();
                }
        }
        public Transaction(long clientID,long txID){
                this.txID = txID;
                this.clientID = clientID;
                this.confirmationState = false; //By default
                lock = new Semaphore(1);
                try {
                        lock.acquire();
                } catch (InterruptedException e) {
                        e.printStackTrace();
                }
        }

        /**
         * Cette méthode change l’état de la transaction en “confirmée”.
         */
        public void setConfirmed(){
                this.confirmationState = true;
                this.lock.release();
        }

        /**
         * Cette méthode retourne l’état de la transaction.
         * @return
         */
        public boolean isConfirmed(){return confirmationState;}

        public long getTxID() {
                return txID;
        }

        public void setTxID(long txID) {
                this.txID = txID;
        }

}

