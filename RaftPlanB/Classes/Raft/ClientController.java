package Classes.Raft;

import Classes.Connection;
import Classes.Utils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.Semaphore;

public class ClientController {

	private int port;
	public ServerSocket listenner;
	public Thread thread_listennerIncome;
	public Thread leader_listenner;

	private int leader_port;
	private Connection leader_connection;
	public List<Command> commands_list;
	private String value_to_deal;
	public String value_received;
	private boolean is_value_decide_received;

	private Semaphore data_mutex;
	public Semaphore main_mutex;
	public final String[] command_type = {"SET","ADD","SUB","MULT"};

	public ClientController(int port,String message){
		this.port = port;
		this.leader_port = 0;
		this.commands_list = new ArrayList<>();
		leader_connection = null;
		value_received = null;
		value_to_deal = message;
		is_value_decide_received = false;
		this.data_mutex = new Semaphore(1);
		main_mutex = new Semaphore(1);
		try {
			main_mutex.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread_listennerIncome = new Thread(new Runnable() {
			@Override
			public void run() {
				listenToNetwork();
			}
		});
		thread_listennerIncome.start();
	}
	public String contains_commandtype(String command){
		for(String tmp_str : command_type){
			if(tmp_str.equals(command))
				return tmp_str;
		}
		return null;
	}

	public List<Command> create_command_list(String command_str){
		String[] result = command_str.split("&&");
		List<Command> command_list = new ArrayList<>();

		String tmp_cm_str = "";
		String tmp_value_str = "";
		int tmp_value = -1;
		for(String cm_str : result){
			String[] tmp_str = cm_str.split(" ");
			String tmp_cm_type = contains_commandtype(tmp_str[0]);
			tmp_value = Integer.parseInt(tmp_str[1]);
			if(tmp_cm_type == null)
				return null;
			Command tmp_cm = new Command(tmp_cm_type,tmp_value);
			command_list.add(tmp_cm);
		}
		return command_list;
	}
	public void listenToNetwork(){
		Socket socket = null;
		//System.out.println("Client "+Integer.toString(port)+ " is listening.");
		try {
			listenner = new ServerSocket();
			listenner.bind(new InetSocketAddress("127.0.0.1", port));
			try {
				while (true) {
					try {
						socket = listenner.accept();
						//System.out.println("Client "+Integer.toString(port)+" new connection.");
					} catch (Exception e) {
						break;
					}
					Connection conf = new Connection(socket);

					int port_connected = socket.getPort();
					String port_str = Integer.toString(port_connected);
					Thread thread = new Thread(port_str) {
						public void run(){
							listen_to_connected(conf);
						}
					};
					thread.start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Server error");
		}

	}
	/**
	 * code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
	 * */
	public void listen_to_connected(Connection conf){
		int code_error = 1;
		String answer;
		int timeout = -1;

		while(code_error > 0 ) {
			if(code_error == 1){
				answer = Utils.asynchrone_listen(conf);
//                isBackGround = Utils.checkIfBackground(answer);
				if(answer != null){
					code_error = start_parse(answer, conf);
				}
			}else
			if(code_error == 2){
				timeout = Utils.generate_random_timeout();
				answer = Utils.synchrone_listen(conf,timeout);
//                    isBackGround = Utils.checkIfBackground(answer);
				if(answer != null){
					code_error = start_parse(answer, conf);
				}else{
					//TODO handle panne processus or communication
				}
			}

		}
		if (code_error == -1) {
			//TODO HandleError
		}
		conf.terminate(null);


	}

	public void connectLeader() {
		try {
			this.leader_connection = startConnectionToPeers(leader_port);
			this.leader_listenner = new Thread(String.valueOf(leader_port)) {
				public void run(){
					listen_to_connected(leader_connection);
				}
			};
			leader_listenner.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Connection startConnectionToPeers(int port) throws IOException {
		Socket client_socket = new Socket();
		client_socket.connect(new InetSocketAddress("127.0.0.1", port));
		Connection conf = new Connection(client_socket);
		return conf;
	}
	public void printReceivedValue() {
		System.out.format("Client confirme consensus with value: %s\n",value_received);
	}

	public void writeOperation(Command command){
		String command_tmp = command.getCommandString();
		send_message(command_tmp,leader_connection);
	}
	public void init_command(String command_str){
		commands_list = create_command_list(command_str);
	}
	public void send_all_operation(){
		for(Command tmp_cm : commands_list){
			writeOperation(tmp_cm);
		}
	}
	private int send_receive_message(String message,Connection con) {
		String data_send = "Client"+Utils.destSeparator + message;
		con.write(data_send);
		String answ = con.read();
		if(answ == null){
			//TODO HANDLE READ ERROR
		}
		return start_parse(answ,con);
	}
	public void send_message(String message,Connection con){
		String data_send = "Client"+Utils.destSeparator + message;
		System.out.format("Client send message %s\n",data_send);
		leader_connection.write(data_send);
	}

	public void getLog(){

	}
	public void terminate_system() {
		String message = "CLIENT&SHUTDOWN ALL";
		connectLeader();
		leader_connection.write(message);
		leader_connection.terminate(null);
		leader_connection = null;
		try {
			listenner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public int setLeader(int portt){
		try {
			data_mutex.acquire();
			if(leader_port == portt){
				data_mutex.release();
				return -1;
			}

			System.out.format("Client received new leader port %d \n",portt);
			if(this.leader_connection != null){
				leader_connection.terminate("SHUTDOWN");
				leader_connection = null;
			}
			leader_port = portt;
			data_mutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return 1;
	}
	/**
	 * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
	 * */
	public int start_parse(String message, Connection conf){
		int code_error = 0;
		String messagetmp = message;
		String message_received = null;
		//String from = null;
		String fromtype = null;
		long fromID = -1;
		String[] result = messagetmp.split(Utils.destSeparator);
		int nb_message = result.length;
		if(nb_message ==  1){
			message_received = result[0];
			code_error = process(message_received, conf,null);
		}
		else {
			String from = result[0];
			String[] result_1 = from.split(" ");
			code_error = process(message_received, conf,result_1);
		}
		return code_error;
	}
	/**
	 * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
	 * */
	public int process(String message,Connection conf,String[] from) {
		//System.out.println("Noeud avec id: "+id+" receive message :" +message);
		StringTokenizer st = new StringTokenizer(message," ");
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str;
		switch(command.toUpperCase()) {
			case "OK":
				return 1;
			case "CLIENTRES":
				if(nbToken < 2){
					return -1;
				}
				str = st.nextToken();
				try {
					data_mutex.acquire();
					if(!this.is_value_decide_received){
						this.value_received = str;
						is_value_decide_received = true;
						printReceivedValue();
						main_mutex.release();
					}
					data_mutex.release();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			case "LEADERPORT":
				if(nbToken < 2){
					return -1;
				}
				str = st.nextToken();
				int port_leader = Integer.parseInt(str);
				int code_return = setLeader(port_leader);
				if(code_return > 0){
					connectLeader();
					send_all_operation();
				}
				break;
			default:
				// code block
		}
		return 0;

	}

}