package Classes.Raft;
import Classes.Connection;
import Classes.Utils;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;

public class Leader {
	 public enum Etat {
		START,
		WAIT_NEW_COMMAND,
		ON_CONSENSUS,
		STOP,
		ERROR
	}
	private Noeud noeud;
	private Thread thread_one;    /** Thread principale pour executer le concensus */
	public Etat etat_leader;
	private Semaphore wait_mutex;
	private Semaphore data_mutex;
	private Semaphore boolean_mutex;
	private boolean is_started;

	public int terme;
	public boolean is_stable;
	public boolean stopThread;
	public String value_decided;

	private int old_terme;
	private String old_value_decided;

	private int nb_acceptor_connected;
	private final int nbQuorom;

	private int nb_answer_promise;
	private int nb_answer_accept;

	private int nb_quorom_compteur;
	private int nb_nack_compteur;




	public Leader(Noeud noeud){
		this.noeud = noeud;
		stopThread = false;
		this.terme = 0;
		etat_leader = Etat.WAIT_NEW_COMMAND;
		this.is_stable = false;
		boolean_mutex = new Semaphore(1);
		is_started = false;
		this.old_terme = 0;
		old_value_decided = null;
		this.nbQuorom = ((noeud.nb_friend-1)/2) + 1;
	}
	public void reset(){
		this.terme = 0;
		this.old_terme = 0;
		old_value_decided = null;
	}
	public void start_consensus() throws InterruptedException {
		boolean haveToStart = false;
		Etat myEtat = null;
		boolean_mutex.acquire();
		if(etat_leader == Etat.WAIT_NEW_COMMAND){
			etat_leader = Etat.ON_CONSENSUS;
			haveToStart = true;
		}
		boolean_mutex.release();
		if(haveToStart){
			Thread thread_tmp = new Thread() {
				public void run(){
					try {
						wait_new_command();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
			thread_tmp.start();
		}
	}

	public void wait_new_command() throws InterruptedException {
		int index_now = -1;
		int index_start_notcommitted = -1;
		while(!stopThread){
			if(index_now == -1)
				index_now = noeud.log.get_nb_notCommitted();
			else{
				index_now++;
			}
			if(index_now < 1){
				boolean_mutex.acquire();
				etat_leader = Etat.WAIT_NEW_COMMAND;
				boolean_mutex.release();
			}
			else{
				Command cmp = noeud.log.getCommand_from_index(index_now);
				if(cmp.getTerme() == -1){
					cmp.setTerme(terme);
				}
				Command cmpprec = noeud.log.getCommand_from_index(index_now-1);
				int err_code = startConcensus(index_now,cmp,cmpprec);
				/** Un leader peut commit une commande seulement quand son propre terme a au moins une entrée committed */
				if(cmp.getTerme() == terme){
					noeud.log.setCommittedCommand(index_now);
					if(index_start_notcommitted != -1){
						while(index_start_notcommitted < index_now){
							noeud.log.setCommittedCommand(index_start_notcommitted);
							index_start_notcommitted++;
						}
						index_start_notcommitted = -1;
					}
					index_now = -1;
				}else{
					if(index_start_notcommitted == -1){
						index_start_notcommitted = index_now;
					}
				}
			}

		}
	}

	public int startConcensus(int index,Command cmp,Command cmp_prec){
		final int[] returnValue = {-1};
		thread_one = new Thread("Thread_one") {
			public void run(){
				int code_error = -2;
				while(code_error == -2){
					String cmp_str = cmp.getCommandString();
					String cmp_prec_str = cmp_prec.getCommandString();
					code_error = sendAppendEntries(index,cmp.getTerme(),cmp_str,cmp_prec_str);
					returnValue[0] = code_error;
					if(code_error == -1){
						if(noeud.panne_leader){
							//TODO HANDLE ERROR
							break;
						}
						//code_error = sendAppendEntries(); TODO SEE
					}
				}

			}
		};
		thread_one.start();
		try {
			thread_one.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return returnValue[0];
	}
	public void commitCommand(){

	}
	public int sendAppendEntries(int index,int terme,String command,String prec_command) {
		System.out.format("Leader send AppendEntries(%d, %d, %s, %s)\n",index,terme,command,prec_command);
		String index_str = String.valueOf(index);
		String terme_str = String.valueOf(terme);
		String message = "APPENDENTRIES"+ Utils.commandParamSeparator + index_str + Utils.paramSeparator+terme_str + Utils.paramSeparator+command+ Utils.paramSeparator+prec_command;
		nb_nack_compteur = 0;
		nb_quorom_compteur = 0;
		nb_answer_accept = 0;
		Map<Integer, Connection> outcall_connected = noeud.getOutcall_connected();
		nb_acceptor_connected = outcall_connected.size();
		wait_mutex = new Semaphore(nb_acceptor_connected);
		data_mutex = new Semaphore(1);
		for(Connection con : outcall_connected.values()){
			con.write(message);
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					try {
						wait_mutex.acquire();
						//System.out.println(Thread.currentThread().getName()+" pomise_wait acquired");
						//System.out.println(con.socket.getPort());
						String answer = con.read();
						int result = -1;
						if(answer != null)
							result = process(answer);
						if(result == -1){
							data_mutex.acquire();
							nb_answer_accept++;
							int nb_answer = nb_answer_accept;
							data_mutex.release();
							if(nb_answer >= nb_acceptor_connected)
								wait_mutex.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			thread.start();
		}
		try {
			thread_one.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
			try {
				//System.out.println("Principale pomise_wait ask acquire");
				wait_mutex.acquire();
				//System.out.println("Principale pomise_wait acquired");
				data_mutex.acquire();
				int nb_answers = this.nb_answer_accept;
				int nack_answers = this.nb_nack_compteur;
				int quorom_answers = this.nb_quorom_compteur;
				data_mutex.release();
				//System.out.println("Operation terminé!");
				if (nack_answers >= nbQuorom) {
					System.out.println("Leader quorom nack accept");
					return -2; /** -2 pour recommencer */
				}

			} catch (InterruptedException e) {
				System.out.format("The current thread is interrupted\n");
				e.printStackTrace();
			}

		return 0;

	}
	public int sendPrepareRequest(){
		nb_nack_compteur = 0;
		nb_quorom_compteur = 0;
		old_terme = 0;
		nb_answer_promise = 0;
		terme++;
		//sendPrepareRequest();
		Map<Integer, Connection> outcall_connected = noeud.getOutcall_connected();
		nb_acceptor_connected = outcall_connected.size();
		wait_mutex = new Semaphore(nb_acceptor_connected);
		data_mutex = new Semaphore(1);
		for(Connection con : outcall_connected.values()){
			con.write("PREPARE&"+String.valueOf(terme));
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					//System.out.println("run by: " + getName());
					try {
						wait_mutex.acquire();
						//System.out.println(Thread.currentThread().getName()+" pomise_wait acquired");
						//System.out.println(con.socket.getPort());
						String answer = con.read();
						int result = -1;
						if(answer != null)
							result = process(answer);
						if(result == -1){
							data_mutex.acquire();
							nb_answer_promise++;
							int nb_answers = nb_answer_promise;
							data_mutex.release();
							if(nb_answers >= nb_acceptor_connected)
								wait_mutex.release();

						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			con.thread_listennerIncome = thread;
			thread.start();
		}
		try {
			thread_one.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(true){
			try {
				//System.out.println("Principale pomise_wait ask acquire");
				wait_mutex.acquire();
				//System.out.println("Principale pomise_wait acquired");
				data_mutex.acquire();
				int nb_answers = this.nb_answer_promise;
				int nack_answers = this.nb_nack_compteur;
				int quorom_answers = this.nb_quorom_compteur;
				data_mutex.release();
				//System.out.println("Operation terminé!");
				if(quorom_answers > nbQuorom){
					System.out.println("Leader quorom promise");
					return -3; /** -3 pour sendAcceptRequest() */
				}
				else
					if(nack_answers > nbQuorom){
						System.out.println("Leader quorom NACK promise");
						return -2; /** -2 pour recommencer */
					}
					else{
						if(old_value_decided != null){
							System.out.println("Leader change sa valeur decider");
							value_decided = old_value_decided;
							terme = old_terme;
							return -3; /** -3 pour sendAcceptRequest() */
						}
					}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;

		}
		return 0;
	}
	public void appendCommand(){

	}
	public int process(String message) throws InterruptedException {
		//System.out.println("Leader receive "+message);
		StringTokenizer stt = new StringTokenizer(message,"\r\n");
		//int nbToken = stt.countTokens();
		String command_tmp = stt.nextToken();
		StringTokenizer st = new StringTokenizer(command_tmp,"&");
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str;
		int proposal_numb;
		int nb_answers = 0;
		switch (command.toUpperCase()) {
			case "NACK" -> {
				//System.out.format("Leader id %d  receive NACK\n",noeud.id);
				data_mutex.acquire();
				nb_answer_promise++;
				nb_nack_compteur++;
				nb_answers = nb_answer_promise;
				int nack_nb = nb_nack_compteur;
				data_mutex.release();

				if (nack_nb > nb_acceptor_connected / 2 || nb_answers >= nb_acceptor_connected) {
					wait_mutex.release();

				}
			}
			case "ACCEPTED" -> {
				if (nbToken < 2)
					return -1;
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				//System.out.format("Leader id %d  receive ACCEPTED(%d)\n",noeud.id,proposal_numb);
					data_mutex.acquire();
					nb_answer_accept++;
					nb_quorom_compteur++;
					nb_answers = nb_answer_accept;
					int quorom_nb = nb_quorom_compteur;
					data_mutex.release();

					if(quorom_nb > nb_acceptor_connected / 2 || nb_answers >= nb_acceptor_connected) {
						wait_mutex.release();
					}
				return 0;
			}
			case "PROMISE" -> {
				if (nbToken < 2)
					return -1;
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				if (proposal_numb != terme) {
					data_mutex.acquire();
					nb_answer_promise++;
					nb_answers = nb_answer_promise;
					data_mutex.release();

					if (nb_answers >= nb_acceptor_connected) {
						wait_mutex.release();

					}
					return 0;
				}
				if (nbToken == 2) {
					//System.out.format("Leader id %d  receive Promise(%d,-,-)\n",noeud.id,proposal_numb);
					data_mutex.acquire();
					nb_answer_promise++;
					nb_quorom_compteur++;
					nb_answers = nb_answer_promise;
					int quorom_nb = nb_quorom_compteur;
					data_mutex.release();

					if(quorom_nb > nb_acceptor_connected / 2 || nb_answers >= nb_acceptor_connected) {
						wait_mutex.release();
					}
					return 0;
				}
				if (nbToken != 4)
					return -1;
				str = st.nextToken();
				int tmp_old_proposal_nb = Integer.parseInt(str);
				str = st.nextToken();
				//System.out.format("Leader id %d  receive Promise(%d, %d, %s)\n",noeud.id,proposal_numb,tmp_old_proposal_nb,str);
				data_mutex.acquire();
				nb_answer_promise++;
				nb_answers = nb_answer_promise;
				if (old_terme < tmp_old_proposal_nb) {
					old_terme = tmp_old_proposal_nb;
					old_value_decided = str;
				}
				data_mutex.release();

				if (nb_answers == nb_acceptor_connected) {
					wait_mutex.release();

				}
			}
		}
		return 0;

	}
}