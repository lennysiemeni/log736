package Classes.Raft;
import Classes.Connection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Follower {

	private Noeud noeud;

	private List<Integer> learner_port;
	private List<Connection> learner_con;

	private int proposal_nb_promise;
	private int proposal_nb_accepted;

	private String value_decided;


	public Follower(Noeud noeud){
		this.noeud = noeud;
		proposal_nb_promise = 0;
		proposal_nb_accepted = 0;
		value_decided = null;
		learner_con = new ArrayList<Connection>();
		learner_port = new ArrayList<Integer>();
	}

	public void addLearner(int port) {
		learner_port.add(port);
	}

	public void setLearner_port(List<Integer> learner_port){
		this.learner_port = learner_port;
	}

	public int sendPromiseResponse(int proposal_nb,Connection con) {
		int return_value = 2;
		//System.out.format("Follower id %d  receive PROMISE(%d)\n",noeud.id,proposal_nb,proposal_nb);
		if(this.proposal_nb_promise > proposal_nb){
			con.write("NACK&");
			return_value = 1;
		}
		else
			if(value_decided != null){
				this.proposal_nb_promise = proposal_nb;
				con.write("PROMISE&"+String.valueOf(proposal_nb) + "&"+String.valueOf(this.proposal_nb_accepted) + "&" +this.value_decided);
			}
			else{
				con.write("PROMISE&"+String.valueOf(proposal_nb));
			}
			return return_value;
	}
	/**
	 * 
	 * @param proposal_nb
	 * @param value
	 */
	public void sendAcceptedRequest(int proposal_nb, String value) {
		if(noeud.isLearner){
			noeud.learner.valueAccepted = value;
			noeud.learner.handleAccept(proposal_nb,value);
		}
		if(learner_con.isEmpty()){
			if(noeud.outcall_connected.isEmpty()){
				for(int port : learner_port){
					try {
						Connection conn = noeud.startConnectionToPeers(port);
						noeud.outcall_connected.put(port,conn);
						this.learner_con.add(conn);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			else{
				for(int port : learner_port){
					this.learner_con.add(noeud.outcall_connected.get(port));
				}
			}
		}
		for(Connection con : this.learner_con){
			con.write("ACCEPT&"+proposal_nb+"&"+value);
		}
	}
	/**
	 *
	 * @param proposal_nb
	 * @param con
	 */
	public void acknowledge(int proposal_nb, Connection con) {
		con.write("ACCEPTED&"+proposal_nb);
	}
	/**
	 *
	 * @param proposal_nb
	 * @param value
	 */
	public void handleAcceptRequest(int proposal_nb, String value,Connection con) {
		//System.out.format("Follower id %d  receive accept(%d, %s)\n",noeud.id,proposal_nb,value);
		if(this.proposal_nb_promise > proposal_nb){
			con.write("NACK&");
		}
		else{
			acknowledge(proposal_nb,con);
			if(value_decided == null || !value_decided.equals(value)){
				value_decided = value;
				proposal_nb_accepted = proposal_nb;
				sendAcceptedRequest(proposal_nb,value);
			}
		}
	}

	public int handleAppendEntries(String[] params, Connection con) {
		int err_code = 0;

		return err_code;
	}


}