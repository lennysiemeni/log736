package Classes.Raft;

public enum CommandType {
    SET,
    ADD,
    SUB,
    MULT
}
