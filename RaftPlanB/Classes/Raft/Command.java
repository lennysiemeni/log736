package Classes.Raft;

import java.util.concurrent.Semaphore;

public class Command {
    private int terme;
    private String call;
    private int value;
    private boolean isCommitted;
    private Semaphore lock;
    private boolean isLast;
    public Command(String call,int value, int terme, boolean isLast){
        this.terme = terme;
        this.value = value;
        this.call = call;
        this.isLast = isLast;
        isCommitted = false;
    }
    public Command(String call,int value, int terme){
        this.terme = terme;
        this.value = value;
        this.call = call;
        isCommitted = false;
    }
    public Command(String call,int value, boolean isLast){
        this.value = value;
        this.call = call;
        this.isLast = isLast;
        isCommitted = false;
    }
    public Command(String call,int value){
        this.value = value;
        this.call = call;
        isCommitted = false;
    }
    public String getCommandString(){
        return call + " " + String.valueOf(value);
    }
    public  void setCommitted(boolean isCommitted){
        this.isCommitted = isCommitted;
    }
    public int getTerme(){
        return terme;
    }
    public void setTerme(int terme){
        this.terme = terme;
    }
    public int getValue(){
        return value;
    }
    public String getCall(){
        return call;
    }
    public boolean getIsCommitted(){
        return isCommitted;
    }

    public boolean getIsLast(){
        return isLast;
    }
}
