package Classes.Raft;

import java.io.IOException;

public class MainRaft {


    public static String message = "SET 6&&ADD 3&&SUB 1&&MULT 3";
    public static void test1() throws IOException, InterruptedException {
        //String message_command = "SET 6 && ADD 3 && SUB 1 && MULT 3";
        ClientController client = new ClientController(25100,message);
        int[] learner_id = {1,2};
        int nb_friend = 6;
        Noeud nd = new Noeud(1,nb_friend,learner_id);
        client.setLeader(25200);
        client.connectLeader();
        client.init_command(message);
        client.send_all_operation();
//        Noeud[] noeud_array = new Noeud[nb_friend];
//        for(int id = 1;id<=nb_friend;id++){
//            noeud_array[id-1] = new Noeud(id,nb_friend,learner_id);
//        }
//        Thread thread_electionLeader = new Thread() {
//            public void run(){
//                noeud_array[1].is_election_leader = true;
//                noeud_array[1].requestVote();
//            }
//        };
        //noeud_array[5].set_self_leader();
//        thread_electionLeader.start();
//        client.main_mutex.acquire();
//        if(client.value_received.equals(message)){
//            System.out.println("Concensus respected");
//        }
//        client.terminate_system();
    }
    public static void test2() throws IOException, InterruptedException {
        ClientController client = new ClientController(25100,message);
        int[] learner_id = {1,2};
        int nb_friend = 6;
        Noeud[] noeud_array = new Noeud[nb_friend];
        for(int id = 1;id<=nb_friend;id++){
            noeud_array[id-1] = new Noeud(id,nb_friend,learner_id);
        }
        noeud_array[0].panne_learner = true;
        noeud_array[4].panne_acceptor = true;
        noeud_array[5].set_self_leader();
        client.main_mutex.acquire();
        if(client.value_received.equals(message)){
            System.out.println("Concensus respected");
        }
        client.terminate_system();
    }
    public static void test3() throws IOException, InterruptedException {
        ClientController client = new ClientController(25100,message);
        int[] learner_id = {1,2};
        int nb_friend = 6;
        Noeud[] noeud_array = new Noeud[nb_friend];
        for(int id = 1;id<=nb_friend;id++){
            noeud_array[id-1] = new Noeud(id,nb_friend,learner_id);
        }
        noeud_array[5].panne_leader = true;
        noeud_array[5].set_self_leader();
        client.main_mutex.acquire();
        if(client.value_received.equals(message)){
            System.out.println("Concensus respected");
        }
        client.terminate_system();
    }
    public static void print_separator(int nb_test){
        int nb_tiret = 60;
        int nb_tiret_moite = nb_tiret/2 -3;
        for(int i =0;i<nb_tiret;i++)
            System.out.format("-");
        System.out.println("");
        for(int i =0;i<nb_tiret_moite;i++)
            System.out.format(" ");
        System.out.format("test %d",nb_test);
        for(int i =0;i<nb_tiret_moite;i++)
            System.out.format(" ");
        System.out.println("");
        for(int i =0;i<nb_tiret;i++)
            System.out.format("-");
        System.out.println("");
    }

    public static int essayer(Integer[] allo){
        allo = new Integer[2];
        allo[0] = 1;
        allo[1] = 1;
        return 0;
    }
    public static void main(String[] args) throws IOException, InterruptedException {
        print_separator(1);
        test1();
//        Thread.currentThread().sleep(5000);
//        print_separator(2);
//        test2();
//        Thread.currentThread().sleep(5000);
//        print_separator(3);
//        test3();

    }
}
