package Classes.Raft;
import Classes.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Log {
    public List<Command> log;
    public int nb_command_committed;
    public int nb_command;
//    private Semaphore append_mutex;
//    private Semaphore commit_mutex;
    private Semaphore data_mutex;
    public Log(){
//        append_mutex = new Semaphore(1);
//        commit_mutex = new Semaphore(1);
        data_mutex = new Semaphore(1);
        log = new ArrayList<Command>();
        nb_command_committed = 0;
        nb_command = 0;
    }
    public int append_command(Command com) throws InterruptedException {
        int command_numb = -1;
        data_mutex.acquire();
        log.add(com);
        nb_command++;
        command_numb = nb_command;
        data_mutex.release();
        return command_numb;
    }
    public int setCommittedCommand(int index) throws InterruptedException {
        if(index < 0)
            return -1;
        int nbCommand,nb_tmp = -1;
        data_mutex.acquire();
        nbCommand = nb_command;
        if(index <= nbCommand){
            log.get(index).setCommitted(true);
            nb_command_committed++;
            nb_tmp = nb_command_committed;
        }
        data_mutex.release();
        return nb_tmp;
    }
    public Command getCommand_from_index(int index) throws InterruptedException{
        Command comm = null;
        if(index < 0)
            return comm;
        int nbCommand = -1;
        data_mutex.acquire();
        nbCommand = nb_command;
        if(index <= nbCommand){
            comm = log.get(index);
        }
        data_mutex.release();
        return  comm;
    }

    public Command get_notCommitted_command() throws InterruptedException {
        Command cmp = null;
        int result = -1;
        data_mutex.acquire();
        result = nb_command - nb_command_committed;
        if(result > 0){
            cmp = log.get(nb_command_committed);
        }
        data_mutex.release();
        return  cmp;
    }
    public int get_nb_notCommitted() throws InterruptedException {
        int result = -1;
        data_mutex.acquire();
        result = nb_command - nb_command_committed;
        data_mutex.release();
        return  result;
    }
    public void rollbackAndSynchronizeCommits(){

    }
}
