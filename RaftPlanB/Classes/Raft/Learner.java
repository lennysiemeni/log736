package Classes.Raft;
import Classes.Connection;
import java.io.IOException;
import java.util.concurrent.Semaphore;

public class Learner {

	private Noeud noeud;

	private int nb_answer_accept;
	private int nbQuorom;

	public String valueAccepted;
	private int proposal_nb;

	private Semaphore data_mutex;

	public Learner(Noeud noeud){
		this.noeud = noeud;
		this.proposal_nb = 0;
		this.valueAccepted = null;
		this.nb_answer_accept = 0;
		this.nbQuorom = ((noeud.nb_friend-1)/2) + 1;
		this.data_mutex = new Semaphore(1);

	}

	/**
	 * 
	 * @param value_decided
	 */
	public void notifyClientCommit(String value_decided) throws IOException {
		//System.out.format("Learner id %d  send clientRes(%s)\n",this.noeud.id,value_decided);
		Connection client =  noeud.startConnectionToPeers(noeud.client_port);
		client.write("clientRes&"+value_decided);
		client.terminate(null);
	}
	public void sendClientLeaderPort(int port) {
		//System.out.format("Learner id %d  send LeaderPort(%d)\n",this.noeud.id,port);
		try {
			Connection client =  noeud.startConnectionToPeers(noeud.client_port);
			client.write("LeaderPort&"+port);
			client.terminate(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void handleAccept(int proposal_nb, String value) {

		try {
			this.data_mutex.acquire();
			//System.out.format("Learner id %d  receive ACCEPT(%d,%s)\n",this.noeud.id,proposal_nb,value);
			if(this.proposal_nb < proposal_nb){
				valueAccepted = value;
				this.proposal_nb = proposal_nb;
				nb_answer_accept = 0;
			}
			else
			if(this.proposal_nb == proposal_nb){
				nb_answer_accept++;
				if(nb_answer_accept == nbQuorom){
					notifyClientCommit(value);
				}
			}
			this.data_mutex.release();
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}

}