package Classes;

import Interfaces.IBlock;
import Interfaces.IMiner;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Miner implements IMiner {

    public final static int BLOCK_SIZE = 10;
    public long id;
    //private DatagramSocket socket;
    public ServerSocket listenner;
    public Thread thread_listennerIncome;
    public Map<Integer, Connection> miner_outcall_connected;
    public Map<Long, Connection> client_connected;
    private ArrayList<IBlock> blockchain;
    private ArrayList<ArrayList<IBlock>> branches;
    private ArrayList<Integer> neighborNodes;
    private LinkedList<Transaction> mempool;
    private Semaphore mutex_pool_sets;
    private Semaphore mutex_blockchain_sets;
    private Semaphore mutex_branches_sets;
    private int nbMiner; /** Pour simplifier on va considérer que le miner est connecté avec tous les autres miners */
    public final int port;



    public Miner (long id,int nbMiner){
        this.id = id;
        this.port = calculPort(id);
        this.nbMiner = nbMiner;
        this.neighborNodes = neighborNodesPort();
        this.blockchain = null;
        mutex_blockchain_sets = new Semaphore(1);
        mutex_branches_sets = new Semaphore(1);
        mutex_pool_sets = new Semaphore(1);
        this.mempool = new LinkedList<Transaction>();
        this.miner_outcall_connected = new HashMap<Integer, Connection>();
        this.client_connected = new HashMap<Long, Connection>();
        thread_listennerIncome = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    listenToNetwork();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread_listennerIncome.start();
    }

    public int calculPort(long id){
        int tmp_id = (int) id;
        return 25100 + tmp_id * 100;
    }

    //used whenever a miner finds a branch longer than its blockchain
    private void setLongestChain(){
        //TODO
    }

    //generates a new block, finds the corresponding PoW and sets the block's hash and nounce
    private IBlock mineBlock(){
        int ind_blc = 0;
        List<Transaction> tx_array = new ArrayList<Transaction>();
        while(ind_blc<BLOCK_SIZE){
            Transaction tmp_tx = popFromMemPool();
            if(tmp_tx == null){ //Peut-etre mempool est devenu vide
                break;
            }
            tx_array.add(tmp_tx);
        }
        //TODO CONTINUER
        return null;

    }

    private void addBlock(Block block) {
        try {
            mutex_blockchain_sets.acquire();
            blockchain.add(block);
            mutex_blockchain_sets.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean validateBlock(IBlock previousBlock, IBlock currentBlock){
        return true;
    }

    private void addToMemPool(Transaction tx){
        try {
            mutex_pool_sets.acquire();
            mempool.add(tx);
            mutex_pool_sets.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    private Transaction popFromMemPool(){
        Transaction tmp = null;
        try {
            mutex_pool_sets.acquire();
            if(!mempool.isEmpty())
                tmp = mempool.removeFirst();
            mutex_pool_sets.release();
        } catch (InterruptedException|IndexOutOfBoundsException e) {
            tmp = null;
        }
        return tmp;
    }
    //returns the genesis block
    public ArrayList<IBlock> init() throws IOException {
        //TODO
        return null;
    }
    /** Pour simplifier on va considérer que le miner est connecté avec tous les autres miners */
    public ArrayList<Integer> neighborNodesPort() {
        ArrayList<Integer> neighborPort = new ArrayList<Integer>();
        for (long i = 0; i < this.nbMiner; i++) {
            if (i != id) {
                int port_miner = this.calculPort(i);
                neighborPort.add(port_miner);
            }
        }
        return neighborPort;
    }

    public void connectToAll(){
        miner_outcall_connected = connect();
    }
    //returns a list of connected nodes
    public Map<Integer, Connection> connect() {
        Map<Integer, Connection> peers_connected = new HashMap<Integer, Connection>();
        ExecutorService es = Executors.newCachedThreadPool();
        for(int port_miner : neighborNodes) {
            if(miner_outcall_connected.isEmpty() || !miner_outcall_connected.containsKey(port_miner))
                es.execute(new Runnable() {
                    public void run() {
                        try {
                            Connection cn = Utils.startConnectionToPeers(port_miner);
                            //send_message(Utils.Miner_to_miner_message_type.HANDSHAKE.name(),cn);
                            peers_connected.put(port_miner,cn);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

        }

        es.shutdown();
        try {
            boolean finished = false;
            while(!finished){
                finished = es.awaitTermination(1, TimeUnit.MINUTES);
                if(!finished ){
                    //Maybe panne prcessor detected
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return peers_connected;
    }
    /**
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
     * */
    public int client_process(String message,long id_client, Connection conf){
        System.out.format("Miner id: %d received from: client id: %d the message: %s  \n",id,conf.id,message);
        int code_error = 1;
        switch (message.toUpperCase()) {
            case "HELLOWORLD":
                break;
            default:
                code_error = -1;
        }

        return code_error;
    }
    /**
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
     * */
    public int miner_process(String message,long id_miner, Connection conf){
        System.out.format("Miner id: %d received from: miner id: %d the message: %s  \n",id,conf.id,message);
        int code_error = 1;
        switch (message.toUpperCase()) {
            case "HELLOWORLD":
                break;
            default:
                code_error = -1;
        }


        //TODO PARSE MINER COMMAND

        return code_error;
    }
    /**
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
     * */
    public int general_process(String message, Connection conf){
        //System.out.format("Miner id: %d received from: %s id: %d the message: %s  \n",id,conf.Role.name(),conf.id,message);
        int code_error = 1;

        //TODO PARSE GENERAL COMMAND

        return code_error;
    }
    /**
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
     * */
    public int startProcess(String message, Connection conf){
        int code_error = 0;
        String messagetmp = message;
        String message_received = null;
        //String from = null;
        String fromtype = null;
        long fromID = -1;
        String[] result = messagetmp.split("@@");
        int nb_message = result.length;
        if(nb_message ==  1){
            message_received = result[0];
            code_error = general_process(message_received, conf);
        }
        else {
            String from = result[0];
            String[] result_1 = from.split(" ");
            fromtype = result_1[0];
            fromID = Long.parseLong(result_1[1]);
            message_received = result[1];
            switch (fromtype.toUpperCase()) {
                case "MINER":
                    code_error = miner_process(message_received,fromID, conf);
                    break;
                case "CLIENT":
                    code_error = client_process(message_received,fromID, conf);
                    break;
                default:
                    code_error = -1;
            }
        }
        return code_error;
    }
    private void send_message(String message,Connection conf){
        String data_send = "Miner "+this.id+"@@" + message;
        conf.write(data_send);
    }
    public void broadcast_message(String message){
        for(Connection conf : miner_outcall_connected.values()){
            send_message(message,conf);
        }
    }
    /**
     * code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
     * */
    public void listen_to_connected(Connection conf){
        int code_error = 1;
        int timeout = -1;
        String answer = Utils.asynchrone_listen(conf);
//        boolean isBackGround = Utils.checkIfBackground(answer);
//        ArrayList<Thread> thread_list = new ArrayList<Thread>();
        String[] result = answer.split("@@");
        int nb_message = result.length;
        if(nb_message ==  1){
            code_error = general_process(result[0], conf);
        }
        else {
            String from = result[0];
            String[] result_1 = from.split(" ");
            String fromtype = result_1[0];
            long fromID = Long.parseLong(result_1[1]);
            switch (fromtype.toUpperCase()) {
                case "MINER":
                    //System.out.format("Miner id: %d have new connection with Miner id: %d \n",id,fromID);
                    conf.id = fromID;
                    conf.Role = Utils.Role.MINER;
                    //miner_incall_connected.put(fromID,conf);
                    code_error = miner_process(result[1],fromID, conf);
                    break;
                case "CLIENT":
                    //System.out.format("Miner id: %d have new connection with Client id: %d \n",id,fromID);
                    conf.id = fromID;
                    conf.Role = Utils.Role.CLIENT;
                    client_connected.put(fromID,conf);
                    code_error = client_process(result[1],fromID, conf);
                    break;
                default:
                    code_error = -1;
            }
        }

        while(code_error > 0 ) {
            if(code_error == 1){
                answer = Utils.asynchrone_listen(conf);
//                isBackGround = Utils.checkIfBackground(answer);
                if(answer != null){
                    code_error = startProcess(answer, conf);
                }
            }else
                if(code_error == 2){
                    timeout = Utils.generate_random_timeout();
                    answer = Utils.synchrone_listen(conf,timeout);
//                    isBackGround = Utils.checkIfBackground(answer);
                    if(answer != null){
                        code_error = startProcess(answer, conf);
                    }else{
                        //TODO handle panne processus or communication
                    }
                }

        }
        if (code_error == -1) {
            //TODO HandleError
        }
        conf.terminate(null);


    }
    //calls mineBlock method whenever it collects transactions and validates received blocks and adds it to the current chain
    public void listenToNetwork()throws IOException{

        Socket socket = null;
        System.out.println("Miner id:"+String.valueOf(id)+ " is listening.");
        try {
            listenner = new ServerSocket();
            //bind the socketserver only to localhost
            listenner.bind(new InetSocketAddress("127.0.0.1", port));
            try {
                while (true) {
                    try {
                        socket = listenner.accept();

                    } catch (Exception e) {
                        //System.out.format("Noeud id %d closing server\n",id);
                        break;
                    }
                    Connection conf = new Connection(socket);
                    Thread thread = new Thread() {
                        public void run(){
                            listen_to_connected(conf);
                        }
                    };
                    thread.start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Server error");
        }

    }

    public ArrayList<Block> synchronise() throws IOException{

        return null;

    }


}
