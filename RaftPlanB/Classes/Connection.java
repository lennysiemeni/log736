package Classes;


import java.io.*;
import java.net.Socket;

public class Connection {

	public Socket socket;
	public Thread thread_listennerIncome;
	public PrintWriter out;
	public BufferedReader in;
	public long id;
	public Utils.Role Role;

	public Connection(Socket socket){
		this.socket = socket;
		this.id = -1;
		this.thread_listennerIncome = null;
		try {
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING));
			this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public Connection(Socket socket,int id){
		this.socket = socket;

		this.thread_listennerIncome = null;
		try {
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING));
			this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.id = id;

	}
	public String read() {
		String firstLine;
		try{
			while ((firstLine = in.readLine()) != null) {
				return firstLine;
			}
		}catch (IOException ex){
			ex.printStackTrace();
		}

		return "ERROR";
	}

	/**
	 * 
	 * @param message
	 */
	public void write(String message) {
		//System.out.println("Noeud "+socket.getLocalPort()+" writing");
		out.write(message+ "\r\n");
		out.flush();
	}
	public void terminate(String message){
		if(message != null){
			write(message);
		}
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// This is telnet specific, maybe you have to change it according to your
	// protocol
	private static boolean checkIfFinished(String data) {
		int length = data.length();
		if (length < 3) {
			return false;
		} else {
			if (data.charAt(length -1) == '\n') {
				if (data.charAt(length -2) == '\r') {
					return true;
				}
			}
			return false;
		}
	}


	public void write_background(String message) {
		//System.out.println("Noeud "+socket.getLocalPort()+" writing");
		out.write(message+ "%@\r\n");
		out.flush();
	}


}