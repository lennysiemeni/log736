package Classes;

import java.io.IOException;

public class MainClass {

    public static void print_separator(int nb_test){
        int nb_tiret = 60;
        int nb_tiret_moite = nb_tiret/2 -3;
        for(int i =0;i<nb_tiret;i++)
            System.out.format("-");
        System.out.println("");
        for(int i =0;i<nb_tiret_moite;i++)
            System.out.format(" ");
        System.out.format("test %d",nb_test);
        for(int i =0;i<nb_tiret_moite;i++)
            System.out.format(" ");
        System.out.println("");
        for(int i =0;i<nb_tiret;i++)
            System.out.format("-");
        System.out.println("");
    }
    public static int calculMinerPort(int id){
        return 25100 + id * 100;
    }
    public static Client[] generateClientArray(int nbClient){
        Client[] client_array = new Client[nbClient];
        for(int i=0;i<nbClient;i++){
            client_array[i] = new Client(i);
        }
        return  client_array;
    }
    public static Miner[] generateMinerArray(int nbMiner){
        Miner[] miner_array = new Miner[nbMiner];
        for(int i=0;i<nbMiner;i++){
            miner_array[i] = new Miner(i,nbMiner);
        }
        return  miner_array;
    }

    public static void test1() {
        int nbMiner = 6;
        Miner[] miner_array = generateMinerArray(nbMiner);
        int nbClient = 3;
        Client[] client_array = generateClientArray(nbClient);
        for(int i=0;i<nbClient;i++){
            client_array[i].setMinerPort(calculMinerPort(i));
            try {
                client_array[i].init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for(int i=0;i<nbMiner;i++){
            miner_array[i].connectToAll();
        }
        for(int i=0;i<nbClient;i++){
            client_array[i].miner_send("HELLOWORLD");
        }
        Utils.forceThreadSleepSeconde(Thread.currentThread(),10);
        //miner_array[1].broadcast_message("HELLOWORLD");
        for(int i=0;i<nbMiner;i++){
            miner_array[i].broadcast_message("HELLOWORLD");
        }

    }
    public static void main(String[] args) throws IOException, InterruptedException {
        print_separator(1);
        test1();

    }
}
