package test;

import client.Client;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.*;
import java.net.ConnectException;

public class ShutDownAndBlock {

    Client client;

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @BeforeEach
    public void setUp() throws Exception {
        client = new Client();
    }

    @Test
    public void shutdown_and_blocking() throws IOException{
        client.getOut().println("SHUTDOWN");
        client.getOut().flush();

        client.getOut().println("PING");
        client.getOut().flush();
        exception.expect(ConnectException.class);
    }
}
