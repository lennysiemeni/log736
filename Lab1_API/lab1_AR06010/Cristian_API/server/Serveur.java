package server;

import interfaces.IServeur;
import shared.Constants;
import threads.ServeurThreadsCristian;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

// Inspire de : https://www.codejava.net/java-se/networking/java-socket-server-examples-tcp-ip
// Aide pour les threads : https://stackoverflow.com/questions/10131377/socket-programming-multiple-client-to-one-server
// Fermer un serveur proprement : https://stackoverflow.com/questions/8001204/how-to-close-serversocket-connection-when-catching-ioexception

/**
 * Cette classe permet d'instancier le serveur d'ecoute TELNET
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
public class Serveur extends Thread implements IServeur{
    private static final int SERVER_PORT = 25000;
    public String receivedMessage;
    private ServerSocket server_soc;
    private Socket socket;
    public boolean shutdown_signal;
    public boolean disconnect_signal;



    /**
     * Class Constructor
     * @throws IOException connection error with any of the clients
     */
    public Serveur() throws IOException {
        int port = SERVER_PORT;
        this.shutdown_signal = false;
    }

    /**
     * Run the Server
     */
    public void run(){
        try {
            startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the shutdown value if at any time such command has been received
     * @param value if shutdown command has been received
     */
    public void setShutdown(boolean value){
        this.shutdown_signal = value;
    }

    /**
     * Starts a threaded server and set it up to a listening state
     * Con support multiple clients connected at once
     * @throws IOException connection error with any of the clients
     */
    public void startServer() throws IOException{
        boolean shutdown_signal = false;
        String line=null;
        System.out.println("Server is up......");
        setShutdown(false);
        try{
            this.server_soc = new ServerSocket(SERVER_PORT);
            try {
                while(!this.shutdown_signal){
                    socket = this.server_soc.accept();
                    System.out.println("New Client connected");
                    try{

                        parseCommand();
                        if(disconnect_signal){socket.close();socket = new Socket();disconnect_signal=false;}

                    } catch(Exception e){
                        e.printStackTrace();
                        System.out.println("Connection Error");
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch(IOException e){
            e.printStackTrace();
            System.out.println("Server error");
        }
        if(this.shutdown_signal) {socket.close();stopServer();}
    }

    /**
     * Command parser. Takes the command sent from a client and parses it
     * @throws IOException connection error
     * @throws InterruptedException we simulated a more realistic RTT behavior by putting
     * the process in a sleep state. The result produces an RTT between 1 and 100 ms
     */
    public void parseCommand() throws IOException, InterruptedException {
        InputStreamReader in = new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING);
        BufferedReader bf = new BufferedReader(in);
        PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
        String line;
        while(!shutdown_signal && !disconnect_signal){
            //While at least one client is alive
            line = bf.readLine();
            if(line == null){
                System.out.println("Closing client connection ...");
                break;
            } else {
                //Parse les commandes les les plus simples pour le lab1
                if(line.equalsIgnoreCase(Constants.DISCONNECT_COMMAND)){
                    System.out.println("Received : '"+line+"' command");
                    disconnect_signal = true;
                    System.out.println("Closing client connection ...");
                }
                else if(line.equalsIgnoreCase(Constants.SHUTDOWN_COMMAND)){
                    System.out.println("Received : '"+line+"' command");
                    shutdown_signal = true;
                } else if(line.equalsIgnoreCase(Constants.GET_TIME_COMMAND)){
                    // Simule un RTT realiste 0~100ms pour eviter de toujours avoir 0-1 ms de RTT en local
                    System.out.println("Received : '"+line+"' command");
                    Thread.sleep((long) (1+Math.random()*100));
                    out.println(getTime());
                    out.flush();
                } else if(line.equalsIgnoreCase(Constants.PING_COMMAND)) {
                    System.out.println("Received : '"+line+"' command");
                    out.println("Hello, this server is online");
                    out.flush();
                }else {
                    //Parse commandes plus complexes pour tests et inconnues
                    StringTokenizer st = new StringTokenizer(line, " ");
                    int nbToken = st.countTokens();
                    if (nbToken > 1) {
                        String command = st.nextToken();
                        if(command.equalsIgnoreCase(Constants.DECODE_COMMAND)){
                            String text = line.substring(7);
                            System.out.println("Received : '"+line+"' command");
                            out.println(text); //üzüm
                            out.flush();
                        }
                    } else {
                        System.out.println("Received : '"+line+"' command");
                        out.println(Constants.UNKNOWN_COMMAND+"'"+line+"'");
                        out.flush();
                    }

                }
            }
        }
    }

    /**
     * Stops the server gracefully
     * @throws IOException connection error with any of the clients
     */
    public void stopServer() throws IOException {
        try{
            System.out.println("Server is shutting down ...");
            this.socket.close();
            System.out.println("Server closed successfully...");
        } catch (IOException e){
            System.out.println("Error while attempting to close the server : " + e.getMessage());
        }
    }

    /**
     * Get the time of the server in miliseconds
     * @return time of the Server in miliseconds
     */
    public long getTime() {
        return System.currentTimeMillis();
    }

    /**
     * Entry point which instancies a Server
     * @param args Servers parameters if any
     *
     */
    public static void main(String[] args) throws IOException {
        Serveur serveur = new Serveur();
        serveur.startServer();
    }
}
