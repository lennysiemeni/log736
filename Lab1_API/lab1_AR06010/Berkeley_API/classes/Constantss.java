package classes;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Object ou on stock les string Constantes qui represente les messages envoyer/recu par nos serveur/clients.
 */
public class Constantss {
	public static final String GET_TIME_COMMAND = "GET Time";
	public static final String DISCONNECT_COMMAND = "DISCONNECT";
	public static final String SHUTDOWN_COMMAND = "SHUTDOWN";
	public static final String UNKNOWN_COMMAND = "UNKNOWN COMMAND";
	public static final String PING_COMMAND = "PING";
	public static final String DECODE_COMMAND = "DECODE";
	public static final Charset TELNET_ENCODING = StandardCharsets.ISO_8859_1;// encoding
	// of telenet
	//public static final String TELNET_ENCODING = "ISO-8859-1";
}
