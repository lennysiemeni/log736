package classes;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Object de Configuration pour un Noeud. On stockera les information pertinante pour une connection comme les socket.
 */
public class NoeudConf {
    public Socket socket;
    public InputStreamReader in;
    public PrintWriter out;
    public BufferedReader bf;
    public int rang;
    public NoeudConf(Socket socket,InputStreamReader in,PrintWriter out,BufferedReader bf,int rang){
        this.socket = socket;
        this.in = in;
        this.out = out;
        this.bf = bf;
        this.rang = rang;
    }

}