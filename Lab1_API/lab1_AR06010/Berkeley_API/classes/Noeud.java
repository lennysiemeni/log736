package classes;

import interfaces.INoeud;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Class Noeud qui implement INoeud et qui represente un noeud
 */
public class Noeud implements INoeud{
    private long time;
    final int portt;
    public long offset;
    private Map<Integer, NoeudConf> node_connected_conf;
    public Map<String, Long> timestamp;
    public Boolean isLeader;
    private ServerSocket listenner;
    public Boolean isTelnet;

    /**
     * Constructor
     * @param porttt Le port number auquelle le Noeud devrait ce connecter
     */
    public Noeud(int porttt)
    {
        this.portt = porttt;
        this.node_connected_conf = new HashMap<Integer, NoeudConf>();
        this.timestamp = new HashMap<String, Long>();
        isLeader = false;
        isTelnet = false;
        offset = 0;

    }
    /**
     * Cette méthode devra enclencher le mécanisme de synchronisation
     * uniquement si le noeud l’invoquant est le dirigeant du réseau. Par la
     * suite, un calcul dérivant la moyenne du temps des horloges recus sera fait et
     * le différentiel approprié sera renvoyé à chaque noeud.
     * @param ports list de ports des autre noeuds
     * @param seuil Le seuil limit auquelle on ignora les temps avec valeur abherante
     * @return List de offsets que ont devrait renvoyer au clients
     * @throws IOException
     */
    public long[] requestTime(int[] ports, long seuil) throws IOException {
        if(isLeader){
            /**Aller chercher tout les heures*/
            ExecutorService es = Executors.newCachedThreadPool();
            int nbClient = ports.length;
            long[] node_time = new long[nbClient];
            for(int i=1;i<=ports.length;i++) {
                int finalRang_node = i;
                es.execute(new Runnable() {
                    public void run() {
                        long temps = 0;
                        try {
                            int port_noeud = ports[finalRang_node-1];
                            NoeudConf confs = node_connected_conf.get(port_noeud);
                            temps = ask_hour(confs,3);
                            //System.out.println("Leader received node " + Integer.toString(finalRang_node) + " time " + Long.toString(temps));
                            node_time[finalRang_node-1] = temps;

                            //nb_cl_time_receive++;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            es.shutdown();
            try {
                boolean finished = es.awaitTermination(1, TimeUnit.MINUTES);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //long[] node_time = new long[ports.length];
            /**Prend en compte seulement les heures des noeud qui ne depassent pas le seuil*/
            Long leader_time = this.time;
            System.out.println("Leader had time "+Long.toString(leader_time));
            ArrayList<Long> time_list = new ArrayList<Long>();
            for(int i = 0;i<ports.length;i++) {
                Long temps = node_time[i];
                //node_time[i] = temps;
                Long ecart = leader_time - temps;
                String index_str = Integer.toString(i+1);
                System.out.println("Node "+index_str+" had time "+Long.toString(temps) + " with ecart "+Long.toString(ecart));
                timestamp.put("ecart_"+index_str,ecart);
                if (Math.abs(ecart) <= seuil) {
                    time_list.add(temps);
                }
            }
            /**Calculer la moyen*/
            int time_keep = time_list.size();
            if(time_keep==0){
                System.out.println("All time exced seuil");
                closeSystem();
                return null;
            }
            Long tmp = leader_time;
            for(int j=0;j<time_keep;j++){
                tmp += time_list.get(j);
            }
            tmp /= (time_keep+1);

            long[] offsets = new long[ports.length];
            for(int i = 0;i<ports.length;i++){
                offsets[i] = tmp - node_time[i];
            }
            /**Envoyer les offset la moyen*/
            Long offset_leader = tmp - leader_time;
            System.out.format("Leader have OFFSET: %d \n",offset_leader);
            setTime(offset_leader);
            for(int i = 0;i<ports.length;i++){

                long offset_nd = offsets[i];
                int port_nd = ports[i];
                NoeudConf conf = node_connected_conf.get(port_nd);
                Socket socket_nd = conf.socket;
                PrintWriter out = conf.out;
                System.out.format("Leader send OFFSET: %d to Node %d\n",offset_nd,socket_nd.getPort());
                String dataSend = "OFFSET "+ Long.toString(offset_nd);
                out.println(dataSend);
                out.flush();
            }


            return offsets;
        }
        return null;
    }
    /**
     *Cette méthode devra démarrer le noeud et ouvrir un « socket »
     * d’écoute sur un port TCP unique pour chaque n client. (25100+100n : n >= 0).
     * Si le noeud est configuré comme dirigeant (« leader »), alors il mettra en oeuvre
     * la communication avec les autres pairs. Sinon, le noeud devra attendre pour
     * la requête de son temps d’horloge local provenant du dirigeant du réseau.
     * @param x Variable number of arguments pour tous les port tcp des clients
     * @return Socket retourne un socket qui est responsable pour la connection du Noeud
     * @throws IOException
     */
    public Socket startNoeud(int ... x) throws IOException {
        int port = x[0];
        if(!isLeader) {
            stay_connected(port);
        }else{
            int nbClient = x[1];
            for(int i=1;i<=nbClient;i++){
                int node_port = port + 100 * i;
                Socket client_socket = new Socket("localhost", node_port);
                PrintWriter out = new PrintWriter(client_socket.getOutputStream());
                InputStreamReader in = new InputStreamReader(client_socket.getInputStream());
                BufferedReader bf = new BufferedReader(in);
                NoeudConf conf = new NoeudConf(client_socket,in,out,bf,i);
                node_connected_conf.put(node_port,conf);
            }
        }

        return null;
    }
    /**
     * Cette méthode devra retourner la valeur du temps d’horloge local.
     * @return long qui represente l'heure du noeud
     */
    public long getTime() {
        return System.currentTimeMillis() + offset;
    }

    /**
     *Cette méthode devra modifier la valeur en mémoire du temps
     * d’horloge local.
     * @param offset long qui represente la nouvelle heure du noeud
     */
    public void setTime(long offset) {
        timestamp.put("befor_adjust",getTime());
        this.offset = offset;
        timestamp.put("after_adjust",getTime());
    }

    /**
     * Method Utiliser par le leader pour collecter les temps des autre noeuds
     * @param conf NoeudConf pour la configuration de la connection
     * @param numberOfTries Nombre Maximal d'essei pour contacter les autres noeuds
     * @return le temps finale en numeric (long)
     * @throws IOException
     */
    public long ask_hour(NoeudConf conf,int numberOfTries) throws IOException {
        ArrayList<Long> server_ts = new ArrayList<Long>();
        ArrayList<Long> rtt_requests = new ArrayList<Long>();
        ArrayList<Long> leaderTime = null;
        if(conf.rang == 1){
            leaderTime = new ArrayList<Long>();
        }

        int count = 0;
        long temps = 0;
        long rtt_send = 0;
        long rtt_receive = 0;
        long rtt = 0;
        //System.out.println("Leader request : GET Time");
        try{
            String str ="";
            while(count < numberOfTries){
                rtt_send = getTime();
                conf.out.println("GET Time");
                conf.out.flush();
                str = conf.bf.readLine();
                rtt_receive = getTime();
                rtt = rtt_receive - rtt_send;
                //System.out.println("Node "+Integer.toString(clientId)+" | Time : "+str+" ms. | rtt : "+rtt+" ms. | count : "+count);
                server_ts.add(Long.parseLong(str));
                rtt_requests.add(rtt);
                if(conf.rang==1){
                    leaderTime.add(rtt_send);
                }
                count++;
            }
            int position_min =0;
            for (int i = 0; i < rtt_requests.size(); i++) {
                if(rtt_requests.get(position_min) > rtt_requests.get(i)) position_min = i;
            }
            long rtt_min = rtt_requests.get(position_min);
            long server_ts_min_rtt = server_ts.get(position_min);
            temps = server_ts_min_rtt + (rtt_min/2);;
            if(conf.rang==1){
                this.time = leaderTime.get(position_min);
            }
            //System.out.println("Node "+Integer.toString(clientId)+" | Time : "+str+" ms. | rtt : "+rtt+" ms. | count : "+count);
        } catch (Exception e){
            e.printStackTrace();
        }
        return temps;
    }

    /**
     * Assurer que la connection reste ouverte avec le port specifier
     * @param port Port auquelle il faut rester connecter
     */
    public void stay_connected(int port){
        Socket socket = null;
        //ServerSocket listen_soc = null;
        System.out.println("Node "+Integer.toString(port)+ " is listening.");
        try {
            listenner = new ServerSocket(port);
            try {
                while (true) {
                    try {
                        socket = listenner.accept();
                        System.out.println("Node "+Integer.toString(port)+" connected with leader.");
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Connection Error");
                    }
                    InputStreamReader in = new InputStreamReader(socket.getInputStream());
                    PrintWriter out = new PrintWriter(socket.getOutputStream());
                    BufferedReader bf = new BufferedReader(in);
                    int port_connected = socket.getLocalPort();

                    NoeudConf conf = new NoeudConf(socket,in,out,bf,0);
                    node_connected_conf.put(port_connected,conf);
                    client_tcp(conf);
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Server error");
        }

    }

    /**
     * Le noeud client recevra le NoeudConf du leader et pourra communiquer avec le leader
     * @param conf NoeudConf pour connecter un client
     */
    public void client_tcp(NoeudConf conf) throws IOException {

        String str = null;
        int ndport = conf.socket.getLocalPort();
        String ndport_str = String.valueOf(ndport);
        boolean client_closed_connection = false;
        boolean shutdown_signal_received = false;

        try {
            loopperso :
            while (true) {
                //While at least one client is alive
                str =  conf.bf.readLine();
                if(isTelnet){
                    //TODO TelNet Communication
                    byte[] dataCopy = str.getBytes();
                    str = new String(dataCopy, Constantss.TELNET_ENCODING);
                }
                if (str == null) {
                    conf.socket.close();
                } else if (str.equalsIgnoreCase("GET TIME")) {
                    // Simule un RTT realiste 0~100ms pour eviter de toujours avoir 0-1 ms de RTT en local
                    //System.out.println("Node "+ndport_str+" received : '" + str + "' command");
                    //Thread.sleep((long) (1 + Math.random() * 100));
                    long temps = getTime();
                    String temps_str = Long.toString(temps);
                    if(isTelnet){
                        //TODO TelNet Communication
                        byte[] dataCopy = temps_str.getBytes();
                        temps_str = new String(dataCopy, Constantss.TELNET_ENCODING);
                    }
                    conf.out.println(temps_str);
                    timestamp.put("time_send",temps);
                    //pr.println(server.getTime());
                    conf.out.flush();

                } else {
                    StringTokenizer st = new StringTokenizer(str, " ");
                    int nbToken = st.countTokens();
                    if (nbToken > 1) {
                        String command = st.nextToken();
                        if (command.equalsIgnoreCase("OFFSET")) { // retrieves the stored value
                            // for a given key
                            timestamp.put("offset_receive",getTime());
                            String offset_str = st.nextToken();
                            long offset = Long.parseLong(offset_str);
                            setTime(offset);
                            System.out.format("Noed %d received OFFSET %d\n",conf.socket.getLocalPort(),offset);
                        }
                    } else {
                        //System.out.format("Nombre token: %d\n", nbToken);

                        switch (str.toUpperCase()) {
                            case "DISCONNECT":
                                //System.out.println("Node "+ndport_str+" received : '" + str + "' command");
                                client_closed_connection = true;
                                break;
                            case "SHUTDOWN":
                                System.out.println("Node "+ndport_str+" received : '" + str + "' command");
                                shutdown_signal_received = true;

                                //this.getThreadGroup().destroy();
                                break loopperso;
                            default:
                                conf.out.println("Unknown command : '" + str + "'");
                                conf.out.flush();
                                break;
                        }
                    }
                }
            }
            if(shutdown_signal_received|client_closed_connection){
                conf.bf.close();
                conf.out.close();
                conf.in.close();
                conf.socket.close();
            }
        } catch (IOException e) {
            System.out.println("Server error: " + e.getMessage());
            e.printStackTrace();
            try {
                conf.socket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    /**
     * Fonction qui va fermer tous les connection a tous les noeuds
     */
    public void closeSystem(){
        ArrayList<NoeudConf> valueList = new ArrayList<NoeudConf>(node_connected_conf.values());
        System.out.println("Leader is closing all connections");
        for(int i = 0;i<valueList.size();i++){
            NoeudConf conf = valueList.get(i);
            closeNode(conf);

        }
    }

    /**
     * Fermer la connection a un Noeud
     * @param conf NoeudConf pour le noeud qui devrait etre fermer
     */
    public void closeNode(NoeudConf conf){
        Socket socket_nd = conf.socket;
        PrintWriter out = conf.out;
        String dataSend = "SHUTDOWN";
        out.println(dataSend);
        out.flush();
        try {
            conf.bf.close();
            conf.in.close();
            out.close();
            socket_nd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}