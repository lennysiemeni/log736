package classes;

import java.io.IOException;

public class MainBerkeley {
    public static void main(String[] args) throws IOException {
        //Socket client_socket = new Socket("localhost", 25000);
        /** On suppose que le leader est deja elu */
        System.out.println("Port élu: 25100");
        Noeud nd1 = new Noeud(25200);
        Noeud nd2 = new Noeud(25300);
        Noeud nd3 = new Noeud(25400);
        nd3.isTelnet = true;
        new Thread(() -> {
            try {
                nd1.startNoeud(25200);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                nd2.startNoeud(25300);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                nd3.startNoeud(25400);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
        Noeud ld;

        ld = new Noeud(25100);
        ld.isLeader = true;
        ld.startNoeud(25100,3);


        int offset_nd1 = -30; int offset_nd2 = 40; int offset_nd3 = 60;
        nd1.offset = offset_nd1; nd2.offset = offset_nd2; nd3.offset = offset_nd3;
        int[] ports = {25200,25300,25400};
        int seuil = 100;

        System.out.println("Debut synchronisation");
        ld.requestTime(ports,seuil);
        System.out.println("Fin synchronisation");
        ld.closeSystem();
        Long nd1_t1 = nd1.timestamp.get("time_send");
        Long nd1_t2 = nd1.timestamp.get("after_adjust");
        System.out.println("Noeud 1 temps synchro= avant: "+nd1_t1+" apres: "+nd1_t2);
        Long nd2_t1 = nd1.timestamp.get("befor_adjust");
        Long nd2_t2 = nd1.timestamp.get("after_adjust");
        Long difference = nd2_t2 - nd2_t1;
        System.out.println("Noeud 2 difference temps apres ajustement: "+difference);
        Long ecart_nd1 = ld.timestamp.get("ecart_1");
        if(Math.abs(offset_nd1) == Math.abs(ecart_nd1)){
            System.out.println("System a reçu l'heure exacte de la part de Noeud 1");
        }
        else{
            System.out.println("Non"+ Math.abs(offset_nd1) +" Suivant "+Math.abs(ecart_nd1));
        }









    }
}