package interfaces;

import java.io.IOException;
import java.net.Socket;

public interface INoeud {
    public long[] requestTime(int[] ports, long seuil) throws IOException;
    public Socket startNoeud(int ... x) throws IOException;
    public long getTime();
    public void setTime(long offset);
}
