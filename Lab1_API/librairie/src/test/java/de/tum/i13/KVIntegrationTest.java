package librairie.src.test.java.de.tum.i13;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

//import org.junit.jupiter.api.Test;

public class KVIntegrationTest {

    public static Integer port = 25200;

    public String doRequest(Socket s, String req) throws IOException {
        PrintWriter output = new PrintWriter(s.getOutputStream());
        BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
        System.out.println("Client write: "+req);
        output.write(req + "\r\n");
        output.flush();

        String res = input.readLine();
        System.out.println("Client read: "+res);
        return res;
    }

    public String doRequest(String req) throws IOException {
        Socket s = new Socket();
        s.connect(new InetSocketAddress("127.0.0.1", port));
        String res = doRequest(s, req);
        s.close();

        return res;
    }

//    @Test
//    public void doing_a_few_requests() throws IOException, InterruptedException {
//        Thread th = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    librairie.src.main.java.de.tum.i13.server.nio.StartSimpleNioServer.main(new String[]{port.toString()});
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//        th.start(); // started the server
//        Thread.sleep(2000);
//
//        for (int tcnt = 0; tcnt < 2; tcnt++){
//            final int finalTcnt = tcnt;
//            new Thread(){
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(finalTcnt * 100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        for(int i = 0; i < 100; i++) {
//                            Socket s = new Socket();
//                            s.connect(new InetSocketAddress("127.0.0.1", port));
//                            assertThat(doRequest(s, "PUT table key"+ finalTcnt +" valuetest" + i), containsString("OK"));
//                            assertThat(doRequest(s, "GET table key" + finalTcnt), containsString("valuetest" + i));
//                            s.close();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }.start();
//        }
//
//
//
//        //Assert.assertThat(doRequest("GET table key"), containsString("valuetest"));
//
//        Thread.sleep(5000);
//        th.interrupt();
//    }
    @Test
    public void doing_a_few_requests2() throws IOException, InterruptedException {
        Thread th = new Thread() {
            @Override
            public void run() {
                try {
                    librairie.src.main.java.de.tum.i13.server.nio.StartSimpleNioServer.main(new String[]{port.toString()});
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        th.start(); // started the server
        Thread.sleep(2000);

        for (int tcnt = 0; tcnt < 2; tcnt++){
            final int finalTcnt = tcnt;
            new Thread(){
                @Override
                public void run() {
                    try {
                        Thread.sleep(finalTcnt * 100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    try {
                        for(int i = 0; i < 5; i++) {
                            Socket s = new Socket();
                            s.connect(new InetSocketAddress("127.0.0.1", port));
                            //assertThat(doRequest(s, "PUT table key"+ finalTcnt +" valuetest" + i), containsString("OK"));
                            doRequest(s, "PUT table key"+ finalTcnt +" valuetest" + i);
                            doRequest(s, "GET table key" + finalTcnt);
                            //assertThat(doRequest(s, "GET table key" + finalTcnt), containsString("valuetest" + i));
                            s.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }



        //Assert.assertThat(doRequest("GET table key"), containsString("valuetest"));

        Thread.sleep(5000);
        th.interrupt();
    }

//    @Test
//    public void doing_a_few_request_with_always_new_connections() throws Exception{
//        for (int tcnt = 0; tcnt < 3; tcnt++){
//            final int finalTcnt = tcnt;
//            int finalTcnt1 = tcnt;
//            new Thread(() -> {
//                try {
//                    Thread.sleep(finalTcnt1 * 100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    for(int i = 0; i < 100000; i++) {
//                        assertThat(doRequest("PUT table key"+ finalTcnt +" valuetest" + i), containsString("OK"));
//                        assertThat(doRequest("GET table key" + finalTcnt), containsString("valuetest" + i));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }).start();
//        }
//    }
}
