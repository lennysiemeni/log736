package librairie.src.main.java.de.tum.i13.server.nio;

import java.io.IOException;

/**
 * Created by chris on 14.10.14.
 */
public class rouler {

    public static void main(String[] args) throws IOException, InterruptedException {
        SimpleNioServer sn = new SimpleNioServer();

//        Integer port = 5559;
//        if (args.length == 1) {
//            port = Integer.parseInt(args[0]);
//        }
        Integer port = 8090;
        sn.bindSockets("127.0.0.1", port);
        port = 8091;
        sn.bindSockets("127.0.0.1", port);

        Runnable serveur = new Runnable() {
            @Override
            public void run() {
                try {
                    sn.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };

        ClientNio client1 = new ClientNio(8090,"Salut");
        ClientNio client2 = new ClientNio(8091,"Bonjour");
        new Thread(serveur).start();
        client1.startClient();
        client2.startClient();

    }
}

