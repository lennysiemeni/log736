package librairie.src.main.java.de.tum.i13.server.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class ClientNio {
    public int port;
    public String messages;

    public ClientNio(int portt, String message){
        this.port = portt;
        this.messages = message;
    }
    public void startClient()
            throws IOException, InterruptedException {

        InetSocketAddress hostAddress = new InetSocketAddress("localhost", port);
        SocketChannel client = SocketChannel.open(hostAddress);

        System.out.println("Client... started");

        String threadName = Thread.currentThread().getName();

//        // Send messages to server
//        String [] messages = new String []
//        		{threadName + ": test1",threadName + ": test2",threadName + ": test3"};


        byte [] message = new String(messages).getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(message);
        client.write(buffer);
        //System.out.println(messages);
        buffer.clear();
        Thread.sleep(5000);

        client.close();
    }
}

