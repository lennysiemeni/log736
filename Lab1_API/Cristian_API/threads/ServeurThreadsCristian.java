package threads;

import shared.Constants;
import server.Serveur;
import interfaces.IServeur;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * Cette classe permet d'instancier un nouveau thread pour le serveur
 * permettant ainsi a client d'interagir de maniere parallele meme si il y a plusieurs clients connectes
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 */
public class ServeurThreadsCristian extends Thread implements IServeur{
    protected Socket socket;
    private boolean shutdown_server_thread = false;
    private boolean client_closed_connection = false;

    /**
     * Class constructor
     * @param clientSocket socket du client connecte
     */
    public ServeurThreadsCristian(Socket clientSocket) {
        this.socket = clientSocket;
    }

    /**
     * Runnable Thread instance of ServerThread
     */
    public void run(){

        String line=null;
        BufferedReader br=null;
        BufferedReader is=null;
        PrintWriter os=null;
        try{
            InputStreamReader in = new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING);
            BufferedReader bf = new BufferedReader(in);
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
            while(!shutdown_server_thread && !client_closed_connection){
                //While at least one client is alive
                line = bf.readLine();
                if(line == null){
                    client_closed_connection = true;
                    System.out.println("Closing client connection ...");
                    break;
                } else {
                    //Parse les commandes les les plus simples pour le lab1
                    if(line.equalsIgnoreCase(Constants.DISCONNECT_COMMAND)){
                        System.out.println("Received : '"+line+"' command");
                        client_closed_connection = true;
                        System.out.println("Closing client connection ...");
                    }
                    else if(line.equalsIgnoreCase(Constants.SHUTDOWN_COMMAND)){
                        System.out.println("Received : '"+line+"' command");
                        shutdown_server_thread = true;
                    } else if(line.equalsIgnoreCase(Constants.GET_TIME_COMMAND)){
                        // Simule un RTT realiste 0~100ms pour eviter de toujours avoir 0-1 ms de RTT en local
                        System.out.println("Received : '"+line+"' command");
                        Thread.sleep((long) (1+Math.random()*100));
                        out.println(getTime());
                        out.flush();
                    } else if(line.equalsIgnoreCase(Constants.PING_COMMAND)) {
                    System.out.println("Received : '"+line+"' command");
                    out.println("Hello, this server is online");
                    out.flush();
                    }else {
                        //Parse commandes plus complexes pour tests et inconnues
                        StringTokenizer st = new StringTokenizer(line, " ");
                        int nbToken = st.countTokens();
                        if (nbToken > 1) {
                            String command = st.nextToken();
                            if(command.equalsIgnoreCase(Constants.DECODE_COMMAND)){
                                String text = line.substring(7);
                                System.out.println("Received : '"+line+"' command");
                                out.println(text); //üzüm
                                out.flush();
                            }
                        } else {
                            System.out.println("Received : '"+line+"' command");
                            out.println(Constants.UNKNOWN_COMMAND+"'"+line+"'");
                            out.flush();
                        }

                    }
                }
            }
        } catch (IOException | InterruptedException e){
            System.out.println("Server error: " + e.getMessage());
            e.printStackTrace();
            try {
                socket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } finally {
            if(client_closed_connection || shutdown_server_thread){
                try {
                    socket.close();
                    System.out.println("Client socket closed");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Methode pour arreter le thread
     *
     */
    public void stopServer() throws java.io.IOException {}

    /**
     * Methode pour lancer le thread
     *
     */
    public void startServer()throws java.io.IOException {}

    /**
     * Get the time of the server in miliseconds
     * @return time of the Server in miliseconds
     */
    public long getTime() {
        return System.currentTimeMillis();
    }

    /**
     * Get the value of the shutddown Signal
     * @return this.shutdown_server_thread
     */
    public boolean getShutdownSignal(){
        return this.shutdown_server_thread;
    }

    /**
     * Get the value of the disconnect Signal from a client.
     * Allows the server to be notified if a client wants to close the connection
     * so it can close the client_socket
     * @return this.client_closed_connection
     */
    public boolean getClientClosedSignal(){
        return this.client_closed_connection;
    }

}
