package client;
import interfaces.IClient;
import shared.Constants;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Cette classe permet d'instancier le client TCP TELNET
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
public class Client implements IClient {
    public static final int SERVER_PORT = 25000;
    private long time = System.currentTimeMillis(); //default assuming the client has a functioning clock of course...
    private long rtt_min = 0; //default value
    private long latency = 0; //default value
    private int accuracy = 0; //default value
    public final ArrayList<Long> server_ts = new ArrayList<Long>();
    public final ArrayList<Long> rtt_requests = new ArrayList<Long>();
    public final Socket client_socket = new Socket("localhost", SERVER_PORT);

    private final PrintWriter out = new PrintWriter(new OutputStreamWriter(client_socket.getOutputStream(), Constants.TELNET_ENCODING));
    private final InputStreamReader in = new InputStreamReader(client_socket.getInputStream(), Constants.TELNET_ENCODING);
    private final BufferedReader bf = new BufferedReader(in);

    /**
     * Class Constructor
     * @throws IOException connection error with the server
     */
    public Client() throws IOException {}

    /**
     * Sends multiple time requests to the server specified by the socket and server port
     * @param serverPort port of the server
     * @param currentTime time of the client
     * @param numberOfTries number of requests to send to the server
     * @throws IOException connection error with the server
     */
    public void requestTime(int serverPort, long currentTime, int numberOfTries) throws IOException {
        int count = 0;
        long rtt_send = 0;
        long rtt_receive = 0;
        long rtt = 0;
        //Simple formatting to see results in the console of the client instance
        //help : https://stackoverflow.com/questions/909843/how-to-get-the-unique-id-of-an-object-which-overrides-hashcode
        System.out.println("ClientID "+System.identityHashCode(this)+" request :"+Constants.GET_TIME_COMMAND);
        try{
            String str ="";
            while(count < numberOfTries){
                //l'algorithme de Cristian Selon ce qui a ete vu en cours
                rtt_send = System.currentTimeMillis();
                out.println(Constants.GET_TIME_COMMAND);
                out.flush();
                str = bf.readLine();
                rtt_receive = System.currentTimeMillis();
                rtt = rtt_receive - rtt_send;
                System.out.println("ClientID : "+System.identityHashCode(this)+" | Time : "+str+" ms. | rtt : "+rtt+" ms.  | count : "+(count+1));
                this.server_ts.add(Long.parseLong(str));
                this.rtt_requests.add(rtt);
                count++;
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally{
            try {
                close(client_socket, out, in, bf);
                System.out.println("ClientID : "+System.identityHashCode(this)+" is disconnected from server");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("");
    }

    /**
     * Computes the accuracy of RTT, the new computed time and displays the results
     * before and after adjustement in the client console
     */
    public void showResults(){
        System.out.println("Time BEFORE synchronisation : "+getTime()+" ms");
        System.out.println("Accuracy : ±"+getAccuracy()+" ms");
        System.out.println("Time AFTER synchronisation : "+getTime()+" ms\n");
    }

    /**
     * Closes the client socket gracefully
     * @param client_socket client socket
     * @param pr PrintWriter output of the client
     * @param in InputStream of the client
     * @param bf BufferReader of the client
     * @throws Exception connection error while closing
     */
    public void close(Socket client_socket, PrintWriter pr, InputStreamReader in, BufferedReader bf) throws Exception {
        pr.close();
        in.close();
        bf.close();
        client_socket.close();
    }


    /**
     * Get the client socket usefull for JUnit tests
     * @return client_socket
     */
    public Socket getClient_socket() {
        return client_socket;
    }

    /**
     * Get the client output
     * @return out client PrintWriter
     */
    public PrintWriter getOut() {
        return out;
    }

    /**
     * Get the client input
     * @return in client InputStreamReader
     */
    public InputStreamReader getIn() {
        return in;
    }

    /**
     * Get the client Buffer
     * @return bf client BufferReader
     */
    public BufferedReader getBf() {
        return bf;
    }

    /**
     * Get the latency client and server determined by the client
     * @return this.latency
     */
    public long getLatency() {
        return this.latency;
    }

    /**
     * Set the latency between the client and server
     * @param newLatency latency between the client and server
     */
    public void setLatency(long newLatency) { this.latency = newLatency; }

    /**
     * Get time of the client clock
     * @return time in milliseconds
     */
    public long getTime() {
        return this.time;
    }

    /**
     * Set time of the client clock
     * @param newTime new time to set to
     */
    public void setTime(long newTime) {
        this.time = newTime;
    }

    /**
     * Set computed accuracy to desired value
     * @param lastComputedAccuracy computed accuracy
     */
    public void setAccuracy(int lastComputedAccuracy){this.accuracy = lastComputedAccuracy;}

    /**
     * Computes the accuracy and sets new computed time to client clock
     * @return accuracy of the new computed time
     */
    public int getAccuracy() {
        //First get min RTT from all RTT requests and get the serveur time at the corresponding index

        int position_min =0;
        // Solution pas tres propre mais permet de coordonner le tableau en obtenant le ts_min correspondant au rtt_min en O(n)
        for (int i = 0; i < rtt_requests.size(); i++) {
            if(rtt_requests.get(position_min) > rtt_requests.get(i)) position_min = i;
        }
        long rtt_min = rtt_requests.get(position_min);
        long server_ts_min_rtt = server_ts.get(position_min);

        //On a juge qu'il valait mieux mettre a jour automatiquement le temps des que l'on calcule la precision
        //pour eviter le "Time drifting"
        //Par exemple il est peu pertiant de s'apercevoir que la precision change, sans ajuster
        //l'horloge a chaque fois...
        setTime(server_ts_min_rtt + (rtt_min/2));
        return Math.abs((int) ((rtt_min/2) - latency));
    }

    /**
     * Entry point of the class. Clients are instantiated from here
     * @param args program arguments if any
     * @throws IOException errors with the server
     */
    public static void main(String[] args) throws IOException {
        Client cl1 = new Client();
        cl1.requestTime(SERVER_PORT,cl1.getTime(),6);
        cl1.showResults();
        Client cl2 = new Client();
        cl2.requestTime(SERVER_PORT,cl2.getTime(),5);
        cl2.showResults();
        Client cl3 = new Client();
        cl3.requestTime(SERVER_PORT,cl2.getTime(),12);
        cl3.showResults();

    }
}
