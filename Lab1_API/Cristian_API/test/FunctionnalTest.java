package test;

import client.Client;
import org.junit.Rule;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Order;
import org.junit.rules.ExpectedException;
import shared.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.*;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


public class FunctionnalTest {
    Client client;

    @BeforeEach
    public void setUp() throws Exception {
        client = new Client();
    }

    @Test
    @Order(1)
    public void test_command_should_not_be_understood() throws IOException {
        client.getOut().println("test");
        client.getOut().flush();
        String str = "";
        str = client.getBf().readLine();
        assert(str.contains("UNKNOWN COMMAND'test'"));
    }

    @Test
    @Order(2)
    public void test_time_exo_1(){
        //donnees de la correction des exercices 1 du cours
        List<Long> rtt_test = new ArrayList<Long>();
        List<Long> server_ts_test = new ArrayList<Long>();
        rtt_test.add((long) 22);
        rtt_test.add((long) 25);
        rtt_test.add((long) 20);
        server_ts_test.add((long) 36623000);
        server_ts_test.add((long) 36625000);
        server_ts_test.add((long) 36628000);
        client.rtt_requests.addAll(rtt_test);
        client.server_ts.addAll(server_ts_test);

        assertEquals(client.getAccuracy(), 10);
        assertEquals(client.getTime(), 36628010); // 10:54:28:010
        client.setLatency(8);
        assertEquals(client.getAccuracy(), 2);

    }

    @Test
    @Order(3)
    public void server_is_using_telnet_protocol() throws IOException{
        //Special edge case caracters found here : https://stackoverflow.com/questions/652161/how-do-i-convert-between-iso-8859-1-and-utf-8-in-java
        String encodedWithISO88591 = "Ã¼zÃ¼m baÄlarÄ±";
        String decodedWithISO = new String(new String(encodedWithISO88591.getBytes(), Constants.TELNET_ENCODING));
        String encodedToUTF8 = "üzüm ba?lar?";
        String decodedToUTF8 = new String(encodedWithISO88591.getBytes(Constants.TELNET_ENCODING), StandardCharsets.UTF_8); //üzüm ba?lar?
        client.getOut().println("DECODE "+encodedWithISO88591);
        client.getOut().flush();
        String str = "";
        str = client.getBf().readLine();
        assertEquals(str, encodedWithISO88591); //"Ã¼zÃ¼m baÄlarÄ±"

        client.getOut().println("DECODE "+decodedToUTF8);
        client.getOut().flush();
        str = client.getBf().readLine();
        assertEquals(str, encodedToUTF8); //"üzüm ba?lar?"
    }

    @Test
    @Order(4)
    public void rtt_minimum(){
        ArrayList<Long> rtt_test = new ArrayList<Long>();
        ArrayList<Long> server_ts_test = new ArrayList<Long>();
        rtt_test.add((long) 250);
        rtt_test.add((long) 112);
        rtt_test.add((long) 20);
        server_ts_test.add((long) 36623000);
        server_ts_test.add((long) 36625000);
        server_ts_test.add((long) 36628000);
        client.rtt_requests.addAll(rtt_test);
        client.server_ts.addAll(server_ts_test);

        int real_rtt = client.getAccuracy();
        int expected_rtt = Math.abs((int) ((20/2) - client.getLatency()));
        assertEquals(real_rtt, expected_rtt);
    }

    @Test
    @Order(5)
    public void rtt_minimmum_duplicat(){
        ArrayList<Long> rtt_test = new ArrayList<Long>();
        ArrayList<Long> server_ts_test = new ArrayList<Long>();
        rtt_test.add((long) 16);
        rtt_test.add((long) 15);
        rtt_test.add((long) 15);
        server_ts_test.add((long) 36625000);
        server_ts_test.add((long) 36625000);
        server_ts_test.add((long) 36625000);
        client.rtt_requests.addAll(rtt_test);
        client.server_ts.addAll(server_ts_test);

        int real_rtt = client.getAccuracy();
        int expected_rtt = Math.abs((int) ((15/2) - client.getLatency()));
        assertEquals(real_rtt, expected_rtt);
    }


    @AfterEach
    public void close() throws Exception{
        client.close(client.getClient_socket(), client.getOut(), client.getIn(), client.getBf());
    }

}
