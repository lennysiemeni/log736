package interfaces;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.time.format.DateTimeFormatter;

public interface IServeur {
    public void startServer()throws java.io.IOException;
    public void stopServer() throws java.io.IOException;
    public long getTime();

}
