package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Ami {

	public int id;

	private final int port;
	public int client_port;

	public int nb_friend;
	public List<Integer> ami_port;
	public Map<Integer, Connection> outcall_connected;

	public ServerSocket listenner;
	public Thread thread_listennerIncome;

	private Acceptor acceptor;
	public Learner learner;
	public boolean isLearner;
	private Proposer proposer;
	//private boolean isLeader;
	public int leader_port;

	public boolean is_election_leader;
	public Semaphore election_leader_mutex;
	public Semaphore election_learner_mutex;
	private Semaphore wait_mutex;

	public boolean panne_learner;
	public boolean panne_acceptor;
	public boolean panne_leader;
	public boolean panne_leader_detected;

	public Ami(int id, int nb_friend, int[] learner_id) throws IOException {
		this.id = id;
		this.client_port = calculPort(0);
		this.nb_friend = nb_friend;
		this.port = calculPort(id);
		this.leader_port = -1;
		this.panne_leader_detected = false;
		this.outcall_connected = new HashMap<Integer, Connection>();
		this.acceptor = new Acceptor(this);
		this.proposer = null;
		this.panne_acceptor = false;
		this.panne_learner = false;
		this.learner = null;
		setLearner_id(learner_id);
		thread_listennerIncome = new Thread(new Runnable() {
			@Override
			public void run() {
				stay_connected();
			}
		});
		thread_listennerIncome.start();

		ami_port = new ArrayList<Integer>();
		for(int i=1;i<=nb_friend;i++)
			if(i !=id){
				int port_ami = calculPort(i);
				ami_port.add(port_ami);
			}
		election_leader_mutex = new Semaphore(1);
		is_election_leader = false;
		election_learner_mutex = new Semaphore(1);
	}
	public int calculPort(int id){
		return 25100 + id * 100;
	}
	public int portToId(int port_convert){
		return (port_convert - 25100) / 100;
	}
	public void set_self_leader(){
		if(proposer == null)
			proposer = new Proposer(this);
		else
			proposer.reset();

		//this.isLeader = true;
		panne_leader_detected = false;

		try {
			connectAllPeers();
			annonceLeaderAll();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setLearner_id(int ... learnerId){
		List<Integer> learner_port = new ArrayList<Integer>();
		for(int l_id : learnerId){
			if(this.id == l_id){
				this.isLearner = true;
				this.learner = new Learner(this);
			}
			else
				learner_port.add(calculPort(l_id));
		}
		acceptor.setLearner_port(learner_port);
	}
	public void stay_connected(){
		Socket socket = null;
		//System.out.println("Client "+Integer.toString(port)+ " is listening.");
		try {
			listenner = new ServerSocket();
			//bind the socketserver only to localhost
			listenner.bind(new InetSocketAddress("127.0.0.1", port));
			try {
				while (true) {
					try {
						socket = listenner.accept();
					} catch (Exception e) {
						//System.out.format("Ami id %d closing server\n",id);
						break;
					}
					Connection conf = new Connection(socket);
					//incall_connected.put(port_connected,conf);
					int port_connected = socket.getPort();
					String port_str = Integer.toString(port_connected);
					Thread thread = new Thread("Client "+port_str) {
						public void run(){
							int errCode = 1;
							String answer = null;
							while(errCode > 0 ) {
								if(errCode == 1){
									errCode = -1;
									answer = conf.read();
									if(answer != null){
										errCode = process(answer, conf);
									}

								}

								else
									if(errCode == 2){
										answer = wait_promise_reaction(conf);
										if(answer == null){
											System.out.format("acceptor id %d detect leader breackdown\n",id);
											panne_leader_detected = true;
											try {
												election_leader_mutex.acquire();
											} catch (InterruptedException e) {
												e.printStackTrace();
											}
											if(!is_election_leader){
												is_election_leader = true;
												election_leader_mutex.release();
												Thread tmp_thread = new Thread(new Runnable() {
													@Override
													public void run() {
														startElectionLeader();
													}
												});

												tmp_thread.start();
											}else{
												election_leader_mutex.release();
											}
											errCode = 1;
										}else{
											errCode = process(answer, conf);
										}

									}

								if (errCode == -1) {
									//TODO HandleError
								}
							}
							conf.terminate(null);
						}
					};
					thread.start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Server error");
		}

	}
	public Map<Integer, Connection> getOutcall_connected(){
		return outcall_connected;
	}
	public String listenAmi(Connection conf){
		return conf.read();
	}

	public String wait_promise_reaction(Connection con){
		//System.out.format("Acceptor id %d waiting for leader promise reaction\n",id);
		Semaphore detector_mutex = new Semaphore(1);
		String[] answer = {null};
		Thread thread = new Thread("Thread "+con.socket.getPort()) {
			public void run(){
				try {
					detector_mutex.acquire();
					answer[0] = con.read();
					detector_mutex.release();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} }};
		thread.start();
		try {
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int timeout_compt=0;
		int time_wait = 15;
		int timeout_tolere_limit = 0;
		while(true){
			try {
				boolean isAcquired = detector_mutex.tryAcquire(time_wait, TimeUnit.SECONDS); /** If the specified waiting time elapses then the value false returned.  */
				if(isAcquired || timeout_tolere_limit < timeout_compt)
					break;
				timeout_compt++;
			} catch (InterruptedException e) {
				System.out.format("The current thread is interrupted\n");
				e.printStackTrace(); } }
		return answer[0];
	}

	public void startElectionLeader(){
		Thread thread_principal = new Thread("Thread_one") {
			public void run(){
				electionLeader();

			}
		};
		thread_principal.start();
		try {
			thread_principal.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void electionLeader() {
		System.out.println("Ami avec id: "+id+" start electionLeader");
		try {
			connectAllPeers();
		} catch (IOException e) {
			e.printStackTrace();
		}
		wait_mutex = new Semaphore(nb_friend-1);
		int[] receive_stop = {0};
		for(Connection con : outcall_connected.values()){
			con.write("ELECTION&LEADER&"+ id);
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			int id_tmp = this.id;
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					try {
						wait_mutex.acquire();
						String answer = con.read();
						if(answer != null && answer.equals("ANSWER")){
							//System.out.format("Ami id %d receive ANSWER\n",id_tmp);
							receive_stop[0] = 1;
							wait_mutex.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			thread.start();
		}
		try {
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(true){
			try {
				wait_mutex.tryAcquire(10, TimeUnit.SECONDS);
				terminate_all(null);
				if(receive_stop[0]==0){
					System.out.println("Ami avec id: "+ id+" est elu Leader");
					set_self_leader();

				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;

		}


	}
	public void annonceLeaderAll(){
		String id_string = String.valueOf(id);
		for(Connection con : outcall_connected.values())
			con.write("COORDINATION&LEADER&"+id_string);
	}

	public int process(String message, Connection conf) {
		StringTokenizer st = new StringTokenizer(message,"&\r\n");
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str;
		String value;
		int proposal_numb;
		switch(command.toUpperCase()) {
			case "SHUTDOWN ALL":
				//System.out.format("Ami id %d receive SHUTDOWN ALL\n",this.id);
				try {
					terminate_all(null);
					listenner.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case "SHUTDOWN":
				//System.out.format("Ami id %d receive SHUTDOWN\n",this.id);
				break;
			case "CLIENT":
				if(nbToken < 2)
					return -1;
				str = st.nextToken();
				if(str.equals("SHUTDOWN ALL")){
					System.out.format("Leader id %d  receive ClientReq(%s)\n",this.id,str);

					try {
						terminate_all("SHUTDOWN ALL");
						this.listenner.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return 0;
				}
				String tmp = str;

				StringTokenizer client_command = new StringTokenizer(tmp,"/");
				if(client_command.countTokens() < 3)
					return -1;
				proposer.value_decided = str;
				conf.write("OK");
				System.out.format("Leader id %d  receive ClientReq(%s)\n",this.id,str);
				proposer.sendtmp();
				return 0;
			case "COORDINATION":
				if(nbToken < 3)
					return -1;
				str = st.nextToken();
				String str_id_coordination = st.nextToken();
				int id_coordination = Integer.valueOf(str_id_coordination);
				int ports = calculPort(id_coordination);
				//System.out.format("Ami id %d  receive COORDINATION %s from id %d\n",this.id,str,id_coordination);
				if (str.equalsIgnoreCase("LEARNER"))
					acceptor.addLearner(ports);
				else if(str.equalsIgnoreCase("LEADER")){
					setLeader_port(ports);
					if(isLearner)
						learner.sendClientLeaderPort(ports);
					return 1; /** 1 pour continuer à ecouter */
				}

				else
					return -1;
				break;
			case "ELECTION":
				if(nbToken < 3)
					return -1;
				str = st.nextToken();
				String str1 = st.nextToken();
				int id_ami = Integer.parseInt(str1);
				if(str.equalsIgnoreCase("LEADER")&& id > id_ami){
					//System.out.format("Ami id %d receive ELECTION LEADER from ami id %d\n",this.id,id_ami);
					conf.write("ANSWER");
					try {
						election_leader_mutex.acquire();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(!is_election_leader && leader_port != calculPort(id)){
						if(leader_port != -1){
							this.panne_leader_detected = true;
						}
						is_election_leader = true;
						election_leader_mutex.release();
						Thread tmp_thread = new Thread(new Runnable() {
							@Override
							public void run() {
								startElectionLeader();
							}
						});

						tmp_thread.start();
					}else{
						election_leader_mutex.release();
					}

				}
				conf.terminate(null);
				break;

			case "PREPARE":
				if(nbToken < 2)
					return -1;
				if(panne_acceptor){
					System.out.format("PANNE Acceptor id %d on PROMISE message\n",this.id);
					return 1;
				}
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				return acceptor.sendPromiseResponse(proposal_numb,conf);
				//return 1; /** 1 pour continuer à ecouter */
			case "ACCEPTREQ":
				if(nbToken < 3)
					return -1;
				if(panne_acceptor){
					System.out.format("PANNE Acceptor id %d on ACCEPTREQ message\n",this.id);
					return 1;
				}
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				value = st.nextToken();
				acceptor.handleAcceptRequest(proposal_numb,value,conf);
				return 1;

			case "ACCEPT":
				if(nbToken < 3)
					return -1;
				if(panne_learner){
					System.out.format("PANNE Learner id %d on ACCEPT message\n",this.id);
					break;
				}
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				value = st.nextToken();
				learner.handleAccept(proposal_numb,value);
				break;

		}
		return 0;

	}
	public void setLeader_port(int port){
		this.leader_port = port;
	}

	public Connection startConnectionToPeers(int port) throws IOException {
		Socket client_socket = new Socket();
		client_socket.connect(new InetSocketAddress("127.0.0.1", port));
		PrintWriter out = new PrintWriter(client_socket.getOutputStream());
		InputStreamReader in = new InputStreamReader(client_socket.getInputStream());
		BufferedReader bf = new BufferedReader(in);
		Connection conf = new Connection(client_socket);
		return conf;
	}
	public Map<Integer, Connection> startConnectAllPeers() throws IOException {
		Map<Integer, Connection> peers_connected = new HashMap<Integer, Connection>();
		ExecutorService es = Executors.newCachedThreadPool();
		List<Integer> ami_port_tmp = new ArrayList<Integer>();
		if(panne_leader_detected){
			for(int port_tmp : ami_port)
				if(port_tmp != leader_port)
					ami_port_tmp.add(port_tmp);
		}else{
			ami_port_tmp = ami_port;
		}
		for(int port_friend : ami_port_tmp) {
			es.execute(new Runnable() {
				public void run() {
					try {
						Connection cn = startConnectionToPeers(port_friend);
						peers_connected.put(port_friend,cn);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
		}

		es.shutdown();
		try {
			boolean finished = es.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return peers_connected;
	}

	public void connectAllPeers() throws IOException {
		if(outcall_connected.isEmpty())
			this.outcall_connected = startConnectAllPeers();
		else{
			if(ami_port.size() != outcall_connected.size()){
				List<Integer> ami_port_tmp = new ArrayList<Integer>();
				if(panne_leader_detected){
					for(int port_tmp : ami_port)
						if(port_tmp != leader_port)
							ami_port_tmp.add(port_tmp);
				}else{
					ami_port_tmp = ami_port;
				}
				for(int port_ami : ami_port_tmp)
					if(!outcall_connected.containsKey(port_ami)){
						Connection cn = startConnectionToPeers(port_ami);
						outcall_connected.put(port_ami,cn);
					}
			}
		}
	}
//	public void shutdown_all(){
//		//System.out.println("Closing all system");
//		for(Connection con : outcall_connected.values()){
//			con.write("SHUTDOWN ALL");
//			con.terminate(false);
//		}
//		try {
//			listenner.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

	public void terminate_all(String message){
		//System.out.println("Closing all system");
		for(Connection con : outcall_connected.values()){
			con.terminate(message);
		}
		outcall_connected.clear();
	}
}
