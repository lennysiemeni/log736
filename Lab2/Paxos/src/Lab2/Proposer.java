package Lab2;


import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;

public class Proposer {

	private Ami ami;
	private Thread thread_one;    /** Thread principale pour executer le concensus */

	private Semaphore wait_mutex;
	private Semaphore data_mutex;

	private int proposal_nb;
	public String value_decided;

	private int old_proposal_nb;
	private String old_value_decided;

	private int nb_acceptor_connected;
	private final int nbQuorom;

	private int nb_answer_promise;
	private int nb_answer_accept;

	private int nb_quorom_compteur;
	private int nb_nack_compteur;




	public Proposer(Ami ami){
		this.ami = ami;
		this.proposal_nb = 0;
		this.old_proposal_nb = 0;
		old_value_decided = null;
		this.nbQuorom = ((ami.nb_friend-1)/2) + 1;
	}
	public void reset(){
		this.proposal_nb = 0;
		this.old_proposal_nb = 0;
		old_value_decided = null;
	}
	public void sendtmp(){
		thread_one = new Thread("Thread_one") {
			public void run(){
				int code_error = -2;
				while(code_error == -2){
					code_error = sendPrepareRequest();
					if(code_error == -3){
						if(ami.panne_leader){
							System.out.format("PANNE Leader id %s before sending AcceptReq\n",ami.id);
							break;
						}
						code_error = sendAcceptRequest();
					}
				}

			}
		};
		thread_one.start();
		try {
			thread_one.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public int sendAcceptRequest() {
		System.out.println("sendAcceptRequest start");

		nb_nack_compteur = 0;
		nb_quorom_compteur = 0;
		nb_answer_accept = 0;
		Map<Integer, Connection> outcall_connected = ami.getOutcall_connected();
		nb_acceptor_connected = outcall_connected.size();
		wait_mutex = new Semaphore(nb_acceptor_connected);
		data_mutex = new Semaphore(1);
		for(Connection con : outcall_connected.values()){
			con.write("ACCEPTREQ&"+String.valueOf(proposal_nb)+"&"+this.value_decided);
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					try {
						wait_mutex.acquire();
						//System.out.println(Thread.currentThread().getName()+" pomise_wait acquired");
						//System.out.println(con.socket.getPort());
						String answer = con.read();
						int result = -1;
						if(answer != null)
							result = process(answer);
						if(result == -1){
							data_mutex.acquire();
							nb_answer_accept++;
							int nb_answer = nb_answer_accept;
							data_mutex.release();
							if(nb_answer >= nb_acceptor_connected)
								wait_mutex.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			thread.start();
		}
		try {
			thread_one.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
			try {
				//System.out.println("Principale pomise_wait ask acquire");
				wait_mutex.acquire();
				//System.out.println("Principale pomise_wait acquired");
				data_mutex.acquire();
				int nb_answers = this.nb_answer_accept;
				int nack_answers = this.nb_nack_compteur;
				int quorom_answers = this.nb_quorom_compteur;
				data_mutex.release();
				//System.out.println("Operation terminé!");
				if (nack_answers >= nbQuorom) {
					System.out.println("Leader quorom nack accept");
					return -2; /** -2 pour recommencer */
				}

			} catch (InterruptedException e) {
				System.out.format("The current thread is interrupted\n");
				e.printStackTrace();
			}

		return 0;

	}
	public int sendPrepareRequest(){
		nb_nack_compteur = 0;
		nb_quorom_compteur = 0;
		old_proposal_nb = 0;
		nb_answer_promise = 0;
		proposal_nb++;
		//sendPrepareRequest();
		Map<Integer, Connection> outcall_connected = ami.getOutcall_connected();
		nb_acceptor_connected = outcall_connected.size();
		wait_mutex = new Semaphore(nb_acceptor_connected);
		data_mutex = new Semaphore(1);
		for(Connection con : outcall_connected.values()){
			con.write("PREPARE&"+String.valueOf(proposal_nb));
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					//System.out.println("run by: " + getName());
					try {
						wait_mutex.acquire();
						//System.out.println(Thread.currentThread().getName()+" pomise_wait acquired");
						//System.out.println(con.socket.getPort());
						String answer = con.read();
						int result = -1;
						if(answer != null)
							result = process(answer);
						if(result == -1){
							data_mutex.acquire();
							nb_answer_promise++;
							int nb_answers = nb_answer_promise;
							data_mutex.release();
							if(nb_answers >= nb_acceptor_connected)
								wait_mutex.release();

						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			con.thread_listennerIncome = thread;
			thread.start();
		}
		try {
			thread_one.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(true){
			try {
				//System.out.println("Principale pomise_wait ask acquire");
				wait_mutex.acquire();
				//System.out.println("Principale pomise_wait acquired");
				data_mutex.acquire();
				int nb_answers = this.nb_answer_promise;
				int nack_answers = this.nb_nack_compteur;
				int quorom_answers = this.nb_quorom_compteur;
				data_mutex.release();
				//System.out.println("Operation terminé!");
				if(quorom_answers > nbQuorom){
					System.out.println("Leader quorom promise");
					return -3; /** -3 pour sendAcceptRequest() */
				}
				else
					if(nack_answers > nbQuorom){
						System.out.println("Leader quorom NACK promise");
						return -2; /** -2 pour recommencer */
					}
					else{
						if(old_value_decided != null){
							System.out.println("Leader change sa valeur decider");
							value_decided = old_value_decided;
							proposal_nb = old_proposal_nb;
							return -3; /** -3 pour sendAcceptRequest() */
						}
					}

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;

		}
		return 0;
	}
	public int process(String message) throws InterruptedException {
		//System.out.println("Proposer receive "+message);
		StringTokenizer stt = new StringTokenizer(message,"\r\n");
		//int nbToken = stt.countTokens();
		String command_tmp = stt.nextToken();
		StringTokenizer st = new StringTokenizer(command_tmp,"&");
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str;
		int proposal_numb;
		int nb_answers = 0;
		switch (command.toUpperCase()) {
			case "NACK" -> {
				//System.out.format("Leader id %d  receive NACK\n",ami.id);
				data_mutex.acquire();
				nb_answer_promise++;
				nb_nack_compteur++;
				nb_answers = nb_answer_promise;
				int nack_nb = nb_nack_compteur;
				data_mutex.release();

				if (nack_nb > nb_acceptor_connected / 2 || nb_answers >= nb_acceptor_connected) {
					wait_mutex.release();

				}
			}
			case "ACCEPTED" -> {
				if (nbToken < 2)
					return -1;
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				//System.out.format("Leader id %d  receive ACCEPTED(%d)\n",ami.id,proposal_numb);
					data_mutex.acquire();
					nb_answer_accept++;
					nb_quorom_compteur++;
					nb_answers = nb_answer_accept;
					int quorom_nb = nb_quorom_compteur;
					data_mutex.release();

					if(quorom_nb > nb_acceptor_connected / 2 || nb_answers >= nb_acceptor_connected) {
						wait_mutex.release();
					}
				return 0;
			}
			case "PROMISE" -> {
				if (nbToken < 2)
					return -1;
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				if (proposal_numb != proposal_nb) {
					data_mutex.acquire();
					nb_answer_promise++;
					nb_answers = nb_answer_promise;
					data_mutex.release();

					if (nb_answers >= nb_acceptor_connected) {
						wait_mutex.release();

					}
					return 0;
				}
				if (nbToken == 2) {
					//System.out.format("Leader id %d  receive Promise(%d,-,-)\n",ami.id,proposal_numb);
					data_mutex.acquire();
					nb_answer_promise++;
					nb_quorom_compteur++;
					nb_answers = nb_answer_promise;
					int quorom_nb = nb_quorom_compteur;
					data_mutex.release();

					if(quorom_nb > nb_acceptor_connected / 2 || nb_answers >= nb_acceptor_connected) {
						wait_mutex.release();
					}
					return 0;
				}
				if (nbToken != 4)
					return -1;
				str = st.nextToken();
				int tmp_old_proposal_nb = Integer.parseInt(str);
				str = st.nextToken();
				//System.out.format("Leader id %d  receive Promise(%d, %d, %s)\n",ami.id,proposal_numb,tmp_old_proposal_nb,str);
				data_mutex.acquire();
				nb_answer_promise++;
				nb_answers = nb_answer_promise;
				if (old_proposal_nb < tmp_old_proposal_nb) {
					old_proposal_nb = tmp_old_proposal_nb;
					old_value_decided = str;
				}
				data_mutex.release();

				if (nb_answers == nb_acceptor_connected) {
					wait_mutex.release();

				}
			}
		}
		return 0;

	}
}