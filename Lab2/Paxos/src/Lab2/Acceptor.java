package Lab2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Acceptor {

	private Ami ami;

	private List<Integer> learner_port;
	private List<Connection> learner_con;

	private int proposal_nb_promise;
	private int proposal_nb_accepted;

	private String value_decided;


	public Acceptor(Ami ami){
		this.ami = ami;
		proposal_nb_promise = 0;
		proposal_nb_accepted = 0;
		value_decided = null;
		learner_con = new ArrayList<Connection>();
		learner_port = new ArrayList<Integer>();
	}

	public void addLearner(int port) {
		learner_port.add(port);
	}

	public void setLearner_port(List<Integer> learner_port){
		this.learner_port = learner_port;
	}

	public int sendPromiseResponse(int proposal_nb,Connection con) {
		int return_value = 2;
		//System.out.format("Acceptor id %d  receive PROMISE(%d)\n",ami.id,proposal_nb,proposal_nb);
		if(this.proposal_nb_promise > proposal_nb){
			con.write("NACK&");
			return_value = 1;
		}
		else
			if(value_decided != null){
				this.proposal_nb_promise = proposal_nb;
				con.write("PROMISE&"+String.valueOf(proposal_nb) + "&"+String.valueOf(this.proposal_nb_accepted) + "&" +this.value_decided);
			}
			else{
				con.write("PROMISE&"+String.valueOf(proposal_nb));
			}
			return return_value;
	}
	/**
	 * 
	 * @param proposal_nb
	 * @param value
	 */
	public void sendAcceptedRequest(int proposal_nb, String value) {
		if(ami.isLearner){
			ami.learner.valueAccepted = value;
			ami.learner.handleAccept(proposal_nb,value);
		}
		if(learner_con.isEmpty()){
			if(ami.outcall_connected.isEmpty()){
				for(int port : learner_port){
					try {
						Connection conn = ami.startConnectionToPeers(port);
						ami.outcall_connected.put(port,conn);
						this.learner_con.add(conn);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			else{
				for(int port : learner_port){
					this.learner_con.add(ami.outcall_connected.get(port));
				}
			}
		}
		for(Connection con : this.learner_con){
			con.write("ACCEPT&"+proposal_nb+"&"+value);
		}
	}
	/**
	 *
	 * @param proposal_nb
	 * @param con
	 */
	public void sendAcceptedResponse(int proposal_nb, Connection con) {
		con.write("ACCEPTED&"+proposal_nb);
	}
	/**
	 *
	 * @param proposal_nb
	 * @param value
	 */
	public void handleAcceptRequest(int proposal_nb, String value,Connection con) {
		//System.out.format("Acceptor id %d  receive accept(%d, %s)\n",ami.id,proposal_nb,value);
		if(this.proposal_nb_promise > proposal_nb){
			con.write("NACK&");
		}
		else{
			sendAcceptedResponse(proposal_nb,con);
			if(value_decided == null || !value_decided.equals(value)){
				value_decided = value;
				proposal_nb_accepted = proposal_nb;
				sendAcceptedRequest(proposal_nb,value);
			}
		}
	}


}