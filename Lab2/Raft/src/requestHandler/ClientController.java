package requestHandler;
/**
 * Cette classe permet d'instancier le client
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import java.net.ServerSocket;
import java.util.List;
import java.util.Map;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;

public class ClientController {

	private Map<Integer, Connection> connected;
	private List<Integer> proposer_port;
	private int leader_port;
	private Connection leader_connection;
	private List<String> message_to_send;
	public String value_received;
	private boolean is_value_decide_received;
	private List<Integer> learner_port;
	public Thread thread_listennerIncome;
	private ServerSocket listenner;
	private Semaphore data_mutex;
	public Semaphore main_mutex;
	private int port;
	protected int counter = 0;
	private int message_length;

	public ClientController(int port, List<String> message){
		this.port = port;
		this.leader_port = 0;
		this.message_length = message.size();
		leader_connection = null;
		value_received = null;
		message_to_send = message;
		is_value_decide_received = false;
		this.data_mutex = new Semaphore(1);
		main_mutex = new Semaphore(1);
		try {
			main_mutex.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread_listennerIncome = new Thread(new Runnable() {
			@Override
			public void run() {
				stay_connected();
			}
		});
		thread_listennerIncome.start();
	}

	/**
	 * Maintient la connection a l'ecoute
	 */
	public void stay_connected() {
		Socket socket = null;
		System.out.println("Client " + Integer.toString(port) + " is listening.");
		try {
			listenner = new ServerSocket();
			listenner.bind(new InetSocketAddress("127.0.0.1", port));
			try {
				while (true) {
					try {
						socket = listenner.accept();
						System.out.println("Client " + Integer.toString(port) + " new connection.");
					} catch (Exception e) {
						break;
					}
					Connection conf = new Connection(socket);
					int port_connected = socket.getPort();
					String port_str = Integer.toString(port_connected);
					Thread thread = new Thread(port_str) {
						public void run() {
							int answer = -2;
							while (answer == -2) {
								answer = listenAmi(conf);
								if(counter <= message_length-1){
									System.out.println("Notice : Client -> leader is ready, sending : "+message_to_send.get(counter));
									writeOperation(message_to_send.get(counter),leader_connection);
									counter++;
								} else {

								}
							}
							if(answer == 1){
								leader_connection.terminate(null);
								leader_connection = null;
							}
							conf.terminate(null);
						}
					};
					thread.start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Server error");
		}
	}

	/**
	 * Ecoute et process la requete d'un noeud du reseau
	 * @param conf
	 * @return
	 */
	public int listenAmi(Connection conf){
		String message = conf.read();
		int err_code =process(message,conf);
		if(err_code == -1){
			return err_code;
			//TODO Handle error
		}
		return err_code;
	}

	public void connectLeader() {
		try {
			this.leader_connection = startConnectionToPeers(leader_port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Demarrer la connexion a UN noeuds du reseau + client
	 * @param port port d'un noeud ou client
	 * @return retourne la config d'un noeud du reseau
	 * @throws IOException erreur reseau
	 */
	public Connection startConnectionToPeers(int port) throws IOException {
		Socket client_socket = new Socket();
		client_socket.connect(new InetSocketAddress("127.0.0.1", port));
		PrintWriter out = new PrintWriter(client_socket.getOutputStream());
		InputStreamReader in = new InputStreamReader(client_socket.getInputStream());
		BufferedReader bf = new BufferedReader(in);
		Connection conf = new Connection(client_socket);
		return conf;
	}

	public void printReceivedValue() {
		System.out.format("Client confirme consensus with value: %s\n",value_received);
	}

	public void writeOperation(String message, Connection con) {
		String messageToSend = "CLIENT&"+message;
		con.write(messageToSend);
	}

	public void shutdownSystem(){

		try {
			leader_connection.terminate("LEADER SHUTDOWN ALL");
			listenner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int setLeader(int portt){
		try {
			data_mutex.acquire();
			if(leader_port == portt){
				data_mutex.release();
				return -1;
			}

			System.out.format("Client received new leader port %d \n",portt);
			if(this.leader_connection != null){
				leader_connection.terminate("SHUTDOWN");
				leader_connection = null;
			}
			leader_port = portt;
			data_mutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return 1;
	}

	/**
	 * parseur des commandes recus dans le thread de chaque noeud
	 * @param message message recu
	 * @param conf node configuration
	 * @return error_code code d'erreur d'execution
	 */
	public int process(String message,Connection conf) {
		String message_backup = message;
		StringTokenizer st = new StringTokenizer(message,"&\r\n");
		int errorCode = 0;
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str;
		int value;
		int proposal_numb;
		switch(command.toUpperCase()) {
			case "OK":
				System.out.println("Notice : Received OK TO RECEIVE from leader");
				return -2;/** -2 pour continuer à ecouter */
			case "CLIENTRES":
				if(nbToken < 2){
					return -1;
				}
				str = st.nextToken();
				try {
					data_mutex.acquire();
					if(!this.is_value_decide_received){
						this.value_received = str;
						is_value_decide_received = true;
						//printReceivedValue();
						main_mutex.release();
					}
					data_mutex.release();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				return -2; /** -2 pour continuer à ecouter */
			case "LEADERPORT":
				if(nbToken < 2){
					return -1;
				}
				str = st.nextToken();
				int port_leader = Integer.parseInt(str);
				int code_return = setLeader(port_leader);
				if(code_return > 0){
					connectLeader();
				}
				return -2; /** -2 pour continuer à ecouter */
			case "COMMIT":
				if(nbToken < 2){
					return -1;
				}
				str = st.nextToken();
				System.out.format("Notice -> Client : COMMITTED '"+str+"'");
				return -2; /** -2 pour continuer à ecouter */
			default:
				// code block
		}
		return 0;

	}

	private void incrementMessageCounter() {
		this.counter = ++this.counter;
	}

}