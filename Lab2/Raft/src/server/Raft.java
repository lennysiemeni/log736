package server;
/**
 * Class main de l'algorithme
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 * Malheureusement, nous n'avons pas eu le temps de tout terminer...
 *  - Pas de replication de log
 *  - Pas de support du heartbeat message
 *  - Pas detection qu'un leader tomber en panne et de declancher une nouvelle election de leader
 *  - Pas de reponse a un message de vote (a la suite d'une panne)
 */
import requestHandler.ClientController;
import java.io.IOException;
import java.util.Arrays;

import node.Ami;

public class Raft {
    public static String[]message = {"SET&2","ADD&1","MULT&3","EXECUTE&0"};
    public static void test1() throws IOException, InterruptedException {
        ClientController client = new ClientController(25100, Arrays.asList(message));
        int[] follower_id = {1,2,3,4,5,6};
        int nb_friend = 6;
        Ami[] ami_array = new Ami[nb_friend];
        for(int id = 1;id<=nb_friend;id++){
            ami_array[id-1] = new Ami(id,nb_friend,follower_id);
        }

        //Phase 1 : Élection de leader
        Thread thread_electionLeader = new Thread() {
            public void run(){
                ami_array[1].electionLeader();
            }
        };

        //Phase 2 d'execution de l'algorithme
        thread_electionLeader.start();
        client.main_mutex.acquire();

        //S'assurer que le consensus fut respecté
        if(client.value_received.equals("9")){
            System.out.println("Concensus respected");
        }else{
            System.out.println("Concensus Not respected");
        }
        client.shutdownSystem();
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        test1();
    }
}