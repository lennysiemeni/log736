package node;
/**
 * Cette classe permet d'instancier un follower
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import requestHandler.Connection;
import shared.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Follower extends Ami{

    private Ami ami;

    //private int id;
    private int terme;
    private int value_decided;
    private List<Integer> proposer_port;
    private int nb_proposer_connected;
    private int leaderPort;
    private int proposal_nb;
    private Semaphore data_mutex;
    public Semaphore increment_mutex = new Semaphore(1);

    private List<Integer> followerList = new ArrayList<Integer>();
    private List<Integer> follower_port;

    public void addFollower(int port) {
        followerList.add(port);
    }

    public void setLeaderPort(int port) {
        leaderPort = port;
    }

    public Follower(Ami ami) {
        this.ami = ami;
        this.proposal_nb = 0;
        this.data_mutex = new Semaphore(1);
        this.follower_port = new ArrayList<Integer>();
    }

    public void setFollower_port(List<Integer> follower_port){
        this.follower_port = follower_port;
    }

    /**
     * Follower to leader : send -> accepted(index,cmd)
     */
    public void acknowledge(Connection con) {
        con.write(Constants.ACCEPTED_RESPONSE+"&"+ami.getIndex()+"&"+ami.getLogs().get(getLogs().size()));
    }

    /**
     * Nack(n)
     * @param terme
     */
    public void sendNackResponse(int terme, Connection con) {
            con.write((Constants.NACK_COMMAND+"&"+terme)); //node id
    }

    /**
     *    process upon receiving command from leader: AcceptReq(terme,indexe,command,prev_command)
     */
    public int handleAcceptReq(int currentTerm, int currentlogIndex, String command,String prevCommand, Connection con) throws InterruptedException {
        try {
            data_mutex.acquire();
            //Les appendEntries suivants confirment les commits
            if(ami.getIndex() > 0){
                System.out.println("Follower "+ami.getId()+" is executing previously commited command : "+ami.getLogs().get(ami.getLogs().size()-1));
                ami.executeCommand();
            }
            if (currentTerm == ami.getTerme()){
                ami.getLogs().add(command);
                System.out.println("Notice : Follower "+ami.getId()+" -> Added command : "+ami.getLogs().get(ami.getLogs().size()-1));
            } else {
                ami.getLogs().add(command);
                System.out.println("Follower "+ami.getId()+" : Erreur acceptRequest, indice ou terme invalide. Current Term : "+ami.getTerme()+". Current Index : "+ami.getIndex());
                System.out.println("Got -> Term : "+currentTerm+". Index : "+currentlogIndex);
                sendNackResponse(ami.getTerme(), con);
                data_mutex.release();
                return -1;
            }
            data_mutex.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        increment_mutex.acquire();
        ami.incrementIndex();
        acknowledge(con);
        increment_mutex.release();
        return -2;
    }

    public int getQuorum() throws InterruptedException {
        if(ami.getLogs() !=null){
            this.ami.executeCommand();
            return -2;
        } else {
            return -1;
        }
    }
}
