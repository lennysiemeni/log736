package Lab2;

import java.util.List;
import java.util.Map;

public class Proposer {

	private Ami ami;
	private int proposal_nb;
	private List<Integer> acceptor_port;
	private int nb_acceptor_connected;
	public Boolean isLeader = false;
	private TypeRole role = TypeRole.Proposer;

	public Proposer(Ami ami){
		this.ami = ami;
	}

	/**
	 * 
	 * @param proposal_nb
	 */
	public void sendPrepareRequest(int proposal_nb) {
		// TODO - implement Proposer.sendPrepareRequest
		throw new UnsupportedOperationException();
	}

	/**
	 *
	 * @param proposal_nb
	 * @param value
	 */
	public void sendAcceptRequest(int proposal_nb, int value) {
		// TODO - implement Proposer.sendAcceptRequest
		throw new UnsupportedOperationException();
	}
	/**
	 *
	 * @param proposal_nb
	 * @param old_proposal_nb
	 * @param old_value
	 */
	public void handlePromiseResponse(int proposal_nb, int old_proposal_nb, int old_value) {
		// TODO - implement Proposer.sendAcceptRequest
		throw new UnsupportedOperationException();
	}
	/**
	 *
	 * @param proposal_nb
	 * @param value
	 */
	public void handleAcceptedResponse(int proposal_nb, int value) {
		// TODO - implement Proposer.sendAcceptRequest
		throw new UnsupportedOperationException();
	}
	/**
	 *
	 * @param value
	 */
	public void handleClientReq(int value) {
		// TODO - implement Proposer.sendAcceptRequest
		throw new UnsupportedOperationException();
	}
	/**
	 * 
	 * @param message
	 * @param connections
	 */
	public void process(String message, Map<Integer, Connection> connections) {
		// TODO - implement Proposer.process
		throw new UnsupportedOperationException();
	}

}