package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Ami {

	private int port;
	private int id;
	public Thread thread_listennerIncome;
	public Semaphore election_leader_mutex;
	public Semaphore election_learner_mutex;
	public boolean is_election_leader;
	public boolean is_election_learner;
	private Map<Integer, Connection> incall_connected;
	private Map<Integer, Connection> outcall_connected;
	private boolean isLeader;
	private boolean isListining;
	//public Semaphore semaphore;
	private int[] friend_port;
	private boolean isLearner;
	private Acceptor acceptor;
	private Proposer proposer;
	private Learner learner;
	private ServerListen sn;

	public Ami(int id) throws IOException {
		this.id = id;
		this.port = calculPort(id);
		election_leader_mutex = new Semaphore(1);
		is_election_leader = false;
		election_learner_mutex = new Semaphore(1);
		is_election_learner = false;
		this.incall_connected = new HashMap<Integer, Connection>();
		this.outcall_connected = new HashMap<Integer, Connection>();
		this.acceptor = new Acceptor(this);
		this.proposer = null;
		this.learner = null;
		isListining = false;


		sn = new ServerListen(this);
		sn.bindSockets("localhost", port);
		thread_listennerIncome = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					sn.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		thread_listennerIncome.start();
	}

	public void set_friend_port(int ... friend_port){
		this.friend_port = friend_port;
	}
	public void startAmi(){

	}
	public void setLearner(){
		learner = new Learner(this);
	}




	public void electionLeader() {
		System.out.println("Ami avec id: "+id+" start electionLeader");
		final int[] receive_stop = {0};
		final boolean[] receive_stopp = {false};
		ExecutorService es = Executors.newCachedThreadPool();
		for(Connection con : outcall_connected.values()) {
			//int ami_port = calculPort(id_friend);
			es.execute(new Runnable() {
				public void run() {
					con.write("ELECTION LEADER "+ id);
					String str = con.read();
					receive_stop[0] = 1;
					//closeCurrentThread();

				}
			});
		}

		es.shutdown();
		if(receive_stop[0]==1){
			return;
		}
		try {
			boolean finished = es.awaitTermination(10, TimeUnit.SECONDS);
			if(receive_stop[0]==0){
				System.out.println("Ami avec id: "+ id+" est elu Leader");
				for(Connection con : outcall_connected.values()){
					con.write("COORDINATION LEADER");
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String process(String message, SelectionKey selectionKey) {
		System.out.println("Ami avec id: "+id+" receive message :" +message);
		String message_backup = message;
		StringTokenizer st = new StringTokenizer(message," \r\n");
		int errorCode = 0;
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str;
		int value;
		int proposal_numb;
		switch(command.toUpperCase()) {
			case "SALUT":
				break;
			case "COORDINATION":
				if(nbToken < 2)
					return "-1";
				str = st.nextToken();
				SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
				int ports = socketChannel.socket().getLocalPort();
				if (str.equalsIgnoreCase("LEARNER"))
					acceptor.addLearner(ports);
				else if(str.equalsIgnoreCase("LEADER"))
					acceptor.setLeader(ports);
				else
					return "ERROR";
				break;
			case "ELECTION":
				if(nbToken < 3)
					return "ERROR";
				str = st.nextToken();
				String str1 = st.nextToken();
				int id_ami = Integer.parseInt(str1);
//				if (str.equalsIgnoreCase("LEARNER") && id < id_ami){
//					SocketChannel socketChannels = (SocketChannel) selectionKey.channel();
//					int portss = socketChannels.socket().getLocalPort();
//					try {
//						election_learner_mutex.acquire();
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					if(!is_election_learner){
//						is_election_learner = true;
//						Thread tmp_thread = new Thread(new Runnable() {
//							@Override
//							public void run() {
//								electionLearner();
//							}
//						});
//
//						tmp_thread.start();
//					}
//					election_learner_mutex.release();
//					return "answer LEARNER \r\n";
//				}
//				else
				if(str.equalsIgnoreCase("LEADER")&& id > id_ami){
					//System.out.println("Ami avec id: "+id+" id > id_ami");
					try {
						election_leader_mutex.acquire();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(!is_election_leader){
						is_election_leader = true;
						Thread tmp_thread = new Thread(new Runnable() {
							@Override
							public void run() {
								electionLeader();
							}
						});

						tmp_thread.start();
					}
					election_leader_mutex.release();
					return "answer LEADER \r\n";
				}
				break;

			case "PREPARE":
				if(nbToken < 2)
					return "ERROR";
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				SocketChannel socketChannels = (SocketChannel) selectionKey.channel();
				int portss = socketChannels.socket().getLocalPort();
				acceptor.sendPromiseResponse(proposal_numb,portss);
				break;
			case "PROMISE":
				if(nbToken < 2)
					return "ERROR";
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				int old_proposal_numb = -1;
				int older_value = -1;
				if(nbToken > 2){
					str = st.nextToken();
					old_proposal_numb = Integer.parseInt(str);
					str = st.nextToken();
					older_value = Integer.parseInt(str);
				}
				proposer.handlePromiseResponse(proposal_numb,old_proposal_numb,older_value);
				break;
			case "ACCEPTED":
				if(nbToken < 3)
					return "ERROR";
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				str = st.nextToken();
				value = Integer.parseInt(str);
				acceptor.handleAcceptResponse(proposal_numb,value);
				break;
			case "ACCEPT":
				if(nbToken < 3)
					return "ERROR";
				str = st.nextToken();
				proposal_numb = Integer.parseInt(str);
				str = st.nextToken();
				value = Integer.parseInt(str);
				proposer.handleAcceptedResponse(proposal_numb,value);
				break;
			case "CLIENT":
				if(nbToken < 2)
					return "ERROR";
				if(this.proposer == null){
					this.proposer = new Proposer(this);
				}
				str = st.nextToken();
				value = Integer.parseInt(str);
				proposer.handleClientReq(value);
				break;
			default:
				// code block
		}
		return "OK";

	}
	/** Close the current thread*/
	public void closeCurrentThread(){
		Thread.currentThread().interrupt();//preserve the message
	}
	public void client_tcp(Connection connection){

	}
	public void set_self_leader(boolean isLeader){
		this.isLeader = isLeader;

	}
	public void set_self_learner(boolean isLearner){
		this.isLearner = isLearner;

	}
	public int calculPort(int id){
		return 25100 + id * 100;
	}


	public Connection startConnectionToPeers(int port) throws IOException {
		Socket client_socket = new Socket();
		client_socket.connect(new InetSocketAddress("localhost", port));
		//Socket client_socket = new Socket("localhost", port);
		//System.out.println("Ami "+this.port + " is connecting to "+port);
		PrintWriter out = new PrintWriter(client_socket.getOutputStream());
		InputStreamReader in = new InputStreamReader(client_socket.getInputStream());
		BufferedReader bf = new BufferedReader(in);
		Connection conf = new Connection(client_socket,in,out,bf);
		return conf;
	}
	public Map<Integer, Connection> startConnectAllPeers() throws IOException {
		Map<Integer, Connection> peers_connected = new HashMap<Integer, Connection>();
		ExecutorService es = Executors.newCachedThreadPool();
		for(int port_friend : friend_port) {
			//int ami_port = calculPort(id_friend);
			es.execute(new Runnable() {
				public void run() {
					try {
						Connection cn = startConnectionToPeers(port_friend);
						peers_connected.put(port_friend,cn);
						//outcall_connected.put(port_friend,cn);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
		}

		es.shutdown();
		try {
			boolean finished = es.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return peers_connected;
	}
	public void connectAllPeers() throws IOException {
		this.outcall_connected = startConnectAllPeers();

	}

	public void terminate(Connection connection) {
		connection.terminate();
	}

}