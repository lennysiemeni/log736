package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Connection {

	public Socket socket;
	public PrintWriter out;
	public InputStreamReader in;
	public BufferedReader bf;
	public int id;

	public Connection(Socket socket,InputStreamReader in,PrintWriter out,BufferedReader bf){
		this.socket = socket;
		this.in = in;
		this.out = out;
		this.bf = bf;
	}
	public Connection(Socket socket,InputStreamReader in,PrintWriter out,BufferedReader bf,int id){
		this.socket = socket;
		this.in = in;
		this.out = out;
		this.bf = bf;
		this.id = id;

	}
	public String read()  {
		String str_tmp = "tmp";
		try {
			str_tmp= bf.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str_tmp;
	}

	/**
	 * 
	 * @param message
	 */
	public void write(String message) {
		//System.out.println("Ami "+socket.getLocalPort()+" writing");
		out.write(message+ "\r\n");
		out.flush();
	}
	public void terminate(){ ;
		String dataSend = "SHUTDOWN";
		write(dataSend);
		out.println(dataSend);
		out.flush();
		try {
			bf.close();
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}