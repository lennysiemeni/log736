package Lab2;

import java.util.List;
import java.util.Map;

public class Learner {

	private List<Integer> acceptor_port;
	private Ami ami;
	private int nb_acceptor_connected;
	private int client_port;
	private TypeRole role = TypeRole.Learner;


	public Learner(Ami ami){
		this.ami = ami;
	}
	/**
	 * 
	 * @param value_decided
	 */
	public void sendClientResponse(int value_decided) {
		// TODO - implement Learner.sendClientResponse
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param message
	 * @param connections
	 */
	public void process(String message, Map<Integer, Connection> connections) {
		// TODO - implement Learner.process
		throw new UnsupportedOperationException();
	}

}