package Lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class Acceptor {

	private Ami ami;
	private int proposal_nb;
	private int value_decided;
	private List<Integer> proposer_port;
	private int nb_proposer_connected;
	private int leader;
	private List<Integer> learner_port = new ArrayList<Integer>();
	private TypeRole role = TypeRole.Acceptor;

	public Acceptor(Ami ami){
		this.ami = ami;
	}

	public void addLearner(int port) {
		learner_port.add(port);
	}
	public void setLeader(int port) {
		leader = port;
	}

	/**
	 * 
	 * @param proposal_nb
	 * @param old_proposal_nb
	 * @param old_value
	 */
	public void sendPromiseResponse(int proposal_nb, int old_proposal_nb, int old_value) {
		// TODO - implement Acceptor.sendPromiseResponse
		throw new UnsupportedOperationException();
	}
	/**
	 *
	 * @param proposal_nb
	 * @param port_leader
	 */
	public void sendPromiseResponse(int proposal_nb,int port_leader) {
		// TODO - implement Acceptor.sendPromiseResponse
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param proposal_nb
	 * @param value
	 */
	public void sendAcceptedRequest(int proposal_nb, int value) {
		// TODO - implement Acceptor.sendAcceptedRequest
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param proposal_nb
	 * @param value
	 */
	public void handleAcceptResponse(int proposal_nb, int value) {
		// TODO - implement Acceptor.sendAcceptedResponse
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param message
	 * @param token
	 */
	public void process(String message, StringTokenizer token) {
		// TODO - implement Acceptor.process
		throw new UnsupportedOperationException();
	}

}