package ami;

public enum TypeRole {
	Proposer,
	Leader,
	Acceptor,
	Learner
}