package Lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;

public class ClientController {

	private int port;
	public ServerSocket listenner;
	public Thread thread_listennerIncome;

	private int leader_port;
	private Connection leader_connection;

	private String value_to_deal;
	public String value_received;
	private boolean is_value_decide_received;

	private Semaphore data_mutex;
	public Semaphore main_mutex;

	public ClientController(int port,String message){
		this.port = port;
		this.leader_port = 0;
		leader_connection = null;
		value_received = null;
		value_to_deal = message;
		is_value_decide_received = false;
		this.data_mutex = new Semaphore(1);
		main_mutex = new Semaphore(1);
		try {
			main_mutex.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread_listennerIncome = new Thread(new Runnable() {
			@Override
			public void run() {
				stay_connected();
			}
		});
		thread_listennerIncome.start();
	}
	public void stay_connected(){
		Socket socket = null;
		//System.out.println("Client "+Integer.toString(port)+ " is listening.");
		try {
			listenner = new ServerSocket();
			listenner.bind(new InetSocketAddress("127.0.0.1", port));
			try {
				while (true) {
					try {
						socket = listenner.accept();
						//System.out.println("Client "+Integer.toString(port)+" new connection.");
					} catch (Exception e) {
						break;
					}
					Connection conf = new Connection(socket);

					int port_connected = socket.getPort();
					String port_str = Integer.toString(port_connected);
					Thread thread = new Thread(port_str) {
						public void run(){
							String message = conf.read();
							if(message != null)
								process(message,conf);
							conf.terminate(null);
						}
					};
					thread.start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Server error");
		}

	}

	public void connectLeader() {
		try {
			this.leader_connection = startConnectionToPeers(leader_port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Connection startConnectionToPeers(int port) throws IOException {
		Socket client_socket = new Socket();
		client_socket.connect(new InetSocketAddress("127.0.0.1", port));
		PrintWriter out = new PrintWriter(client_socket.getOutputStream());
		InputStreamReader in = new InputStreamReader(client_socket.getInputStream());
		BufferedReader bf = new BufferedReader(in);
		Connection conf = new Connection(client_socket);
		return conf;
	}
	public void printReceivedValue() {
		System.out.format("Client confirme consensus with value: %s\n",value_received);
	}
	/**
	 * Format message envoyer: Date/Heure/Emplacement
	 * Date: JJ,MM,AAAA
	 * Heure: HH,MM
	 * Emplacement: adresse
	 * */
	public void write() {
		String message = "CLIENT&"+this.value_to_deal;
		connectLeader();
		leader_connection.write(message);
		String answer = leader_connection.read();
		int err_mess = process(answer,leader_connection);
		if(err_mess == 1){
			leader_connection.terminate(null);
			leader_connection = null;
		}
	}
	public void terminate_system() {
		String message = "CLIENT&SHUTDOWN ALL";
		connectLeader();
		leader_connection.write(message);
		leader_connection.terminate(null);
		leader_connection = null;
		try {
			listenner.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public int setLeader(int portt){
		try {
			data_mutex.acquire();
			if(leader_port == portt){
				data_mutex.release();
				return -1;
			}

			System.out.format("Client received new leader port %d \n",portt);
			if(this.leader_connection != null){
				leader_connection.terminate("SHUTDOWN");
				leader_connection = null;
			}
			leader_port = portt;
			data_mutex.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return 1;
	}
	public void setLeader_port(int port){
		this.leader_port = port;
	}
	public int process(String message,Connection conf) {
		//System.out.println("Ami avec id: "+id+" receive message :" +message);
		StringTokenizer st = new StringTokenizer(message,"&\r\n");
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str;
		switch(command.toUpperCase()) {
			case "OK":
				return 1;
			case "CLIENTRES":
				if(nbToken < 2){
					return -1;
				}
				str = st.nextToken();
				try {
					data_mutex.acquire();
					if(!this.is_value_decide_received){
						this.value_received = str;
						is_value_decide_received = true;
						printReceivedValue();
						main_mutex.release();
					}
					data_mutex.release();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			case "LEADERPORT":
				if(nbToken < 2){
					return -1;
				}
				str = st.nextToken();
				int port_leader = Integer.parseInt(str);
				int code_return = setLeader(port_leader);
				if(code_return > 0){
					write();
				}
				break;
			default:
				// code block
		}
		return 0;

	}

}