package shared;
/**
 * Cette classe permet d'instancier le serveur d'ecoute TELNET
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Constants {
	//SERVER Commands
	public static final String GET_TIME_COMMAND = "GET Time";
	public static final String DISCONNECT_COMMAND = "DISCONNECT";
	public static final String SHUTDOWN_COMMAND = "SHUTDOWN";
	public static final String UNKNOWN_COMMAND = "UNKNOWN COMMAND";

	//PAXOS Commands
	public static final String PREPARE_COMMAND = "PREPARE";
	public static final String PROMISE_COMMAND = "PROMISE";
	public static final String ACCEPTREQ_COMMAND = "ACCEPTREQ";
	public static final String ACCEPT_COMMAND = "ACCEPT";
	public static final String PING_COMMAND = "PING";
	public static final String NACK_COMMAND = "NACK";
	public static final String CLIENT_RESPONSE = "CLIENTRES";
	public static final String ACCEPTED_RESPONSE = "ACCEPTED";
	public static final String EXECUTE_COMMAND = "EXECUTE";


	//Possible Roles for "Ami"
	public static final String PROPOSER_ROLE = "PROPOSER";
	public static final String ACCEPTOR_ROLE = "ACCEPTOR";
	public static final String LEARNER_ROLE = "LEARNER";
	public static final String LEADER_ROLE = "LEADER";

	//TEST Commands
	public static final String DECODE_COMMAND = "DECODE";

	// encoding
	// of telenet
	public static final Charset TELNET_ENCODING = StandardCharsets.ISO_8859_1;
}
