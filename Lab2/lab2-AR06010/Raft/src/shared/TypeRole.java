package shared;
/**
 * Cette classe permet d'instancier le serveur d'ecoute TELNET
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
public enum TypeRole {
	Proposer,
	Leader,
	Acceptor,
	Learner
}