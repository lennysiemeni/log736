package node;
/**
 * Cette classe permet d'instancier ami de l'algorithme de raft
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 * Ce programme contient plusieurs codes d'erreurs qui sont les suivants :
 * (-2) : etat normal, poursuivre l'ecoute
 * (-5) : ordonner au noeud specifie d'executer la derniere commande contenue dans son propre log
 * (-3) : reponse NACK et erreur
 * (-1) : erreur critique
 */
import requestHandler.Connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


public class Ami {

	// Common ami attributes
	private int port;
	protected int id;
	private int index = 0;
	private int terme = 1;
	public int nb_friend;
	private int leader_port;
	private int[] friend_port;

	//log
	private List<String> logCommand = new ArrayList<String>();
	private int logIndex = 0;
	private int lastLogIndexPosition = 0;
	private int computed_consensus = 0;

	//mutexes
	public Thread thread_listennerIncome;
	public Semaphore mutex;
	public Semaphore election_follower_mutex;
	public Semaphore election_leader_mutex;
	private Semaphore wait_mutex;

	//flags
	public boolean is_election_leader;
	public boolean is_election_follower;
	private boolean isLeader;
	private boolean isFollower;
	private boolean isListining;
	private boolean is_candidat = false;
	private int last_voted_election = -1; //Le nombre de l'election la plus recentedans laquelle on a voter
	private int last_vote = -1; //Le nombre de candidats de notre vote le plus recent

	//timing
	private int base = 300;
	private final long timeout = (long) (1+Math.random()*300);
	private final long endTime = System.currentTimeMillis() + timeout;

	//server
	public Map<Integer, Connection> outcall_connected;
	public List<Integer> ami_port;
	private ServerSocket listenner;
	public int client_port;


	//Common classes
	private Follower follower;
	private Leader leader;



	public Ami (){}
	public Ami(int id, int nb_friend, int[] follower_id) throws IOException {
		this.id = id;
		this.client_port = calculPort(0);
		this.nb_friend = nb_friend;
		this.port = calculPort(id);
		this.logCommand = getLogs();
		mutex = new Semaphore(1);
		is_election_leader = false;
		election_follower_mutex = new Semaphore(1);
		is_election_follower = false;
		this.outcall_connected = new HashMap<Integer, Connection>();
		this.follower = null;
		this.leader = null;
		setFollower_id(follower_id);
		thread_listennerIncome = new Thread(new Runnable() {
			@Override
			public void run() {
				stay_connected();
			}
		});
		thread_listennerIncome.start();
		ami_port = new ArrayList<Integer>();
		for(int i=1;i<=nb_friend;i++){
			if(i !=id){
				int port_ami = calculPort(i);
				ami_port.add(port_ami);
			}
		}

		if(isLeader){
			this.leader = new Leader(this);
			connectAllPeers();
			annonceLeaderAll();
		}
		/** Peut etre effacer plus tard*/
		isListining = false;
		election_leader_mutex = new Semaphore(1);
		is_election_leader = false;
		election_follower_mutex = new Semaphore(1);
		is_election_follower = false;
	}

	/**
	 * Methode est appeler pour dupliquer le status des logs aux autres noeuds.
	 * Ca retourne True quand ca synchronise les log, et false quand les logs on besoin d'une synchronisation
	 */
	public boolean appendEntries(List<String> logCommandToUpdate)
	{
		//Same size
		if(logCommand.size() == logCommandToUpdate.size())
		{
			if(logCommand.equals(logCommandToUpdate))
			{
				//Les logs sont deja updated
				return true;
			}
			else
			{
				//Les logs sont de la meme taille mais il on pas les meme valeurs alors il y'a une desynchronisation
				return false;
			}
		}

		//On doit rajouter des logs
		else if (logCommand.size() < logCommandToUpdate.size())
		{
			for(int i = logCommand.size(); i < logCommandToUpdate.size(); i++)
			{
				logCommand.add(i, logCommandToUpdate.get(i));
			}
			return true;
		}

		//Les logs de ce noeuds sont plus avancer que les logs de leader, on retourne false et le leader
		//doit appeler rollbackAndSynchronizeCommits parcque on est desynchroniser
		return false;
	}

	/**
	 * If a node cannot accept the contents of an appendEntries message
	 * ce neud doit reparer ces logs en ce comparent a son leader.
	 * Il doit rollback pour etre comme sont leader et puis ensuite r'ajouter tous les logs qui manque
	 */
	public boolean rollbackAndSynchronizeCommits(List<String> logCommandToUpdate)
	{
		logCommand = logCommandToUpdate;
		return true;
	}

	/**
	 * Cette methode est appeller lors de la phase d'election, ca force le noeud qui na pas voter a voter
	 * @return
	 */
	public int respondVote(int terme, int candidat)
	{
		if(last_voted_election < terme)
		{
			return candidat;
		}
		else if (last_voted_election > terme)
		{
			return -1;
		}
		else
		{
			if(candidat > last_vote)
			{
				return candidat;
			}
			else
			{
				return last_vote;
			}
		}
	}

	//Pour calculer le port, pour simplifier le processus d'assignation des connections
	public int calculPort(int id){
		return 25100 + id * 100;
	}




	/**
	 * Assurer que la connection reste ouverte avec les autres noeuds
	 */
	public void stay_connected(){
		Socket socket = null;
		System.out.println("Client "+Integer.toString(port)+ " is listening.");
		try {
			listenner = new ServerSocket();
			//bind the socketserver only to localhost
			listenner.bind(new InetSocketAddress("127.0.0.1", port));
			try {
				while (true) {
					try {
						socket = listenner.accept();
					} catch (Exception e) {
						break;
					}
					Connection conf = new Connection(socket);
					int port_connected = socket.getPort();
					String port_str = Integer.toString(port_connected);
					Thread thread = new Thread("Client "+port_str) {
						public void run(){
							int answer = -2;
							while(answer == -2 ) {
								try {
									//busy waiting pour ecouter le socket
									answer = listenAmi(conf);
								} catch (IOException | InterruptedException e) {
									e.printStackTrace();
								}
								//Le leader va retourner -5 pour signaler aux followers d'executer leur log
								if(answer == -5 && follower != null && leader == null){
									try {
										answer = executeCommand();
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
								}
							}
							conf.terminate(null);
						}
					};
					thread.start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Server error");
		}

	}

	/**
	 * Methode pour executer la commande de la petite calculatrice
	 * @return	error_code
	 * @throws InterruptedException
	 */
	public int executeCommand() throws InterruptedException {
		mutex.acquire();
		if(this.logCommand == null || this.logCommand.size() < 1){
			mutex.release();
			return -1;
		}
		String last_log_element = this.logCommand.get(logCommand.size()-1);
		StringTokenizer st = new StringTokenizer(last_log_element,"&");
		int nbToken = st.countTokens();
		int x;
		if(nbToken != 2){
			System.out.println("Error executing command : nbToken invalid");
			mutex.release();
			return -1;
		}
		String command = st.nextToken();
		switch (command){
			case "ADD":
				x = Integer.parseInt(st.nextToken());
				this.computed_consensus = this.computed_consensus + x;
				break;
			case "MULT":
				x = Integer.parseInt(st.nextToken());
				this.computed_consensus = this.computed_consensus * x;
				break;
			case "SUB":
				x = Integer.parseInt(st.nextToken());
				this.computed_consensus = this.computed_consensus - x;
				break;
			case "SET":
				x = Integer.parseInt(st.nextToken());
				this.computed_consensus = x;
				break;
			case "EXECUTE":
				break;
			default:
				mutex.release();
				System.out.println("Error : Operation '"+command+"' not supported");
				return -1; //Operation not supported
		}
		mutex.release();
		System.out.println("Node "+this.id+" -> Result : "+this.computed_consensus);
		return -2; //Normal execution, resume listening on incoming socket
	}

	/**
	 * methode d'ecoute d'un noeud (analogie avec un ami vue dans le cours)
	 * @param conf network config of a node
	 * @return	code d'erreur de l'execution
	 * @throws IOException erreur reseau
	 * @throws InterruptedException interruption des threads
	 */
	public int listenAmi(Connection conf) throws IOException, InterruptedException {
		String message = conf.read();
		int errCode = process(message,conf);
		if(errCode == -1){
			//TODO HandleError
			return errCode;
		}
		return errCode;
	}

	/**
	 * Commance l'election de leader pour l'initialisation de raft (phase 1)
	 */
	public void startElectionLeader(){
		Thread thread_principal = new Thread("Thread_one") {
			public void run(){
				electionLeader();

			}
		};
		thread_principal.start();
		try {
			thread_principal.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Methode qui devait se charger d'envoyer un heartbeat a defaut d'envoyer un message regulier par le leader
	 * @param heartBeatMessage valeur du heartbeat
	 */
	public void sendHeartBeat(String heartBeatMessage){
		final int[] receive_stop = {0};
			ExecutorService es = Executors.newFixedThreadPool(outcall_connected.size());
			for(Connection con : outcall_connected.values()) {
				es.execute(new Runnable() {
					public void run() {
						con.write(heartBeatMessage+" "+ id); //node id
						String str = con.read();
					}
				});
			}
			es.shutdown();
	}


	/**
	 * Algorithme d'election de leader de la phase 1
	 */
// Par l'algorithme de bully
	public void electionLeader() {
		System.out.println("Ami avec id: "+id+" start electionLeader");
		try {
			connectAllPeers();
		} catch (IOException e) {
			e.printStackTrace();
		}
		wait_mutex = new Semaphore(nb_friend-1);
		int[] receive_stop = {0};
		for(Connection con : outcall_connected.values()){
			con.write("ELECTION&LEADER&"+ id);
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			int id_tmp = this.id;
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					try {
						wait_mutex.acquire();
						String answer = con.read();
						if(answer.equals("ANSWER")){
							System.out.format("Ami id %d receive ANSWER\n",id_tmp);
							receive_stop[0] = 1;
							wait_mutex.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			thread.start();
		}
		try {
			Thread.currentThread().sleep(timeout);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(true){
			try {
				wait_mutex.tryAcquire(1, TimeUnit.SECONDS);
				terminate_all(null);
				if(receive_stop[0]==0){
					System.out.println("Ami avec id: "+ this.id+" est elu Leader");
					set_self_leader();
					leader.sendClientLeaderPort(this.port);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
		}
	}

	/**
	 * Annonce du message de victoire par le leader a tout le reseau (sauf le client)
	 */
	public void annonceLeaderAll(){
		String id_string = String.valueOf(id);
		for(Connection con : outcall_connected.values()){
			con.write("COORDINATION&LEADER&"+id_string);
		}
	}

	/**
	 * Methode qui devait declancher le timer de chaque noeud
	 */
	public void startTimer()
	{
		TimerTask task = new TimerTask()
		{
			public void run()
			{
				electionLeader();
			}
		};
		Timer timer = new Timer("Timer");
		timer.schedule(task, this.timeout);
	}

	/**
	 * A chaque ecoulement de l'interval de heartbeat on va executer ce qui est dans la fonction run
	 * Ce qui est executer est le heartbeat message que on va envoyer a tous les autre noeuds.
	 */
	public void startHeartbeatTimer()
	{
		TimerTask task = new TimerTask()
		{
			public void run()
			{
				ExecutorService es = Executors.newFixedThreadPool(outcall_connected.size());
				for(Connection con : outcall_connected.values()) {
					es.execute(new Runnable() {
						public void run() {
							con.write("STARTTIMER"); //node id
							String str = con.read();
						}
					});
				}
			}
		};
		Timer timerHeartbeat = new Timer("Heartbeat");
		timerHeartbeat.schedule(task, base); //Heartbeat interval: 300
	}


	/**
	 * parseur des commandes recus dans le thread de chaque noeud
	 * @param message message recu
	 * @param conf node configuration
	 * @return error_code code d'erreur d'execution
	 * @throws IOException erreur reseau
	 * @throws InterruptedException interruption de threads
	 */
	public int process(String message, Connection conf) throws IOException, InterruptedException {
		StringTokenizer st = new StringTokenizer(message,"&\r\n");
		int nbToken = st.countTokens();
		//System.out.println("Ami process message : "+message+", nb token: "+nbToken);
		String commandMessage = st.nextToken();
		String str, commandToken;
		int termeToken, indexToken;
		switch(commandMessage.toUpperCase()) {
			case "SHUTDOWN ALL":
				System.out.format("Ami id %d receive SHUTDOWN ALL\n",this.id);
				try {
					terminate_all(null);
					listenner.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return 0;
			case "LEADER SHUTDOWN ALL":
				System.out.format("Leader id %d receive SHUTDOWN ALL\n",this.id);
				try {
					terminate_all("SHUTDOWN ALL");
					listenner.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return 0;
			case "COORDINATION":
				if(nbToken < 3)
					return -1;
				str = st.nextToken();
				String str_id_coordination = st.nextToken();
				int id_coordination = Integer.valueOf(str_id_coordination);
				int ports = calculPort(id_coordination);
				System.out.format("Ami id %d  receive COORDINATION %s from id %d\n",this.id,str,id_coordination);
				if (this.follower == null){
					this.follower = new Follower(this);
					//this.isFollower = true;
				}
				if (str.equalsIgnoreCase("FOLLOWER")) {
					follower.addFollower(ports);
				} else if(str.equalsIgnoreCase("LEADER")){
					setLeader_port(ports);
					this.leader_port = ports;
					return -2;/** -2 pour continuer à ecouter */
				} else{
					return -1;
				}
				break;
			case "ELECTION":
				if(nbToken < 3)
					return -1;
				str = st.nextToken();
				String str1 = st.nextToken();
				int id_ami = Integer.parseInt(str1);
				if(str.equalsIgnoreCase("LEADER")&& this.id > id_ami){
					System.out.format("Ami id %d receive ELECTION LEADER from ami id %d\n",this.id,id_ami);
					conf.write("ANSWER");
					try {
						election_leader_mutex.acquire();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(!is_election_leader){
						is_election_leader = true;
						Thread tmp_thread = new Thread(new Runnable() {
							@Override
							public void run() {
								startElectionLeader();
							}
						});
						tmp_thread.start();
					}
					election_leader_mutex.release();
				}
				conf.terminate(null);
				break;
			case "ACCEPTED":
				if(isLeader){
					if(nbToken < 3)
						return -1;
					str = st.nextToken();
					termeToken = Integer.parseInt(str);
					str = st.nextToken();
					commandToken = str;
					leader.notifyClientCommit(this.client_port);
					return -2;
				}
				break;
			case "ACCEPTREQ":
				if(!isLeader){
					if(nbToken < 3)
						return -1;
					int value;
					str = st.nextToken();
					termeToken = Integer.parseInt(str);
					str = st.nextToken();
					indexToken = Integer.parseInt(str);
					str = st.nextToken();
					commandToken = str;
					str = st.nextToken();
					value = Integer.parseInt(str);
					commandToken = commandToken+"&"+value; //&
					int err_code;
					if(logCommand.size() < 1){
						err_code = follower.handleAcceptReq(termeToken,indexToken,commandToken,null, conf);
					} else {
						err_code = follower.handleAcceptReq(termeToken,indexToken,commandToken,logCommand.get(logCommand.size()-1), conf);
					}
					return err_code;
				}
				break;
			case "STARTTIMER":
				startTimer();
			case "CLIENT":
				int error_code;
				if(isLeader){
					if(nbToken < 2)
						return -1;
					str = st.nextToken();
					int value = Integer.parseInt(st.nextToken());
					System.out.format("Leader id %d  receive CLIENT&%s&%d\n",this.id,str,value);
					if(str.equalsIgnoreCase("EXECUTE")){
						error_code = leader.forceGetQuorum(str+"SS"); //mot cle special ou on force l'execution du log
					} else {
						error_code = leader.handleClientRequest(str+"&"+value); //communication avec les followers
						if(error_code == -2){
							leader.getClient_con().write("OK");
						}
						return error_code; // -2 normal keep listening, -5 followers must compute their logs
						// -3 NACK response
					}
				} else {
					System.out.println("TEST");
					str = st.nextToken();
						if(str.equalsIgnoreCase("EXECUTE")) {
							error_code = follower.getQuorum(); //mot cle special ou on force l'execution du log
						}
				}
			case "EXECUTE": //pour forcer le follower a executer la derniere commande recue (fin de l'algorithme)
				if(!isLeader){
					follower.getQuorum();
				}
				return -2;
			case "EXECUTESS": //pour forcer l'obtention des reponses de tous les followers (fin de l'algorithme)
				if(!isLeader){
					follower.getQuorum();
					conf.write("CONFIRM");
				}
				return -2;
			case "ROLLBACK":
				//TODO SEND FOLLOWER SELF LOG
				if(isLeader){

				}
			default:
				// code block
		}
		return 0;

	}

	/**
	 * Demarrer la connexion a UN noeuds du reseau + client
	 * @param port port d'un noeud ou client
	 * @return retourne la config d'un noeud du reseau
	 * @throws IOException erreur reseau
	 */
	public Connection startConnectionToPeers(int port) throws IOException {
		Socket client_socket = new Socket();
		client_socket.connect(new InetSocketAddress("127.0.0.1", port));
		PrintWriter out = new PrintWriter(client_socket.getOutputStream());
		InputStreamReader in = new InputStreamReader(client_socket.getInputStream());
		BufferedReader bf = new BufferedReader(in);
		Connection conf = new Connection(client_socket);
		return conf;
	}

	/**
	 * Demarrer la connexion a TOUS les noeuds du reseau + client
	 * @return map de tous les noeuds du reseau + client
	 * @throws IOException erreur reseau
	 */
	public Map<Integer, Connection> startConnectAllPeers() throws IOException {
		Map<Integer, Connection> peers_connected = new HashMap<Integer, Connection>();
		ExecutorService es = Executors.newCachedThreadPool();
		for(int port_friend : ami_port) {
			//int ami_port = calculPort(id_friend);
			es.execute(new Runnable() {
				public void run() {
					try {
						Connection cn = startConnectionToPeers(port_friend);
						peers_connected.put(port_friend,cn);
						//outcall_connected.put(port_friend,cn);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
		}

		es.shutdown();
		try {
			boolean finished = es.awaitTermination(1, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return peers_connected;
	}

	/**
	 * Wrapper de la connection a tous les noeuds du reseau
	 * @throws IOException erreur reseau
	 */
	public void connectAllPeers() throws IOException {
		if(outcall_connected.isEmpty()){
			this.outcall_connected = startConnectAllPeers();
		} else {
			if (ami_port.size() != outcall_connected.size()) {
				for (int port_ami : ami_port) {
					if (!outcall_connected.containsKey(port_ami)) {
						Connection cn = startConnectionToPeers(port_ami);
						outcall_connected.put(port_ami, cn);
					}
				}
			}
		}
	}

	/**
	 * Termiener la connection avec tous les noeuds du reseau
	 * @param sendShutDown commande d'arret
	 */
	public void terminate_all(String sendShutDown){
		if(!outcall_connected.isEmpty()){
			for(Connection con : outcall_connected.values()){
				con.terminate(sendShutDown);
			}
		}
		outcall_connected.clear();

	}

	/**
	 * Getters and setters
	 *
	 */
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public void incrementIndex(){
		this.index++;
	}
	public void incrementTerme(){
		this.terme = ++terme;
		//System.out.format("Ami avec id %d a incremente son terme\n",this.id);
	}
	public int getId() {
		return id;
	}
	public List<String> getLogs() {
		return logCommand;
	}
	public void setLogCommand(List<String> logCommand) {
		this.logCommand = logCommand;
	}
	public int getTerme() {
		return terme;
	}

	public void setTerme(int terme) {
		this.terme = terme;
	}
	public void setOutcall_connected(Map<Integer, Connection> outcall_connected) {
		this.outcall_connected = outcall_connected;
	}
	public Map<Integer, Connection> getOutcall_connected() {
		return outcall_connected;
	}
	public void set_friend_port(int ... friend_port){
		this.friend_port = friend_port;
	}

	public void setfollower() throws IOException {
		follower = new Follower(this);
	}

	protected int getComputedConsensus() {
		return this.computed_consensus;
	}

	public void set_self_leader(){
		if(leader == null)
			leader = new Leader(this);
		else
			leader.reset();

		isLeader = true;
		try {
			connectAllPeers();
			annonceLeaderAll();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setFollower_id(int ... followerId) throws IOException {
		List<Integer> follower_port = new ArrayList<Integer>();
		for(int f_id : followerId){
			if(this.id == f_id){
				this.isFollower = true;
				this.follower = new Follower(this);
			}

			else
				follower_port.add(calculPort(f_id));
		}
		follower.setFollower_port(follower_port);
	}

	public void setLeader_port(int port){
		this.leader_port = port;
	}
}