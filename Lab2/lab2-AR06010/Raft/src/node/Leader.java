package node;
/**
 * Cette classe permet d'instancier un leader
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import shared.Constants;
import requestHandler.Connection;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;

public class Leader extends Ami{

	//Common objects
	private Ami ami;

	//leader parameters
	private int terme;

	//private int index=0;
	private String old_value_decided;
	private final int nbQuorom;
	private int proposal_nb;
	private int nb_answer_promise;
	private int nb_answer_acceptreq;
	private int nb_nack;
	private int nb_quorom;
	private int nb_follower_connected;
	private int client_port;

	//mutexes
	private Semaphore wait_mutex;
	private Semaphore data_mutex;

	//Server config
	private List<Integer> follower_port;

	public Connection getClient_con() {
		return client_con;
	}

	private Connection client_con;

	private Thread thread_one;    /** Thread principal pour laeder */

	public Leader(Ami ami) {
		this.client_port = ami.calculPort(0);
		this.terme = ami.getTerme();
		this.ami = ami;
		this.nbQuorom = ((ami.nb_friend-1)/2) + 1;
		this.client_con = null;
	}


	public void reset(){
		this.proposal_nb = 0;
		old_value_decided = null;
	}

	public void sendClientLeaderPort(int port) {
		System.out.format("Leader id %d  send LeaderPort(%d)\n",this.ami.id,port);
		if(client_con == null){
			try {
				client_con = ami.startConnectionToPeers(ami.client_port);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		client_con.write("LeaderPort&"+port);
	}

	public void notifyClientCommit(int port){
		if(client_con != null){
			try {
				client_con = ami.startConnectionToPeers(ami.client_port);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		client_con.write("COMMIT&"+ami.getLogs().get(getLogs().size()-1));
	}
	public void sendClientComputedResult(int port){
		if(client_con != null){
			try {
				client_con = ami.startConnectionToPeers(ami.client_port);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		client_con.write("Result&"+ami.getComputedConsensus());
	}


	public int sendAcceptRequest(int terme, int index, String command, String prev_command) {
		nb_nack = 0;
		nb_quorom = 0;
		nb_answer_promise = 0;
		Map<Integer, Connection> outcall_connected = ami.getOutcall_connected();
		nb_follower_connected = outcall_connected.size();
		wait_mutex = new Semaphore(nb_follower_connected);
		data_mutex = new Semaphore(1);

		for(Connection con : outcall_connected.values()){
			con.write(Constants.ACCEPTREQ_COMMAND+"&"+terme+"&"+index+"&"+command+"&"+prev_command);
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					try {
						wait_mutex.acquire();
						String answer = con.read();
						int result = process(answer);
						if(result == -1){
							wait_mutex.release();
							//TODO Handle error
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			thread.start();
		}
		try {
			thread_one.sleep(300);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(true){
			try {
				wait_mutex.acquire();
				data_mutex.acquire();
				int nb_answers = this.nb_answer_promise;
				int nack_answers = this.nb_nack;
				int quorom_answers = this.nb_quorom;
				data_mutex.release();
				if(quorom_answers >= nbQuorom){
					System.out.println("Leader quorum received, now executing command");
					ami.executeCommand(); // leader execute sa propre commande
					nb_nack = 0;
					nb_quorom = 0;
					nb_answer_promise = 0;
					if(this.terme == terme){
						return -2; //Continuer a ecouter pour d'autre requetes
					} else {

						return -4; /**-4 wrong term, start new election*/
					}
				}
				if(nack_answers >= nbQuorom){
					System.out.println("Leader quorum NACK");
					//TODO
					return -3; /** -2 pour recommencer */
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
		}
		return 0;
	}

	/**
	 * A la suite d'une requete du client, envoie les messages acceptReq aux followers
	 * @param message
	 * @return
	 */
	public int handleClientRequest(String message) {
		ami.getLogs().add(message);
		ami.incrementIndex();
		if(ami.getLogs().size() < 2){
			return sendAcceptRequest(ami.getTerme(), ami.getIndex(), ami.getLogs().get(0), null);
		} else {
			return sendAcceptRequest(ami.getTerme(), ami.getIndex(), ami.getLogs().get(ami.getIndex()-1), ami.getLogs().get(ami.getIndex()-2));
		}
	}

	public int forceGetQuorum(String message) {
		nb_quorom = 0;
		Map<Integer, Connection> outcall_connected = ami.getOutcall_connected();
		nb_follower_connected = outcall_connected.size();
		wait_mutex = new Semaphore(nb_follower_connected);
		data_mutex = new Semaphore(1);

		for(Connection con : outcall_connected.values()){
			con.write(message);
			int port_connected = con.socket.getPort();
			String port_str = Integer.toString(port_connected);
			Thread thread = new Thread("Thread "+port_str) {
				public void run(){
					try {
						wait_mutex.acquire();
						String answer = con.read();
						data_mutex.acquire();
						nb_quorom++;
						int nb_quorom_tmp = nb_quorom;
						data_mutex.release();
						if(nb_quorom_tmp > nb_follower_connected/2){
							wait_mutex.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}finally {

					}
				}
			};
			thread.start();
		}
		try {
			thread_one.sleep(300);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(true){
			try {
				wait_mutex.acquire();
				data_mutex.acquire();
				int quorom_answers = this.nb_quorom;
				data_mutex.release();
				if(quorom_answers > nb_follower_connected/2){
					System.out.println("Leader quorum received, sending anwser to client");
					//ami.executeCommand(); // leader execute sa propre commande
					client_con.write("CLIENTRES&"+ami.getComputedConsensus());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
		}
		return -2;
	}

	/**
	 * Parseur pour le leader
	 * @param message
	 * @return
	 */
	public int process(String message) throws InterruptedException {
		StringTokenizer stt = new StringTokenizer(message,"\r\n");
		String command_tmp = stt.nextToken();
		StringTokenizer st = new StringTokenizer(command_tmp,"&");
		int nbToken = st.countTokens();
		String command = st.nextToken();
		String str, commandToken;
		int indexToken;
		int nb_answers = 0;
		switch (command.toUpperCase()) {
			case Constants.NACK_COMMAND -> {
				data_mutex.acquire();
				nb_answer_promise++;
				nb_nack++;
				nb_answers = nb_answer_promise;
				int nack_nb = nb_nack;
				data_mutex.release();
				if (nb_answers == nb_follower_connected) {
					wait_mutex.release();
				}
			}
			case Constants.ACCEPTED_RESPONSE -> {
				if(nbToken < 3)
					return -1;
				str = st.nextToken();
				indexToken = Integer.parseInt(str);
				commandToken = st.nextToken();
				try {
					data_mutex.acquire();
					if (ami.getIndex() != indexToken){
						System.out.println("Leader "+ami.getId()+": Erreur acceptRequest follower, indice invalide current Leader Index :"+ami.getIndex());
						System.out.println("Got Follower -> Index : "+indexToken);
						data_mutex.release();
						return -1;
					}
					nb_quorom++;
					nb_answer_acceptreq++;
					nb_answers = nb_answer_acceptreq;
					int quorom_nb = nb_quorom;
					data_mutex.release();
					if (quorom_nb > nb_follower_connected / 2 || nb_answers == nb_follower_connected) {
						wait_mutex.release(); /** On reveille le thread parent*/
					} else {
						//System.out.println("NOT RELEASED");
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
		return 0;
	}
}