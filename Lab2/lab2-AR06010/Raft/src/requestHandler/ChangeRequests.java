package requestHandler;

import java.nio.channels.SelectionKey;

public class ChangeRequests {

    public SelectionKey selectionKey;
    public int ops;

    public ChangeRequests(SelectionKey selectionKey, int ops) {
        this.selectionKey = selectionKey;
        this.ops = ops;
    }
}