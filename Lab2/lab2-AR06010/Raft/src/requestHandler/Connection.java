package requestHandler;
/**
 * Cette classe permet d'instancier une configuration reseau d'un noeud
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import shared.Constants;
import java.io.*;
import java.net.Socket;

public class Connection {

	public Thread thread_listennerIncome;
	public Socket socket;
	public PrintWriter out;
	public BufferedReader in;
	public int id;

	public Connection(Socket socket){
		this.socket = socket;
		this.thread_listennerIncome = null;
		try {
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING));
			this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public Connection(Socket socket,int id){
		this.socket = socket;
		this.thread_listennerIncome = null;
		try {
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING));
			this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.id = id;

	}

	public String read()  {
		try {
			String firstLine;
			while ((firstLine = in.readLine()) != null) {
				return firstLine;
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}

		return "ERROR";
	}

	/**
	 * Permet d'envoyer un message a un noeud
	 * @param message
	 */
	public void write(String message) {
		out.write(message+ "\r\n");
		out.flush();
	}
	public void terminate(String sendShutDown){
		if(sendShutDown != null){
			write(sendShutDown);
		}
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// This is telnet specific, maybe you have to change it according to your
	// protocol
	private boolean checkIfFinished(byte[] data) {
		int length = data.length;
		if (length < 3) {
			return false;
		} else {
			if (data[length - 1] == '\n') {
				if (data[length - 2] == '\r') {
					return true;
				}
			}
			return false;
		}
	}
}