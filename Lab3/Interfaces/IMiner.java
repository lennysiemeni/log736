package Interfaces;

import Classes.Block;
import Classes.Connection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public interface IMiner {
    //set target's first zeros number
    int difficulty = 5;

    //returns the genesis block
    ArrayList<Block> init() throws IOException;

    //returns a list of connected nodes
    Map<Integer, Connection> connect();
    //calls mineBlock method whenever it collects transactions and validates received blocks and adds it to the current chain
    void listenToNetwork()throws IOException;

    ArrayList<Block> synchronise () throws IOException;
}
