package Classes;
/**
 * Cette classe permet d'instancier un miner
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import Interfaces.IMiner;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.System.currentTimeMillis;

/**
 * Le miner est l’élément clé dans le réseau de bitcoin.
 * C’est le noeud qui permet de créer des blocs et les ajouter à la blockchain
 * après avoir fait les calculs qui mènent au consensus (Proof of work)
 */
public class Miner implements IMiner {

    //Attributes
    public final static int BLOCK_SIZE = 10;
    public final int port;
    public long target;
    public long id;
    private int clientPort;
    private int localPort;
    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    private int nbMiner; /** Pour simplifier on va considérer que le miner est connecté avec tous les autres miners */

    private int foreign_miner_count;

    public ArrayList<Block> blockchain;
    //private ArrayList<ArrayList<Block>> branches = new ArrayList<>(10);
    private ArrayList<ArrayList<Block>> branches = new ArrayList<ArrayList<Block>>(10); /** (s’il y en a des branches) */


    private ArrayList<Integer> neighborNodes; /** (Pour simplifier on va considérer que le miner est connecté avec tous les autres miners) */
    public LinkedList<Transaction> mempool; /** sert à cumuler les transactions qui ne sont pas encore incluses dans un bloc. */

    public ServerSocket listenner;
    public Thread thread_listennerIncome;
    public Map<Integer, Connection> miner_outcall_connected;
    public Map<Long, Connection> client_connected;

    private Semaphore mutex_pool_sets;
    private Semaphore mutex_blockchain_sets;
    private Semaphore mining_lock;

    //Flags
    private boolean is_minning;
    private boolean asked_for_sync = false;

    //Constructor
    //Ici les parametres du constructeur on ete change
    //Puisque l'on utilise une approche similaire pour la connexion entre les peers
    //Avec ce que l'on a fait dans le Lab1
    public Miner (long id, int nbMiner){
        this.id = id;
        this.port = calculPort(id);
        this.nbMiner = nbMiner;
        this.neighborNodes = neighborNodesPort();
        this.blockchain = init();
        this.branches = new ArrayList<>();
        this.branches.add(new ArrayList<Block>());
        this.foreign_miner_count = 0;
        mutex_blockchain_sets = new Semaphore(1);
        mutex_pool_sets = new Semaphore(1);
        this.mempool = new LinkedList<Transaction>();
        this.is_minning = false;
        this.miner_outcall_connected = new HashMap<Integer, Connection>();
        this.client_connected = new HashMap<Long, Connection>();
        thread_listennerIncome = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    listenToNetwork();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread_listennerIncome.start();
    }

    /**
     * Methode utilitaire pour acceder aux port des mineurs instancies a partir de la classe Miner.java
     * @param id l'identifiant du miner
     * @return port miner
     */
    public int calculPort(long id){
        int tmp_id = (int) id;
        return 25100 + tmp_id * 100;
    }

    //used whenever a miner finds a branch longer than its blockchain -> we have no use for this now (from original API)
    private void setLongestChain(){
        //TODO
    }

    /**
     * Cette methode active le mineur et le passe a l'etat "is_minning"
     * puis envoies le message de vainqueur au reste du reseau
     * @param time_sleep temps d'attente avant de cinnebcer a miner
     * @return error_code indiquant le statut de l'execution
     */
    public int mine_and_send(int time_sleep){
        System.out.format("Miner %d start with blockchaindepth: %d \n",id,blockchain.size());
        int depth_int = blockchain.size();
        this.is_minning = true;
        Utils.forceThreadSleepSeconde(Thread.currentThread(),time_sleep);
        Block bloc = mineBlock(depth_int);
        if(is_minning){
            this.is_minning = false;
            System.out.format("Miner id %d send winnerBloc with bloc_depth: %d\n",id,bloc.depth);
            String separator = "%%";
            broadcast_message(Constants.WINNNER_COMMAND+separator+bloc.toString());
            addBlock(bloc);
            return 0;
        }
        System.out.format("Miner %d didn't win with bloc_depth: %d\n",id,depth_int);
        return -1;
    }
    /**
     * This method generates a new block, finds the corresponding PoW and sets the block's hash and nounce
     * @param depth la profondeur du block a miner
     * @return le bloc mine
     */
    public Block mineBlock(int depth){
        if(!is_minning) {
            return null;
        }
        int ind_blc = 0;
        List<Long> tx_array = new ArrayList<>();
        while(ind_blc<BLOCK_SIZE){
            if(!is_minning){
                return null;
            }
            try{
                Transaction tmp_tx = mempool.get(ind_blc);
                tx_array.add(tmp_tx.getTxID());
                ind_blc++;
            }catch (IndexOutOfBoundsException e){
                break;
            }


        }
        int random = 6;
        Long transaction_coinbase_ID = random+ id;
        tx_array.add(0,transaction_coinbase_ID);
        //Genereate block //TODO : What depth to insert ? maybe blockchain.size()
        Block bloc = new Block(blockchain.get(blockchain.size()-1).getBlockHash(), tx_array, depth, currentTimeMillis());
        String bloc_hashed = bloc.calculateBlockHash();
        int nounce_tmp = 0;
        bloc.setNounce(nounce_tmp);
        while(calculatNumberFirstZero(bloc.calculateBlockHash())<target){
            if(!is_minning){
                return null;
            }
            nounce_tmp++;
            bloc.setNounce(nounce_tmp);
        }
        return bloc;

    }

    /**
     * Cette methode determine le nombre de zero dans une chaine de caractere (pour le nounce generalement)
     * @param data les donnees analyser
     * @return le nombre de zeros trouves dans la chaine
     */
    public int calculatNumberFirstZero(String data){
        char zero = '0';
        int index = 0;
        while(data.charAt(index) == zero)
            index++;
        return index;
    }

    /**
     * Cett methode permet d'ajouter un block dans une blockchain
     * @param block block a ajouter
     * @return error_code sur l'etat de l'execution (0 : normal, -1 : error)
     */
    public int addBlock(Block block) {
        int error_code = 0;
        try {
            mutex_blockchain_sets.acquire();
            if(block.depth != blockchain.size()){
                mutex_blockchain_sets.release();
                return -1;
            }
            blockchain.add(block);
            mutex_blockchain_sets.release();
            List<Long> tx_tmp = block.getTransactions();
            for(Long tx_id : tx_tmp){
                mempool.remove(tx_id);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return error_code;
    }

    /**
     * Cette methode permet de valider un bloc recu
     * @param currentReceivedBlock le bloc a valider
     * @return boolean si le blcok est valide ou non
     */
    private boolean validateBlock(Block currentReceivedBlock){

        if(calculatNumberFirstZero(currentReceivedBlock.getBlockHash())>=target){
            Block previousLocalBlock = this.blockchain.get(this.blockchain.size()-1);
            return currentReceivedBlock.getPreviousHash().equals(previousLocalBlock.getBlockHash());
        }
        return false;
    }

    /**
     * Cette methode permet d'ajouter une transaction au mempool
     * @param tx la transaction a ajouter au mempool
     */
    private void addToMemPool(Transaction tx){
        try {
            mutex_pool_sets.acquire();
            mempool.add(tx);
            mutex_pool_sets.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Cette methode permet d'enlever une transaction du mempool
     * @return retourne la trensaction qui a ete enlevee du mempool
     */
    private Transaction popFromMemPool(){
        Transaction tmp = null;
        try {
            mutex_pool_sets.acquire();
            if(!mempool.isEmpty())
                tmp = mempool.removeFirst();
            mutex_pool_sets.release();
        } catch (InterruptedException|IndexOutOfBoundsException e) {
            tmp = null;
        }
        return tmp;
    }

    /**
     * Cette methode initialise et construit le bloc de genese dans la blockchain
     * @return le bloc de genese
     */
    public ArrayList<Block> init() {
        //https://bitcoin.fr/bloc-genesis/
        String blocHash = "000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f";
        String previousHash = "0000000000000000000000000000000000000000000000000000000000000000";
        int nounce = 2083236893;
        String format = "yyyy-MM-dd hh:mm:ss";
        String time = "2009-01-03 18:15:05";
        long timestamp = -1;
        try {
            timestamp = Utils.parse_time(time,format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Block b1 = new Block(previousHash,blocHash, null, 0, timestamp,nounce);
        b1.setBlockHash(blocHash);
        ArrayList<Block> blockchain_tmp = new ArrayList<>();
        blockchain_tmp.add(b1);
        return blockchain_tmp;
    }

    /**
     * Cette methode assigne la liste des ports des autres miners du reseau
     * Pour simplifier on va considérer que le miner est connecté avec tous les autres miners
     *
     * @return la liste des ports d'ecoute des autres nodes du reseau
     *
     * */
    public ArrayList<Integer> neighborNodesPort() {
        ArrayList<Integer> neighborPort = new ArrayList<Integer>();
        for (long i = 0; i < this.nbMiner; i++) {
            if (i != id) {
                int port_miner = this.calculPort(i);
                neighborPort.add(port_miner);
            }
        }
        return neighborPort;
    }

    /**
     * Cette methode initialise le processus de connection a tous les autres peers du reseau
     *
     */
    public void connectToAll(){
        miner_outcall_connected = connect();
    }

    //returns a list of connected nodes
    public Map<Integer, Connection> connect() {
        Map<Integer, Connection> peers_connected = new HashMap<Integer, Connection>();
        ExecutorService es = Executors.newCachedThreadPool();
        for(int port_miner : neighborNodes) {
            if(miner_outcall_connected.isEmpty() || !miner_outcall_connected.containsKey(port_miner))
                es.execute(new Runnable() {
                    public void run() {
                        try {
                            Connection cn = Utils.startConnectionToPeers(port_miner);
                            //send_message(Utils.Miner_to_miner_message_type.HANDSHAKE.name(),cn);
                            peers_connected.put(port_miner,cn);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

        }

        es.shutdown();
        try {
            boolean finished = false;
            while(!finished){
                finished = es.awaitTermination(1, TimeUnit.MINUTES);
                if(!finished ){
                    //Maybe panne prcessor detected
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return peers_connected;
    }
    /**
     * Cette methode process les commandes recues par le client connecte au miner
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen)
     * (if -1 then ERROR)
     * */
    public int client_process(String message,long id_client, Connection conf){
        System.out.format("Miner id: %d received from: client id: %d the message: %s  \n",id,conf.id,message);
        int code_error = 1;
        String[] result = message.split("%%");
        String command = result[0];
        switch (command.toUpperCase()) {
            case "NEWTX":
                Transaction tr = new Transaction(Long.parseLong(result[1]));
                addToMemPool(tr);
                broadcast_message(Constants.MEMPOOL_COMMAND+"%%"+result[1]);

                break;
            case "LASTDEPTH":
                send_message(String.valueOf("LASTDEPTH%%"+blockchain.size()),conf);
                break;
            case "TXDEPTH":
                Long txid = Long.parseLong(result[1]);
                int depth = -1;
                for(Block bl : this.blockchain){
                    if(bl.is_txID_exist(txid)){
                        depth = bl.depth;
                        break;
                    }
                }
                send_message("TXDEPTH%%"+String.valueOf(depth),conf);
                break;
            default:
                code_error = -1;
        }

        return code_error;
    }

    public void setBranches(ArrayList<ArrayList<Block>> branches) {
        this.branches = branches;
    }

    /**
     * Cette methode process les commandes recues par un/les autres miners connectes au miner dans le reseau
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen)
     * (if -1 then ERROR)
     * */
    public int miner_process(String message,long id_miner, Connection conf){
        System.out.format("Miner id: %d received from: miner id: %d the message: %s  \n",id,conf.id,message);
        String[] result = message.split("%%");
        String command = result[0];
        int code_error = 1;
        String separator = "##";
        switch (command.toUpperCase()) {
            //Le mineur recoit le message du vaincquer qui a trouve la solution au puzzle
            case Constants.WINNNER_COMMAND:
                String[] parametres = result[1].split(separator);
                int depth = Integer.parseInt(parametres[0]);
                String blockHash = parametres[1];
                String previousHash = parametres[2];
                int nounce = Integer.parseInt(parametres[4]);
                Block bl_tmp = new Block(previousHash,blockHash,depth,nounce);
                if(validateBlock(bl_tmp)) {
                    this.is_minning = false;
                    System.out.format("Miner id %d validate bloc depth %d\n",id,depth);
                    String tr_str = parametres[3];
                    String coupe = tr_str.substring(1,tr_str.length()-1); //remove [ ]
                    List<String>  block_transactions = Arrays.asList(coupe.split(", "));
                    List<Long> transactionsB = block_transactions.stream().map(Long::parseLong).collect(Collectors.toList());
                    //System.out.println(transactionsB);
                    bl_tmp.setTransactions(transactionsB);
                    this.addBlock(bl_tmp);
                }
//                if(validateBlock(bl_tmp)){
//                    this.addBlock(bl_tmp);
//                }
                break;
            case Constants.MEMPOOL_COMMAND:
                Transaction tx_tmp = new Transaction(Long.parseLong(result[1]));
                addToMemPool(tx_tmp);
                break;
            case Constants.MEMPOOLTMP_COMMAND:
                try {
                    String dataB = "";
                    for (Transaction t : this.mempool) {
                        dataB += t.getTxID() + " / ";
                    }
                    //System.out.println(this.id + " - BEFORE: " + dataB);

                    String sep = "##";
                    String[] params = result[1].split(sep);

                    //Now parse data in params[0] -> t.getTxID() + "&&" + t.isConfirmed() + "##";
                    for (int i = 1; i < params.length; i++) {
                        String argu = params[i];
                        String[] dudes = argu.split("&&");
                        long txsId = Long.parseLong(dudes[0]);

                        for (Transaction tx : this.mempool) {
                            if (tx.getTxID() == txsId) {
                                continue;
                            }
                        }
                        Transaction t = new Transaction(txsId);
                        boolean confirmed = Boolean.parseBoolean(dudes[1]);
                        if (confirmed) {
                            t.setConfirmed();
                        }
                        this.mempool.add(t);
                    }

                    String dataA = "";
                    for (Transaction t : this.mempool) {
                        dataA += t.getTxID() + " / ";
                    }
                    //System.out.println(this.id + " - AFTER: " + dataA);
                }
                catch(ConcurrentModificationException | NullPointerException e){
                    //System.out.println();
                }
            case Constants.SYNCHRONIZE_COMMAND:
                code_error = send_self_blockchain();
                break;
            case Constants.CHAIN_COMMAND:
                //If miner asked for synchronisation, parse the message
                if(this.asked_for_sync){
                    String block_delimiter = "//->//";
                    String[] block_data = result[1].split(block_delimiter);
                    ArrayList<Block> branch_list = new ArrayList<>();
                    for(int i = 0; i<block_data.length; i++){
                        String[] blocks = block_data[i].split(separator);
                        int block_depth = Integer.parseInt(blocks[0]);
                        String block_Hash = blocks[1];
                        String previous_Hash = blocks[2];
                        String block_tr = blocks[3];
                        String coupe_tr = block_tr.substring(1,block_tr.length()-1); //remove [ ]
                        List<Long> transactionsB = null;
                        if(coupe_tr.length() != 0){
                            List<String>  block_transactions = Arrays.asList(coupe_tr.split(", "));
                            transactionsB = block_transactions.stream().map(Long::parseLong).collect(Collectors.toList());
                            //System.out.println(transactionsB);
                        }

                        //convertStringListToIntList(block_transactions, Integer::parseInt);
                        int block_nounce = Integer.parseInt(blocks[4]);
                        Block block_tmp = new Block(previous_Hash,block_Hash,transactionsB,block_depth,-1,block_nounce);
                        //block_tmp.getBlockHash();
                        branch_list.add(block_tmp);
//                        StringBuilder temp_miner = new StringBuilder();
//                        for(Block bc : branch_list){
//                            temp_miner.append(bc.toString());
//                        }
//                        System.out.println("Miner "+this.id+" ADDING CHAIN :"+ temp_miner);
                        this.branches.add(foreign_miner_count, branch_list);
                    }
                    // We added a blockchain of one branch, we move onto the next one
                    foreign_miner_count++;
                }
                break;
            case Constants.HELLOWORLD_COMMAND:
                break;
            default:
                code_error = -1;
        }
        return code_error;
    }

    /**
     * Cette methode permet a un mineur d'envoyer tout sa blockchain aux autres mineurs
     * generalement a la suite d'une demande de synchronisation entre les mineurs
     * @return code d'erreur si le processus s'est bien deroule
     */
    private int send_self_blockchain(){
        try{
            StringBuilder result = new StringBuilder(Constants.CHAIN_COMMAND+"%%");
            String separator = "//->//";
            for(Block bl : this.blockchain){
                result.append(bl.toString()).append(separator);
            }
            String message_to_send = result.substring(0,result.length()-6);
            broadcast_message(message_to_send);
        } catch (Exception e){
            e.printStackTrace();
            return -1;
        }
        return 1; //TODO 0?
    }



    /**
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen) (if -1 then ERROR)
     * */
    public int startProcess(String message, Connection conf){
        int code_error = 0;
        String messagetmp = message;
        String message_received = null;
        //String from = null;
        String fromtype = null;
        long fromID = -1;
        String[] result = messagetmp.split("@@");
        int nb_message = result.length;
        if(nb_message ==  1){
            message_received = result[0];
            code_error = general_process(message_received, conf);
        }
        else {
            String from = result[0];
            String[] result_1 = from.split(" ");
            fromtype = result_1[0];
            fromID = Long.parseLong(result_1[1]);
            message_received = result[1];
            switch (fromtype.toUpperCase()) {
                case Constants.MINER_COMMAND:
                    code_error = miner_process(message_received,fromID, conf);
                    break;
                case Constants.CLIENT_COMMAND:
                    code_error = client_process(message_received,fromID, conf);
                    break;
                default:
                    code_error = -1;
            }
        }
        return code_error;
    }

    /**
     * Cette methode permet d'envoyer un message general au autres mienurs
     * @param message le message a envoyer
     * @param conf la topographie reseau avec les autres mineurs
     */
    private void send_message(String message,Connection conf){
        String data_send = "Miner "+this.id+"@@" + message;
        conf.write(data_send);
    }

    /**
     * Cett methode permet d'envoyer un message a tout les peers du reseau en broadcast
     * @param message le message a envoyer
     */
    public void broadcast_message(String message){
        for(Connection conf : miner_outcall_connected.values()){
            send_message(message,conf);
        }
    }
    /**
     * Cette methode permet de maintenir le mineur en etat d'ecoute
     * @param conf la configuration du reseau avec les autres peers
     * @return code d'erreur selon l'execution et le traitement des messages recus
     *
     * code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen)
     * (if 2 then synchrone_listen) (if -1 then ERROR)
     * */
    public void listen_to_connected(Connection conf){
        int code_error = 1;
        int timeout = -1;
        String answer = Utils.asynchrone_listen(conf);
        String[] result = answer.split("@@" );
        int nb_message = result.length;
        if(nb_message ==  1){
            code_error = general_process(result[0], conf);
        }
        else {
            String from = result[0];
            String[] result_1 = from.split(" ");
            String fromtype = result_1[0];
            long fromID = Long.parseLong(result_1[1]);
            switch (fromtype.toUpperCase()) {
                case Constants.MINER_COMMAND:
                    System.out.format("Miner id: %d have new connection with Miner id: %d \n",id,fromID);
                    conf.id = fromID;
                    conf.Role = Utils.Role.MINER;
                    code_error = miner_process(result[1],fromID, conf);
                    break;
                case Constants.CLIENT_COMMAND:
                    System.out.format("Miner id: %d have new connection with Client id: %d \n",id,fromID);
                    conf.id = fromID;
                    conf.Role = Utils.Role.CLIENT;
                    client_connected.put(fromID,conf);
                    code_error = client_process(result[1],fromID, conf);
                    break;
                default:
                    code_error = -1;
            }
        }
        while(code_error > 0 ) {
            if(code_error == 1){
                answer = Utils.asynchrone_listen(conf);
                if(answer != null){
                    code_error = startProcess(answer, conf);
                }
            }else
            if(code_error == 2){
                timeout = Utils.generate_random_timeout();
                answer = Utils.synchrone_listen(conf,timeout);
                if(answer != null){
                    code_error = startProcess(answer, conf);
                }else{
                    //handle panne processus or communication
                }
            }
        }
        if (code_error == -1) {
            //HandleError
        }
        conf.terminate(null);
    }

    //calls mineBlock method whenever it collects transactions and validates received blocks and adds it to
    // the current chain
    public void listenToNetwork()throws IOException{
        Socket socket = null;
        //System.out.println("Miner id:"+String.valueOf(id)+ " is listening.");
        try {
            listenner = new ServerSocket();
            //bind the socketserver only to localhost
            listenner.bind(new InetSocketAddress("127.0.0.1", port));
            try {
                while (true) {
                    try {
                        socket = listenner.accept();
                    } catch (Exception e) {
                        //System.out.format("Noeud id %d closing server\n",id);
                        break;
                    }
                    Connection conf = new Connection(socket);
                    Thread thread = new Thread() {
                        public void run(){
                            listen_to_connected(conf);
                        }
                    };
                    thread.start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Server error");
        }

    }

    /**
     * Synchronise method to sync local blockchain to the longest in the network
     * First, compare the branches, get the longest, and insert it in self blockchain
     * @return the newly synchronised blockchain
     * @throws IOException
     */
    public ArrayList<Block> synchronise() throws IOException {
        if (this.branches != null) {
            int index_of_longest_chain = 0;
            int longest_chain_size = 0;
            int temp_chain_size = 0;
            //Determine the longest chain of blocks
            for (int i = 0; i < this.branches.size(); i++) {
                temp_chain_size = this.branches.get(i).size();
                //Not <= here, so we will take the FCFS approach to return the first longest chain in case 2 branches
                //have the same size
                if (longest_chain_size < temp_chain_size) {
                    index_of_longest_chain = i;
                    longest_chain_size = temp_chain_size;
                }
            }
            while (!this.branches.get(index_of_longest_chain).get(0).getPreviousHash().equals(this.blockchain.get(this.blockchain.size() - 1).getBlockHash()) && this.blockchain.size() > 1) {
                this.blockchain.remove(this.blockchain.size() - 1);
            }
            //Insert blocks from the newly validated branch into the blockchain
            for (int i = 0; i < longest_chain_size; i++) {
                this.blockchain.add(branches.get(index_of_longest_chain).get(i));
            }
            //Reset the list of branches and indexes
            foreign_miner_count = 0;
            this.branches.clear();
            return this.blockchain;
        } else {
            return null;
        }
    }

    /**
     * Cette methode initie le protocole de bavadarge
     * Quand un miner recoit une nouvelle requete pour une transaction
     * de la part du client, le mineur le fait savoir au autres miners du reseau
     */
    public void bavardage()
    {
        //Each miner should just broadcast a message with their mempool.
        //Somewhere else il faut controler que chaque miner appelle ca fontion bavardage
        //Il faut pas que la fonction bavardage fait des appelle recursive
        //So peut etre le client peut appeller tous les miners pour leur dire un a un?
        String data = "";
        for(Transaction t : this.mempool)
        {
            data += t.getTxID() + "&&" + t.isConfirmed() + "##";
        }

        if(data.length() > 2)
        {
            data = data.substring(0, data.length() - 2);
        }
        //System.out.println("Broadcast: MEMEPOOL%%"+this.id+"##"+data);
        broadcast_message(Constants.MEMPOOLTMP_COMMAND+"%%"+this.id+"##"+data);
    }

    //Accessor Utile pour les tests unitaires dans la classe MainClass.java
    public void setAsked_for_sync(boolean asked_for_sync) {
        this.asked_for_sync = asked_for_sync;
    }

    /**
     * @return code_error: (if 0 then stop listenning) (if 1 then asynchrone_listen) (if 2 then synchrone_listen)
     * (if -1 then ERROR)
     * */
    public int general_process(String message, Connection conf){
        //System.out.format("Miner id: %d received from: %s id: %d the message: %s  \n",id,conf.Role.name(),conf.id,message);
        int code_error = 1;
        //TODO PARSE GENERAL COMMAND
        return code_error;
    }
}
