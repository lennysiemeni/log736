package Classes;
/**
 * Cette classe permet d'instancier le client du reaseau de blockchain
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import Interfaces.IClient;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.net.DatagramSocket;
import java.io.IOException;


/**
 * Un client est responsable de la génération des transactions. Il est connecté à un miner auquel il envoie ses transactions.
 */
public class Client implements IClient {


    //Attributes
    private long clientID;
    public final static int CONFIRMATION_BLOCK_NUMBER = 1;
    private int minerPort,localPort;
    private Connection minerCon;
    private int lastdepth;
    private Semaphore trans_lock;
    private ArrayList<Transaction> transactions;


    //Constructor
    public Client (long clientID){
        this.clientID = clientID;
        this.localPort = calculClientPort(clientID);
        trans_lock = new Semaphore(1);
        transactions = new ArrayList<>();
    }

    /**
     * Methode utilitaire pour acceder aux port des mineurs instancies a partir de la classe Miner.java
     * @param id l'identifiant du miner
     * @return port miner
     */
    public int calculClientPort(long id){
        int tmp_id = (int) id;
        return 25000 + tmp_id * 100;
    }

    /**
     * Initialiser le port du mineur auquel le client est connecte
     * @param minerPort port du miner
     */
    public void setMinerPort(int minerPort) {
        this.minerPort = minerPort;
    }

    //CLIENT Commands
    private String INIT_COMMNAND = Constants.CLIENT_COMMAND+this.clientID+"&INIT";


    /**
     * Cette methode est responsable de la création des transactions.
     *
     * */
    private Transaction createTransaction(){
        Transaction tx = null;
        /** (Pour simplifier, une transaction est représentée par un identifiant unique)*/
        try {
            trans_lock.acquire();
            long idTemp = Utils.calcul_txID_unique(clientID,transactions.size());
            tx = new Transaction(this.clientID,idTemp);
            transactions.add(tx);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return tx;
    }

    /**
     * Cette methode envoie une commande du client vers un mineur et se place en ecoute en attente
     * d'une reponse
     * @param message message a envoyer
     * @return process la reponse recue du mineur en retour
     */
    private int miner_send_receive(String message) {
        String data_send = "Client "+clientID+"@@" + message;
        minerCon.write(data_send);
        String answ = minerCon.read();
        if(answ == null){
            //TODO HANDLE READ ERROR
        }
        return process(answ);
    }

    /**
     * Cette methode envoie un message a un mineur sans attendre de reponse
     * @param message message a envoyer
     */
    public void miner_send(String message){
        String data_send = "Client "+clientID+"@@" + message;
        minerCon.write(data_send);
    }

    /**
     * Cette methode envoie une transaction via le protocole reseau a un mineur
     * @param tx la transaction a envoyer
     */
    public void sendTX(Transaction tx) {
        String message = "NEWTX" + "%%" + String.valueOf(tx.getTxID());
        miner_send(message);
    }

    /**
     * Cette methode demande au miner la profondeur du dernier bloc dans sa blockchain.
     * @return retourne la profondeur du dernier bloc
     *
     */
    public int getLastBlockDepth() {
        return miner_send_receive(Constants.LASTDEPTH_COMMAND);
    }

    /**
     * Cette methode demande au miner la profondeur du bloc qui contient une certaine transaction représentée par son ID.
     * @return retourne la profondeur du bloc qui contient la transaction demandee
     *
     */
    private int getTxBlockDepth(long id){
        int index = Utils.txID_to_index(clientID,id);
        Transaction trs = null;
        try{
            trs = transactions.get(index);
        } catch (IndexOutOfBoundsException e){
            return -1;
        }
        return miner_send_receive(Constants.TXDEPTH_COMMAND+"%%"+ String.valueOf(id));
    }
    /**
     * Cette méthode permet au client d’attendre jusqu’à ce que une certaine transaction soit confirmée.
     * @param txID ID de la transaction en attente de confirmation
     */
    private void waitForConfirmation(long txID) {
        int index = Utils.txID_to_index(clientID,txID);
        int txDepth = -1;
        while(true){
            txDepth = getTxBlockDepth(txID);
            if(txDepth != -1){
                break;
            }
            System.out.format("Client id %d is still waiting for all confirmation for transaction id %d\n",clientID,txID);
            Utils.forceThreadSleepSeconde(Thread.currentThread(),30);
        }
        while(true){
            int lastDepth = getLastBlockDepth();
            if(lastDepth == -1){
                //ERROR
                break;
            }
            if(lastDepth - txDepth >= CONFIRMATION_BLOCK_NUMBER){
                System.out.format("Client id %d received all confirmation for transaction id %d\n",clientID,txID);
                break;
            }
            System.out.format("Client id %d is still waiting for all confirmation for transaction id %d\n",clientID,txID);
            Utils.forceThreadSleepSeconde(Thread.currentThread(),30);
        }




    }

    /**
     * Cette methode initialise la connexion socket entre le client et un miner.
     * @return retourne un code d'erreur si l'initialisation de la connection s'est bien deroulee
     * ou non
     */
    public int init()throws IOException {
        minerCon = Utils.startConnectionToPeers(minerPort);
        return 0;
    }

    /**
     * Cette methode analyse le message recu et le deconstruit dans un format
     * qui peut etre compris par le client
     * @param message le message recu
     * @return code d'erreur decrivant le status (-1 : erreur, 0 : execution normale, -2 : continuer a ecouter)
     */
    public int process(String message){
        int code_error = 0;
        String messagetmp = message;
        String message_received = null;
        //String from = null;
        String fromtype = null;
        long fromID = -1;
        String[] result = messagetmp.split("@@");
        int nb_message = result.length;
        if(nb_message ==  1){
            message_received = result[0];
        }else{
            String from = result[0];
            String[] result_1 = from.split(" ");
            fromtype = result_1[0];
            fromID = Long.parseLong(result_1[1]);
            message_received = result[1];
        }
        System.out.format("Client id: %d received from miner message: %s  \n",this.clientID,message_received);
        result = message_received.split("%%");
        String command = result[0];
        switch (command.toUpperCase()) {
            case Constants.LASTDEPTH_COMMAND:
                code_error = Integer.parseInt(result[1]);
                break;
            case Constants.TXDEPTH_COMMAND:
                code_error = Integer.parseInt(result[1]);
                break;
            default:
                code_error = -1;
        }

        return code_error;

    }

    /**
     * Cette methode génère une transaction, l’envoie au miner et attend sa confirmation.
     *
     */
    public void sendTransaction() {
        Transaction tx = createTransaction();
        sendTX(tx);
        long tmp_id = tx.getTxID();
        Utils.forceThreadSleepSeconde(Thread.currentThread(),30);
        waitForConfirmation(tmp_id);
    }
}
