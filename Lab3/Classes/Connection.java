package Classes;
import java.io.*;
import java.net.Socket;

/**
 * Cette classe permet d'instancier le module de connection entre les differentes entitees du reseau
 * au niveau plus bas que les methodes que l'on peut trouver dans la classe Client.java ou Miner.java
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */

public class Connection {

	//Attributes
	public Socket socket;
	public Thread thread_listennerIncome;
	public PrintWriter out;
	public BufferedReader in;
	public long id;
	public Utils.Role Role;

	//Constructor
	public Connection(Socket socket){
		this.socket = socket;
		this.id = -1;
		this.thread_listennerIncome = null;
		try {
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING));
			this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	//Constructor override with id to identify which nodes is altering it's connection
	public Connection(Socket socket,int id){
		this.socket = socket;

		this.thread_listennerIncome = null;
		try {
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Constants.TELNET_ENCODING));
			this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), Constants.TELNET_ENCODING));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.id = id;

	}

	/**
	 * Methode permettant de lire les donnees recus par un node
	 * @return retourne les donnees lues en chaine de caractaire
	 */
	public String read() {
		String firstLine;
		try{
			while ((firstLine = in.readLine()) != null) {
				return firstLine;
			}
		}catch (IOException ex){
			ex.printStackTrace();
		}
		return "ERROR";
	}

	/**
	 * Methode permettant a un noeud d'envoyer une chaine de cractere aux autres nodes du reseau
	 * @param message
	 */
	public void write(String message) {
		//System.out.println("Noeud "+socket.getLocalPort()+" writing");
		out.write(message+ "\r\n");
		out.flush();
	}

	/**
	 * Methode permettant a un noeud d'envoyer un message dans le reseau puis de fermer
	 * sa propre connection
	 * @param message message a envoyer
	 */
	public void terminate(String message){
		if(message != null){
			write(message);
		}
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// This is telnet specific, maybe you have to change it according to your
	// protocol
	private static boolean checkIfFinished(String data) {
		int length = data.length();
		if (length < 3) {
			return false;
		} else {
			if (data.charAt(length -1) == '\n') {
				if (data.charAt(length -2) == '\r') {
					return true;
				}
			}
			return false;
		}
	}


	public void write_background(String message) {
		//System.out.println("Noeud "+socket.getLocalPort()+" writing");
		out.write(message+ "%@\r\n");
		out.flush();
	}


}