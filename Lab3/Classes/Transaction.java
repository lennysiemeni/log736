package Classes;
/**
 * Cette classe permet d'instancier une transaction
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import Interfaces.ITransaction;

import java.util.concurrent.Semaphore;

/**
 * Une transaction permet de transférer des Bitcoins entre deux comptes.
 * Pour simplifier l’implémentation, nous allons éliminer l’envoi de Bitcoin entre les clients.
 */
public class Transaction implements ITransaction {
        private long txID, clientID;
        private boolean confirmationState;
        public Semaphore lock;

        //Cosntructeur pour une transaction sans client
        public Transaction(long txID){
                this.txID = txID;
                this.confirmationState = false; //By default
                lock = new Semaphore(1);
                try {
                        lock.acquire();
                } catch (InterruptedException e) {
                        e.printStackTrace();
                }
        }

        //Constructeur
        public Transaction(long clientID,long txID){
                this.txID = txID;
                this.clientID = clientID;
                this.confirmationState = false; //By default
                lock = new Semaphore(1);
                try {
                        lock.acquire();
                } catch (InterruptedException e) {
                        e.printStackTrace();
                }
        }

        /**
         * Cette méthode change l’état de la transaction en “confirmée”.
         */
        public void setConfirmed(){
                this.confirmationState = true;
                this.lock.release();
        }

        /**
         * Cette méthode retourne l’état de la transaction.
         * @return
         */
        public boolean isConfirmed(){return confirmationState;}

        //Getter and setters
        public long getTxID() {
                return txID;
        }

        public void setTxID(long txID) {
                this.txID = txID;
        }

        @Override
        public String toString() {
                return String.valueOf(txID);
        }

}

