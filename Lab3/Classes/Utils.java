package Classes;
/**
 * Cette classe regroupe divers utilitaires communs utilisees dans plusieurs
 * classes du projet
 *
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Utils {

    public static enum Client_to_miner_command_type {LASTDEPTH, TXDEPTH, NEWTX;};
    public static enum Miner_to_client_message_type {TYPEONE, TYPETWO;};
    public static enum Miner_to_miner_message_type {HANDSHAKE, OK;};
    public static enum General_message_type {HELLOWORLD, SHUTDOWN;};
    public static enum Role {CLIENT, MINER, OTHER};
    public static void forceThreadSleepSeconde(Thread thread, long nbSecondes){
        try {
            thread.sleep(TimeUnit.SECONDS.toMillis(nbSecondes));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static long parse_time(String time, String format) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.parse(time).getTime();
    }

    /** (Pour simplifier, une transaction est représentée par un identifiant unique)*/
    public static long calcul_txID_unique(long client_id,int index){
        long tmp = 100;
        return (client_id * tmp) + index;
    }
    public static int txID_to_index(long client_id,long txID){
        long tmp = 100;
        return (int) (txID - (client_id * tmp));
    }
    public static Connection startConnectionToPeers(int port) throws IOException {
        Socket client_socket = new Socket();
        client_socket.connect(new InetSocketAddress("127.0.0.1", port));
        Connection conf = new Connection(client_socket);
        return conf;
    }
    public static String asynchrone_listen(Connection conf){
        return conf.read();
    }
    public static int generate_random_timeout(){
        int min_timeout = 5;
        int max_timeout = 30;
        return (int) Math.random() * (max_timeout - min_timeout + 1) + min_timeout;
    }
    public static String synchrone_listen(Connection con,int timeout){
        //System.out.format("Follower id %d waiting for leader promise reaction\n",id);
        Semaphore detector_mutex = new Semaphore(1);
        String[] answer = {null};
        Thread thread = new Thread("Thread "+con.socket.getPort()) {
            public void run(){
                try {
                    detector_mutex.acquire();
                    answer[0] = con.read();
                    detector_mutex.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } }};
        thread.start();
        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int timeout_compt=0;
        //int time_wait = 15;
        int timeout_tolere_limit = 0;
        while(true){
            try {
                boolean isAcquired = detector_mutex.tryAcquire(timeout, TimeUnit.SECONDS); /** If the specified waiting time elapses then the value false returned.  */
                if(isAcquired || timeout_tolere_limit < timeout_compt)
                    break;
                timeout_compt++;
            } catch (InterruptedException e) {
                System.out.format("The current thread is interrupted\n");
                e.printStackTrace(); } }
        return answer[0];
    }

}
