package Classes;
/**
 * Classe Main de notre implementation du consensus de Nakamoto
 *
 * Plus bas vous trouverez 5 tests differents permettant de voir le
 * comportement du reseau selon les scenario suivants :
 * - Test de Bavardage
 * - Test de Synchronisation
 * - Test de consensus
 * - Test de transactions de minage des blocs
 * - Test sur l'ajustement de difficulte et calcul du nounce
 * @author Lenny SIEMENI
 * @author Mehran ASADI
 * @author Nader BAYDOUN
 *
 */

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainClass {

    //Pretty print
    public static void print_separator(int nb_test){
        int nb_tiret = 60;
        int nb_tiret_moite = nb_tiret/2 -3;
        for(int i =0;i<nb_tiret;i++)
            System.out.format("-");
        System.out.println("");
        for(int i =0;i<nb_tiret_moite;i++)
            System.out.format(" ");
        System.out.format("test %d",nb_test);
        for(int i =0;i<nb_tiret_moite;i++)
            System.out.format(" ");
        System.out.println("");
        for(int i =0;i<nb_tiret;i++)
            System.out.format("-");
        System.out.println("");
    }

    /**
     * Methode utilitaire pour acceder aux port des mineurs instancies a partir de la classe Miner.java
     * @param id l'identifiant du miner
     * @return port miner
     */
    public static int calculMinerPort(int id){
        return 25100 + id * 100;
    }

    //Preps pour les tests
    public static Client[] generateClientArray(int nbClient){
        Client[] client_array = new Client[nbClient];
        for(int i=0;i<nbClient;i++){
            client_array[i] = new Client(i+1);
        }
        return  client_array;
    }
    public static Miner[] generateMinerArray(int nbMiner){
        Miner[] miner_array = new Miner[nbMiner];
        for(int i=0;i<nbMiner;i++){
            miner_array[i] = new Miner(i,nbMiner);
        }
        return  miner_array;
    }
    public static LinkedList<Transaction> generateTransactions(int number_max){
        int min_timeout = 5;
        int max_timeout = 30;
        LinkedList<Transaction> transactions = new LinkedList<Transaction>();
        int nb = 0;
        int randomValue = 0;
        while(nb < number_max){
            transactions.add(new Transaction(nb));
            nb++;
        }

        return transactions;
    }
    public static List<Long> generateListRandomValue(int number_max){
        int min_timeout = 5;
        int max_timeout = 30;
        List<Long> transactions = new ArrayList<Long>();
        int nb = 0;
        long randomValue = 0;
        while(nb < number_max){
            randomValue = (long) (Math.random() * (max_timeout - min_timeout + 1) + min_timeout);
            transactions.add(randomValue);
            nb++;
        }

        return transactions;
    }
    public static Miner[] prep()
    {
        int nbMiner = 3;
        int lol = 9;
        Miner[] miner_array = generateMinerArray(nbMiner);

        List<Transaction> lts1 = generateTransactions(lol);
        for(int i = 0; i < lol;i++)
        {
            if(i%3==0)
            {
                miner_array[0].mempool.add(lts1.get(i));
            }
            if(i%3==1)
            {
                miner_array[1].mempool.add(lts1.get(i));
            }
            if(i%3==2)
            {
                miner_array[2].mempool.add(lts1.get(i));
            }
        }

        int nbClient = 3;
        Client[] client_array = generateClientArray(nbClient);
        for(int i=0;i<nbClient;i++){
            client_array[i].setMinerPort(calculMinerPort(i));
            try {
                client_array[i].init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for(int i=0;i<nbMiner;i++){
            miner_array[i].connectToAll();
        }

        Utils.forceThreadSleepSeconde(Thread.currentThread(),5);
        return miner_array;
    }


    public static void test1(){
        int target = 2;
        int nbMiner = 6;
        Miner[] miner_array = generateMinerArray(nbMiner);
        int nbClient = 3;
        int size_block = 10 - nbClient; /**Puisque BLOCK_SIZE est 10, on veux que les transactions de chaque soit inserer dans prochain le bloc minage */
        /**Initialisation des client */
        Client[] client_array = generateClientArray(nbClient);
        for(int i=0;i<nbClient;i++){
            client_array[i].setMinerPort(calculMinerPort(i));
            try {
                client_array[i].init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        /**Initialisation des mineurs */
        for(Miner mn : miner_array) {
            mn.mempool = generateTransactions(size_block);
            mn.target = target;
            mn.connectToAll();
        }
        Utils.forceThreadSleepSeconde(Thread.currentThread(),10); //Attendre que tout les mineurs se connectent
        /**Chaque client de façon parallèle envoit sa transaction */
        for(Client cl : client_array){
            Thread thread = new Thread(){
                public void run(){
                    cl.sendTransaction();
                }
            };

            thread.start();
        }
        Utils.forceThreadSleepSeconde(Thread.currentThread(),10); //Attendre le bavardage des transactions reçu du client
        System.out.println(miner_array[0].mempool);

        //Minage
        ExecutorService es = Executors.newCachedThreadPool();
        for(Miner mn : miner_array) {
            es.execute(new Runnable() {
                public void run() {
                    int tmp = 0;
                    if(mn.id != 0)
                        tmp = 3;
                    mn.mine_and_send(tmp);
                }
            });

        }

        es.shutdown();
        try {
            boolean finished = false;
            while(!finished){
                finished = es.awaitTermination(1, TimeUnit.MINUTES);
                if(!finished ){
                    //Maybe panne prcessor detected
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("TEST CONFIRMATION DE TRANSACTION -> DONE");
    }

    /**
     * Fonction qui test le minage de block
     */
    public static void testBlocks()
    {
        String genesisHash = "0";
        Transaction t1 = new Transaction(1);
        Transaction t2 = new Transaction(1);
        Transaction t3 = new Transaction(1);
        List<Long> transactions = new ArrayList<Long>();
        transactions.add(t1.getTxID());
        transactions.add( t2.getTxID());
        transactions.add(t3.getTxID());

        int firstNounce = 1000000;

        long tm = System.currentTimeMillis();
        Block b1 = new Block(genesisHash, transactions,0, tm);
        b1.setNounce(firstNounce);
        int myNounce = 0;

        String b1Hash = b1.calculateBlockHash();

        String attemptHash = "";
        boolean hashFound = false;

        List<String> hashedTxs = new ArrayList<String>();
        for(Long t : transactions)
        {
            hashedTxs.add(HashfromString.sha256Hash(t+""));
        }
        String concat = "";
        for(String s : hashedTxs)
        {
            concat += s;
        }

        while(!hashFound)
        {
            attemptHash = concat+myNounce;
            attemptHash = HashfromString.sha256Hash(attemptHash);

            if(attemptHash.equals(b1Hash))
            {
                hashFound = true;
                break;
            }

            if(myNounce > firstNounce)
            {
                System.out.println("BLOCK MINING TEST -> BAD -> Something went wrong :(");
                System.exit(0);
            }
            myNounce++;
        }
        System.out.println("BLOCK MINING TEST -> GOOD -> Hashfound: " + attemptHash);
    }

    public static void testBavardage()
    {
        //On assume que c'est toujour 3 pour simplicite
        Miner[] miner_array = prep();
        for(Miner m : miner_array)
        {
            m.bavardage();
        }
        class Helper extends TimerTask
        {
            public void run()
            {
                try {
                    TreeSet<Long> set0 = new TreeSet<Long>();
                    for (Transaction t : miner_array[0].mempool) {
                        set0.add(t.getTxID());
                    }

                    TreeSet<Long> set1 = new TreeSet<Long>();
                    for (Transaction t : miner_array[1].mempool) {
                        set1.add(t.getTxID());
                    }

                    TreeSet<Long> set2 = new TreeSet<Long>();
                    for (Transaction t : miner_array[2].mempool) {
                        set2.add(t.getTxID());
                    }

                    boolean bool0 = set0.equals(set1);
                    boolean bool1 = set0.equals(set2);
                    boolean bool2 = set1.equals(set2);

                    if (bool0 && bool1 && bool2) {
                        System.out.println("BAVARDAGE TEST -> GOOD");
                    }
                }
                catch (Exception e) {
                }
            }
        }
        Timer timer = new Timer();
        TimerTask task = new Helper();

        timer.schedule(task, 10000);
    }


    /**
     * Test pour synchroniser la blockchain de chaque mineur a la chaine de bloc la plus grande du reseau
     *
     */
    public static void testSync() throws IOException, InterruptedException {

        //Create fake blockchain
        int size_block = 10;
        List<Long> transactions_id = generateListRandomValue(size_block);


        int depth = 0;
        //https://bitcoin.fr/bloc-genesis/
        String blocgeneseHash = "000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f";
        String previousHash = "0000000000000000000000000000000000000000000000000000000000000000";
        long tm = System.currentTimeMillis();
        depth++;
        transactions_id = generateListRandomValue(size_block);
        tm = System.currentTimeMillis();
        Block b2 = new Block(blocgeneseHash,transactions_id,depth,tm);
        depth++;


        String previoushash = b2.calculateBlockHash();
        transactions_id = generateListRandomValue(size_block);
        tm = System.currentTimeMillis();
        Block b3 = new Block(previoushash,transactions_id,depth,tm);
        depth++;

        previoushash = b3.calculateBlockHash();
        transactions_id = generateListRandomValue(size_block);
        tm = System.currentTimeMillis();
        Block b4 = new Block(previoushash,transactions_id,depth,tm);
        depth++;

        previoushash = b4.calculateBlockHash();
        transactions_id = generateListRandomValue(size_block);
        tm = System.currentTimeMillis();
        Block b5 = new Block(previoushash,transactions_id,depth,tm);
        b5.calculateBlockHash();

        //Create mempool
        LinkedList<Transaction> mempool = generateTransactions(size_block+6);

        //Create Miners
        int nbMiner = 10;
        Miner[] miner_array = generateMinerArray(nbMiner);

        //populate miners with data
        for(Miner mn : miner_array) {
            mn.addBlock(b2);
            mn.addBlock(b3);
            mn.mempool = mempool;
            mn.target = 2;
            mn.connectToAll();
        }

        miner_array[3].addBlock(b4);
        miner_array[3].addBlock(b5);
        miner_array[0].addBlock(b4);

        StringBuilder temp_miner = new StringBuilder();
        for(Block bc : miner_array[1].blockchain){
            temp_miner.append(bc.toString());
        }

        System.out.println("Miner One blockchain BEFORE Sync :"+ temp_miner);
        //Request other miners to send their blockchains
        miner_array[1].setAsked_for_sync(true);
        miner_array[1].broadcast_message("SYNCHRONIZE");

        Thread.sleep(1000);

        //Synchronise miner[1] blockchain
        miner_array[1].synchronise();
        temp_miner.setLength(0);
        temp_miner = new StringBuilder();
        for(Block bc : miner_array[1].blockchain){
            temp_miner.append(bc.toString());
        }
        System.out.println("MIER ONE BLOCKCHAIN AFTER SYNC :"+temp_miner);
        miner_array[1].setAsked_for_sync(false);

    }


    public static void main(String[] args) throws IOException, InterruptedException {
        /**
         * Note : Certains tests peuvent prendre du temps mais ne doivent pas durer au-dela de 10 secondes
         */
        System.out.println("Note : Certains tests peuvent prendre du temps mais ne doivent pas durer au-dela de 10 secondes");
        //print_separator(1);
        test1();
        //testBlocks();
        //testSync();
    }
}
